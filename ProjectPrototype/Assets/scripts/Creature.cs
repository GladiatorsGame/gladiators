﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creature
{
    public int hpMax;
    public int hpCurrent;
    
    public int apMax;
    public int apCurrent;

    public int cpMax;
    public int cpCurrent;


    public int blockCurrent;

    public int strCurrent;

    public string name;


    public Creature(int hpMax, int apMax, string name)
    {
        this.hpMax = hpMax;
        hpCurrent = hpMax;

        this.apMax = apMax;
        apCurrent = apMax;

        cpMax = 1;
        cpCurrent = cpMax;

        blockCurrent = 0;
        strCurrent = 0;

        this.name = name;  
    }

    public void IncreaseHp(int hpAmount)
    {
        hpCurrent = Mathf.Min(hpCurrent + hpAmount, hpMax);
    }

    public void DecreaseHp(int hpAmount)
    {
        hpCurrent = Mathf.Max(hpCurrent - hpAmount, 0);
    }

    public void ResetHp()
    {
        hpCurrent = hpMax;
    }

    public void IncreaseAp(int apAmount)
    {
        apCurrent += apAmount;
    }

    public void DecreaseAp(int apAmount)
    {
        apCurrent = Mathf.Max(apCurrent - apAmount, 0);
    }

    public void ResetAp()
    {
        apCurrent = apMax;
    }

    public void IncreaseCp(int cpAmount)
    {
        cpCurrent = cpCurrent + cpAmount;
    }

    public void DecreaseCp(int cpAmount)
    {
        cpCurrent = Mathf.Max(cpCurrent - cpAmount, 0);
    }

    public void ResetCp()
    {
        cpCurrent = cpMax;
    }

    public void ConvertCp()
    {
        IncreaseCp(1);
        DecreaseAp(1);
    }



    public void IncreaseBlock(int blockAmount)
    {
        blockCurrent += blockAmount;
    }

    public void DecreaseBlock(int blockAmount)
    {
        blockCurrent = Mathf.Max(blockCurrent - blockAmount, 0);
    }

    public void ResetBlock()
    {
        blockCurrent = 0;
    }

}
