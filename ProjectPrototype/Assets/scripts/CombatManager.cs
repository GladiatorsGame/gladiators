﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CombatManager : MonoBehaviour
{
    
    public Player player;
    public Enemy enemy;

    public Creature activeCreature;
    public Creature inactiveCreature;

    public enum CombatStates { InitialSetup, Combat, None, Finished};
    public CombatStates currentCombatState;




    public static CombatManager main;
    // Start is called before the first frame update
    void Start()
    {
        if (main == null)
        {
            main = this;
        }
        currentCombatState = CombatStates.None;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void InitializeCombat()
    {
        player = CreatureManager.main.GetPlayer();
        enemy = CreatureManager.main.GetEnemy();

        activeCreature = player;
        //inactiveCreature = enemy;

        player.ResetAp();
        player.ResetInitialNbActions();

        enemy.SetNextAction();

        currentCombatState = CombatStates.InitialSetup;
        SequenceManager.main.InitializeCombat();
        SelectionManager.main.InitializeCombat();
        CombatUiManager.main.InitializeCombat();
        
    }

    public void StartCombat()
    {
        currentCombatState = CombatStates.Combat;
        CombatUiManager.main.StartCombat();
    }

    public void EndTurn()
    {
        //simulation attaque de l'ennemi
        enemy.ResetBlock();
        enemy.PlayActions();
        enemy.SetNextAction();

        SequenceManager.main.DecreaseCooldowns();
        switch(SelectionManager.main.clickMode)
        {
            case SelectionManager.ClickModes.Play:
                SequenceManager.main.ActivatePlayMode();
                break;

            case SelectionManager.ClickModes.Edit:
                SequenceManager.main.ActivateEditMode();
                break;
        }

        player.ResetAp();
        player.ResetCp();
        player.ResetBlock();

        CombatUiManager.main.UpdateTextFields();
        CombatUiManager.main.UpdateButtons();
    }

    public void CheckCombatEnd()
    {
        if(player.hpCurrent <= 0 || enemy.hpCurrent <= 0)
        {
            currentCombatState = CombatStates.Finished;
            CombatUiManager.main.EndCombat();
        }
    }
}
