﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatureManager : MonoBehaviour
{
    public static CreatureManager main;
    public static Player player;
    public Enemy enemy;

    // Start is called before the first frame update
    void Start()
    {
        if(main == null)
        {
            main = this;
        }
        
        if(player == null)
        {
            player = new Player(80, 3, "Player");
        }

        int enemyHp = Random.Range(40, 45);
        enemy = new Enemy(enemyHp, "Jaw Worm");

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Player GetPlayer()
    {
        return player;
    }

    public Enemy GetEnemy()
    {
        return enemy;
    }
}
