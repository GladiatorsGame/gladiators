﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
///  Cooldown Manager qui recoit message d'un slot et qui vérifie si séquence doit être mise en cooldown.
/// </summary>

public class SequenceManager : MonoBehaviour
{
    public static SequenceManager main;
    public List<Sequence> sequences;

    public Player player;
    public Enemy enemy;

    public int currentSequence;

    public List<SlotController> sequence1slots;
    public List<SlotController> sequence2slots;
    public List<SlotController> sequence3slots;

    public Text[] cooldownTexts;

    public int lastPlayedSequence;
    public int lastPlayedSlot;


    // Start is called before the first frame update
    void Start()
    {
        if (main == null)
        {
            main = this;
        }
        sequences = new List<Sequence>();

        sequences.Add(new Sequence(0, sequence1slots, cooldownTexts[0]));
        sequences.Add(new Sequence(1, sequence2slots, cooldownTexts[1]));
        sequences.Add(new Sequence(2, sequence3slots, cooldownTexts[2]));

    }

    public void InitializeCombat()
    {
        player = CreatureManager.main.GetPlayer();
        enemy = CreatureManager.main.GetEnemy();
    }


    //fonction qui decrease de 1 le nombre de tours restants pour les séquences en cooldown
    public void DecreaseCooldowns()
    {
        foreach (Sequence sequence in sequences)
        {
            if (sequence.cooldown)
            {
                if (sequence.remainingCooldown <= 0)
                {
                    sequence.RemoveCooldown();
                }
                else
                {
                    sequence.DecreaseCooldown();
                }

            }
        }
    }

    public void ApplySequenceCooldown(int sequenceNumber, int cooldownDuration)
    {
        if (!sequences[sequenceNumber].cooldown)
        {
            sequences[sequenceNumber].ApplyCooldown(cooldownDuration);
        }
    }

    public void SlotPlayCooldownUpdate(int newSequenceNumber, int newSlotNumber)
    {
        lastPlayedSequence = newSequenceNumber;
        lastPlayedSlot = newSlotNumber;
        //Si une séquence différente que celle en cours est entamée, applique cooldown
        if (newSequenceNumber != currentSequence && sequences[currentSequence].currentSlot > 0 && newSlotNumber == 0)
        {
            sequences[currentSequence].ApplyCooldown(1);
        }

        currentSequence = newSequenceNumber;
        sequences[currentSequence].slots[newSlotNumber].DisableSlotPlay();

        //si c'est la dernière slot de la séquence, la met en cooldown
        if (sequences[currentSequence].currentSlot >= sequences[currentSequence].slots.Count - 1)
        {
            sequences[currentSequence].ApplyCooldown(1);
        }
        //ou si la prochaine slot est vide, donc fin de la séquence, met en cooldown
        else if (sequences[currentSequence].slots[sequences[currentSequence].currentSlot + 1].containedAction == null)
        {
            sequences[currentSequence].ApplyCooldown(1);
        }
        //ou sinon ça poursuit avec la suite de la séquence
        else
        {
            sequences[currentSequence].currentSlot++;
            sequences[currentSequence].slots[sequences[currentSequence].currentSlot].EnableSlotPlay(); //active la prochaine slot dans la séquence
        }
    }

    public void ActivatePlayMode()
    {
        foreach (Sequence sequence in sequences)
        {
            if (!sequence.cooldown) {
                //sequence.EnableSlotsPlay();
                //code de EnableSlotsPlay() déménagé ici pour cohérence
                foreach (SlotController slot in sequence.slots)
                {
                    if (slot.containedAction == null || slot.slotNumber != sequence.currentSlot)
                    {
                        slot.DisableSlotPlay();
                    }
                    else
                    {
                        slot.EnableSlotPlay();
                    }
                }
                //fin du bloc déménagé
            }
            else
            {
                sequence.DisableSlotsPlay();
            }
        }
    }

    public void ActivateEditMode()
    {
        foreach (Sequence sequence in sequences)
        {
            foreach (SlotController slot in sequence.slots)
            {
                if (CombatManager.main.currentCombatState == CombatManager.CombatStates.InitialSetup)
                {
                    if (slot.slotNumber < player.initialSequenceLength && slot.sequenceNumber < player.initialNbSequences)
                    {
                        slot.EnableSlotEdit();
                        if (slot.containedAction == null)
                        {
                            break;
                        }
                    }
                }
                else
                {
                    slot.EnableSlotEdit();
                    if (slot.containedAction == null)
                    {
                        break;
                    }
                }

            }
        }
    }

    public void saveLastPlayed(int newSequenceNumber, int newSlotNumber)
    {
        lastPlayedSequence = newSequenceNumber;
        lastPlayedSlot = newSlotNumber;
    }

    public bool CheckInitialSetupComplete()
    {
        if (player.initialNbActionsCurrent <= 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public SlotController GetSlotController(int sequenceNumber, int slotNumber)
    {
        return sequences[sequenceNumber].GetSlotController(slotNumber);
    }
}
