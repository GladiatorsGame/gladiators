﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActionSetComparer : IEqualityComparer<Action>
{
    public bool Equals(Action x, Action y)
    {
        //Debug.Log(x.GetType() + " / " + y.GetType());
        if (x.GetType() == y.GetType())
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    public int GetHashCode(Action obj)
    {
        return obj.GetType().GetHashCode();
    }
}
