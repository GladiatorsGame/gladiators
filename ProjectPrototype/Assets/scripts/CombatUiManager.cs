﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CombatUiManager : MonoBehaviour
{
    public Player player;
    public Enemy enemy;

    public Text textPlayerHp, textPlayerAp, textPlayerCp, textPlayerBlock, textEnemyHp, textEnemyBlock, textEnemyIntent;


    public Button buttonStartCombat;
    public Button buttonEndTurn;
    public Button buttonConvertCp;
    public Text textButtonEndTurn;

    public static CombatUiManager main;
    // Start is called before the first frame update
    void Start()
    {
        main = this;
        buttonStartCombat.onClick.AddListener(ButtonStartCombatPressed);
        buttonEndTurn.onClick.AddListener(ButtonEndTurnPressed);
        buttonConvertCp.onClick.AddListener(ButtonConvertCpPressed);
        buttonEndTurn.interactable = false;
        buttonConvertCp.interactable = false;
        textButtonEndTurn.text = "End Turn";
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ButtonStartCombatPressed()
    {
        CombatManager.main.InitializeCombat();
    }

    public void ButtonEndTurnPressed()
    {
        if(CombatManager.main.currentCombatState == CombatManager.CombatStates.Combat)
        {
            CombatManager.main.EndTurn();
        }
        else
        {
            SceneManager.LoadScene("CharacterEdit");
        }
             
    }

    public void ButtonConvertCpPressed()
    {
        player.ConvertCp();
        UpdateTextFields();
        UpdateButtons();
    }

    public void InitializeCombat()
    {
        player = CreatureManager.main.GetPlayer();
        enemy = CreatureManager.main.GetEnemy();

        buttonStartCombat.interactable = false;

        UpdateTextFields();
    }

    public void StartCombat()
    {
        buttonEndTurn.interactable = true;

        //même code que UpdateButtons(), appeler UpdateButtons()??
        buttonConvertCp.interactable = true;
        if (player.apCurrent <= 0)
        {
            buttonConvertCp.interactable = false;
        }
    }

    public void EndCombat()
    {
        textButtonEndTurn.text = "End Combat";
    }

    public void UpdateTextFields()
    {
        textPlayerHp.text = "HP: " + player.hpCurrent + "/" + player.hpMax;
        textPlayerAp.text = "AP: " + player.apCurrent + "/" + player.apMax;
        textPlayerCp.text = "CP: " + player.cpCurrent + "/" + player.cpMax;
        textPlayerBlock.text = "Block: " + player.blockCurrent;
        textEnemyHp.text = "HP: " + enemy.hpCurrent + "/" + enemy.hpMax;
        textEnemyBlock.text = "Block: " + enemy.blockCurrent;
        textEnemyIntent.text = "Enemy intent:\n" + enemy.enemyActions[enemy.nextActionNumber].actionName;
    }

    public void UpdateButtons()
    {
        buttonConvertCp.interactable = true;
        if (player.apCurrent <= 0)
        {
            buttonConvertCp.interactable = false;
        }
    }
}
