﻿using UnityEngine;
using System.Collections;

public class ActionState
{
    private bool doubleDamageActive;
    private bool gainHalfBlockActive;

    public ActionState()
    {
        doubleDamageActive = false;

    }

    public void SetDoubleDamageActiveState(bool newState)
    {
        doubleDamageActive = newState;
    }


    public bool GetDoubleDamageActiveState()
    {
        return doubleDamageActive;
    }
}
