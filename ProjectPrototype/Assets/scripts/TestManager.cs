﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TestManager: MonoBehaviour
{
    public Button buttonTest;
    public Button buttonAddSkill1;
    public Button buttonAddSkill2;
    public Player player;

    void Start()
    {
        player = CreatureManager.main.GetPlayer();
        buttonTest.onClick.AddListener(ButtonTestPressed);
        buttonAddSkill1.onClick.AddListener(ButtonAddSkill1Pressed);
        buttonAddSkill2.onClick.AddListener(ButtonAddSkill2Pressed);
    }


    public void ButtonTestPressed()
    {
        SceneManager.LoadScene("CombatScene");
    }

    public void ButtonAddSkill1Pressed()
    {
        player.AddPlayerAction(new BigAttackAction());
        buttonAddSkill1.interactable = false;
        ListActions();
    }
    public void ButtonAddSkill2Pressed()
    {
        player.AddPlayerAction(new BigBlockAction());
        buttonAddSkill2.interactable = false;
        ListActions();
    }

    public void ListActions()
    {
        foreach (KeyValuePair<Action, int> element in player.playerActionSet)
        {
            Debug.Log(element.Key.actionName + " x " + element.Value);
        }
        
    }

}
