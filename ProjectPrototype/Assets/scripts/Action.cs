﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Action
{
    // Start is called before the first frame update
    public string actionName;

    public abstract void Play(Creature activeCreature, Creature inactiveCreature, ActionState actionState);
}
