﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SelectionManager : MonoBehaviour
{
    static public SelectionManager main;

    public Player player;
    public Enemy enemy;

    public Action selectedCategory;
    public List<ActionButtons> actionButtonsList;
    public SlotController hoveredSlot;

    public Button buttonPlay;
    public Text textClickMode;

    public enum ClickModes { Play, Edit}; //pour déterminer le comportement des slots si elles sont cliquées
    public ClickModes clickMode;

    public GameObject actionButtonPrefab;
    public GameObject actionLayoutGroup;

    // Start is called before the first frame update
    void Start()
    {
        if (main == null)
        {
            main = this;
        }
        buttonPlay.onClick.AddListener(ButtonPlayPressed);
        selectedCategory = null;

        actionButtonsList = new List<ActionButtons>();
        buttonPlay.interactable = false;
        ChangeEditMode(ClickModes.Play);
    }

    public void InitializeCombat()
    {
        player = CreatureManager.main.GetPlayer();
        enemy = CreatureManager.main.GetEnemy();

        selectedCategory = null;
        ChangeEditMode(ClickModes.Edit);
        buttonPlay.interactable = false;
        LoadPlayersAction();

    }

    public void ChangeHoveredSlot(SlotController newHoveredSlot)
    {
        hoveredSlot = newHoveredSlot;
    }
    public void ClearHoveredSlot()
    {
        hoveredSlot = null;
    }

    public void ChangeEditMode(ClickModes newClickMode)
    {
        clickMode = newClickMode;
        textClickMode.text = clickMode.ToString() + " Mode";
    }

    //fonction quand une slot est cliquée
    public void ClickSlot()
    {
        switch (clickMode)
        {
            //si on est en Play mode, l'action est jouée
            case ClickModes.Play:
                if (hoveredSlot != null && player.apCurrent > 0)
                {
                    SequenceManager.main.saveLastPlayed(hoveredSlot.sequenceNumber, hoveredSlot.slotNumber);
                    hoveredSlot.PlayAction();
                    SequenceManager.main.SlotPlayCooldownUpdate(hoveredSlot.sequenceNumber, hoveredSlot.slotNumber); 
                    CombatUiManager.main.UpdateTextFields();
                    CombatUiManager.main.UpdateButtons();
                    CombatManager.main.CheckCombatEnd();

                }
                break;

            //si on est en Edit mode, l'action est attachée à la slot
            case ClickModes.Edit:
                ActionButtons currentActionButton = actionButtonsList.Find(x => x.GetLinkedAction() == selectedCategory);

                if (hoveredSlot != null && selectedCategory != null && player.cpCurrent > 0 && currentActionButton.GetCountCurrent() >= 1)
                {
                    int sequenceNumber = hoveredSlot.sequenceNumber;
                    int slotNumber = hoveredSlot.slotNumber;

                    if(hoveredSlot.containedAction != null)
                    {
                        Action oldAction = hoveredSlot.containedAction;
                        ActionButtons oldActionButton = actionButtonsList.Find(x => x.GetLinkedAction() == oldAction);
                        oldActionButton.IncreaseActionCountCurrent(1);
                    }
                    hoveredSlot.SetActionName(selectedCategory);
                    currentActionButton.DecreaseActionCountCurrent(1);

                    //si on est en combat, le joueur perd un AP(CP) et la séquence éditée tombe en cooldown
                    if(CombatManager.main.currentCombatState == CombatManager.CombatStates.Combat)
                    {
                        player.DecreaseCp(1);
                        CombatUiManager.main.UpdateTextFields();
                        SequenceManager.main.ApplySequenceCooldown(sequenceNumber,0);
                    }
                    //si on est dans le setup du combat, une action est déduite de la banque
                    else if(CombatManager.main.currentCombatState == CombatManager.CombatStates.InitialSetup)
                    {
                        player.DecreaseInitialNbActions(1);
                        if (SequenceManager.main.CheckInitialSetupComplete())
                        {
                            CombatManager.main.StartCombat();  //activer un bouton start combat à la place de commencer direct
                            buttonPlay.interactable = true;
                            SequenceManager.main.ActivateEditMode();  //juste pour que le manager update les slots disponibles
                        }
                    }
                    //si la slot éditée n'est pas la dernière de la séquence, la prochaine est enabled pour edit
                    if (slotNumber < SequenceManager.main.sequences[sequenceNumber].slots.Count - 1)
                    {
                        SlotController nextSlot = SequenceManager.main.sequences[sequenceNumber].slots[slotNumber + 1];

                        //Si on est dans le setup initial pré-combat, le nombre de slots est limité.
                        if(CombatManager.main.currentCombatState == CombatManager.CombatStates.InitialSetup)
                        {
                            if(nextSlot.slotNumber < player.initialSequenceLength)
                            {
                                nextSlot.EnableSlotEdit();

                                
                            }
                        }
                        else
                        {
                            nextSlot.EnableSlotEdit();
                        }
                        
                    }
                }
                break;
        }  
    }

    public void ButtonPlayPressed()
    {
        selectedCategory = null;
        ChangeEditMode(ClickModes.Play);
        textClickMode.text = "Play Mode";
        SequenceManager.main.ActivatePlayMode();
        buttonPlay.interactable = false;
    }

    private void LoadPlayersAction()
    {
        actionButtonsList.Clear();

        foreach (KeyValuePair<Action, int> actionEntry in player.playerActionSet)
        {
            print(actionEntry.Key + " " + actionEntry.Value);
            GameObject newButton = Instantiate(actionButtonPrefab, actionLayoutGroup.transform);
            newButton.GetComponent<ActionButtons>().SetLinkedAction(actionEntry.Key, actionEntry.Value);
            actionButtonsList.Add(newButton.GetComponent<ActionButtons>());
        }
    }

    public void CategorySelected(Action linkedAction)
    {
        selectedCategory = linkedAction;
        ChangeEditMode(ClickModes.Edit);
        if (CombatManager.main.currentCombatState != CombatManager.CombatStates.InitialSetup)
        {
            buttonPlay.interactable = true;
        }
        SequenceManager.main.ActivateEditMode();
    }
}
