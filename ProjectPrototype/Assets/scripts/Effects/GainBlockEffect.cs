﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GainBlockEffect: Effect
{
    public int blockAmount;

    public GainBlockEffect(int blockAmount)
    {
        this.blockAmount = blockAmount;
    }

    public override void ApplyEffect(Creature activeCreature, Creature passiveCreature, ActionState actionState)
    {
        activeCreature.IncreaseBlock(blockAmount);
    }

    public override int GetEffectPower(Creature activeCreature, Creature passiveCreature)
    {
        return blockAmount;
    }

}
