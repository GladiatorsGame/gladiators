﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DealDamageEffect: Effect
{
    public int mainDamage;
    public int totalDamage;
    public int adjustedDamage;

    public DealDamageEffect(int damage)
    {
        mainDamage = damage;
    }
    public override void ApplyEffect(Creature activeCreature, Creature passiveCreature, ActionState actionState)
    {

        totalDamage = mainDamage + activeCreature.strCurrent;

        if(actionState.GetDoubleDamageActiveState())
        {
            totalDamage *= 2;
        }

        adjustedDamage = Mathf.Max(totalDamage - passiveCreature.blockCurrent, 0); // Create handle le block
        Debug.Log("total damage: " + totalDamage);
        Debug.Log("adjusted damage: " + adjustedDamage);
        Debug.Log("passive creature block : " + passiveCreature.blockCurrent);
        passiveCreature.DecreaseHp(adjustedDamage);

        passiveCreature.blockCurrent = Mathf.Max(passiveCreature.blockCurrent - totalDamage, 0);
        CombatUiManager.main.UpdateTextFields();
        //Debug.Log(targetCreature.name + " HP:" + targetCreature.hpCurrent);
    }

    public override int GetEffectPower(Creature activeCreature, Creature passiveCreature)
    {
        return mainDamage + activeCreature.strCurrent;
    }

}
