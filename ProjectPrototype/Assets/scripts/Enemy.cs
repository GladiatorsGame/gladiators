﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Creature
{ 
    public List<Action> enemyActions;
    public int nextActionNumber;
    public ActionState enemyActionState;

    public Enemy(int hpMax, string name) : base(hpMax, 0, name)
    {
        nextActionNumber = 0;
        enemyActions = new List<Action>();
        enemyActions.Add(new AttackAction(11, "Attack"));
        enemyActions.Add(new AttackBlockAction(7, 5, "Attack\nBlock"));
        enemyActions.Add(new BlockAction(6, "Buff\nBlock"));
        enemyActionState = new ActionState();

    }

    public void SetNextAction()
    {
        int r = Random.Range(0, 100);
        if (r < 45)
        {
            nextActionNumber = 0;
        }
        else if (r < 45 + 30)
        {
            nextActionNumber = 1;
        }
        else if (r < 45 + 30 + 25)
        {
            nextActionNumber = 2;    
        }
    }

    public void PlayActions()
    {
        if (nextActionNumber == 2)
        {
            strCurrent += 3;
        }
        enemyActions[nextActionNumber].Play(this, CreatureManager.main.GetPlayer(), enemyActionState);
    }
}
