﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActionButtons : MonoBehaviour
{

    Action linkedAction;
    int countCurrent, countMax;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(ButtonPressed);
    }

    private void ButtonPressed()
    {
        SelectionManager.main.CategorySelected(linkedAction);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetLinkedAction(Action newLinkedAction, int count)
    {

        linkedAction = newLinkedAction;
        countCurrent = count;
        countMax = count;
        UpdateText();
       // GetComponentInChildren<Text>().text = newLinkedAction.actionName + " X " + count;
    }

    private void UpdateText()
    {
        GetComponentInChildren<Text>().text = linkedAction.actionName + " X " + countCurrent;
    }

    public Action GetLinkedAction()
    {
        return linkedAction;
    }

    public int GetCountCurrent()
    {
        return countCurrent;
    }

    public int GetCountMax()
    {
        return countMax;
    }

    public void IncreaseActionCountMax(int amount)
    {
        countMax += amount;
        countCurrent += amount;
        UpdateText();
    }

    public void DecreaseActionCountMax(int amount)
    {
       countMax = Mathf.Max(countMax - amount, 0);
       UpdateText();
    }

    public void IncreaseActionCountCurrent(int amount)
    {
        countCurrent = Mathf.Min(countCurrent + amount, countMax);
        UpdateText();
    }

    public void DecreaseActionCountCurrent(int amount)
    {
        countCurrent = Mathf.Max(countCurrent - amount, 0);
        UpdateText();
    }

}
