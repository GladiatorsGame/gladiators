﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextAttackX2Action : Action
{
    //Liste d'effets Effects
    public Effect effect;

    // Start is called before the first frame update
    public NextAttackX2Action(int damage = 6, string actionName = "NextAttackActionX2")
    {
        this.actionName = actionName;
        effect = new NextActionX2Effect();
    }

    public override void Play(Creature activeCreature, Creature passiveCreature, ActionState actionState)
    {
        effect.ApplyEffect(activeCreature, passiveCreature, actionState);
    }

    //quel serait le meilleur moyen pour récupérer le damage et autres infos sur le power des actions??
    public int GetDamage(Creature activeCreature, Creature passiveCreature)
    {
        return effect.GetEffectPower(activeCreature, passiveCreature);
    }
}
