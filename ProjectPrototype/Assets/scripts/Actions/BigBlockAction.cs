﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigBlockAction : Action
{
    public Effect effect;
    // Start is called before the first frame update
    public BigBlockAction(int blockAmount = 8, string actionName = "Big Defend")
    {
        this.actionName = actionName;
        effect = new GainBlockEffect(blockAmount);
    }
    
    public override void Play(Creature activeCreature, Creature passiveCreature, ActionState actionState)
    {
        effect.ApplyEffect(activeCreature, passiveCreature, actionState);
        activeCreature.DecreaseAp(1);
        //CombatUiManager.main.UpdateTextFields();
    }
}
