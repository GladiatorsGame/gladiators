﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackBlockAction : Action
{
    //Liste d'effets Effects
    public List<Effect> effects;

    // Start is called before the first frame update
    public AttackBlockAction(int damage = 1, int block = 1, string actionName = "Strike")
    {
        this.actionName = actionName;
        effects = new List<Effect>();
        effects.Add(new DealDamageEffect(damage));
        effects.Add(new GainBlockEffect(block));
    }

    public override void Play(Creature activeCreature, Creature passiveCreature, ActionState actionState)
    {
        foreach(Effect effect in effects)
        {
            effect.ApplyEffect(activeCreature, passiveCreature, actionState);
        }
        activeCreature.DecreaseAp(1);
        //CombatUiManager.main.UpdateTextFields();
    }
}
