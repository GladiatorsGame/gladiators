﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creature
{
    public int hpMax;
    public int hpCurrent;
    
    public int apMax;
    public int apCurrent;

    public int cpMax;
    public int cpCurrent;


    public int blockCurrent;

    public int strCurrent;
    public int combatPositionValue;

    public string name;


    public Creature(int hpMax, int apMax, string name)
    {
        this.hpMax = hpMax;
        hpCurrent = hpMax;

        this.apMax = apMax;
        apCurrent = apMax;

        cpMax = 1;
        cpCurrent = cpMax;

        blockCurrent = 0;
        strCurrent = 0;
        combatPositionValue = 2;


        this.name = name;  
    }

    public void IncreaseCurrentHp(int hpAmount)
    {
        hpCurrent = Mathf.Min(hpCurrent + hpAmount, hpMax);
    }

    public void DecreaseCurrentHp(int hpAmount)
    {
        hpCurrent = Mathf.Max(hpCurrent - hpAmount, 0);
    }

    public void ResetHp()
    {
        hpCurrent = hpMax;
    }

    public void IncreaseCurrentAp(int apAmount)
    {
        apCurrent += apAmount;
    }

    public void DecreaseCurrentAp(int apAmount)
    {
        apCurrent = Mathf.Max(apCurrent - apAmount, 0);
    }

    public void ResetAp()
    {
        apCurrent = apMax;
    }

    public void IncreaseCurrentCp(int cpAmount)
    {
        cpCurrent = cpCurrent + cpAmount;
    }

    public void DecreaseCurrentCp(int cpAmount)
    {
        cpCurrent = Mathf.Max(cpCurrent - cpAmount, 0);
    }

    public void ResetCp()
    {
        cpCurrent = cpMax;
    }

    public void ConvertCp()
    {
        IncreaseCurrentCp(1);
        DecreaseCurrentAp(1);
    }



    public void IncreaseBlock(int blockAmount)
    {
        blockCurrent += blockAmount;
    }

    public void DecreaseBlock(int blockAmount)
    {
        blockCurrent = Mathf.Max(blockCurrent - blockAmount, 0);
    }

    public void ResetBlock()
    {
        blockCurrent = 0;
    }

    public void ChangeCombatPositionValue(int amount, int maxPositionValue)
    {
        combatPositionValue = Mathf.Clamp(combatPositionValue + amount, 0, maxPositionValue);
        CombatDistanceManager.main.SetCombatDistance();
        Debug.Log(combatPositionValue);
    }

    public void SetCombatPositionValue(int newFightingRangeValue)
    {
        combatPositionValue = Mathf.Clamp(newFightingRangeValue, 0, CombatDistanceManager.main.roomSize);
        CombatDistanceManager.main.SetCombatDistance();
        Debug.Log(combatPositionValue);
    }

    public void ResetCombatPositionValue()
    {
        combatPositionValue = Mathf.Max(CombatDistanceManager.main.roomSize / 2 - 2, 0);
        CombatDistanceManager.main.SetCombatDistance();
        Debug.Log(combatPositionValue);
    }

}
