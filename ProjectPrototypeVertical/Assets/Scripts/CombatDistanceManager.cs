﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatDistanceManager : MonoBehaviour
{

    static public CombatDistanceManager main;

    private int combatDistanceValue;
    public int roomSize; //4 (tight), 8 (normal), 12 (large)
    public enum combatDistances { Close, Mid, Far };
    private combatDistances combatDistance;

    void Start()
    {
        if (main == null)
        {
            main = this;
        }

        roomSize = MapManager.main.GetMap().currentRoom.roomSize;
    }

    public void SetCombatDistance()
    {
        int playerPositionValue = CreatureManager.main.GetPlayer().combatPositionValue;
        int enemyPositionValue = CreatureManager.main.GetEnemy().combatPositionValue;

        combatDistanceValue = Mathf.Abs(playerPositionValue + enemyPositionValue - roomSize);

        if (combatDistanceValue < 3)
        {
            combatDistance = combatDistances.Close;
        }
        else if (combatDistanceValue < 6)
        {
            combatDistance = combatDistances.Mid;
        }
        else if (combatDistanceValue < 9)
        {
            combatDistance = combatDistances.Far;
        }
    }

    public combatDistances GetCombatDistance()
    {
        return combatDistance;
    }

    public int GetCombatDistanceValue()
    {
        return combatDistanceValue;
    }
}
