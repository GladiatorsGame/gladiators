﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
///  Cooldown Manager qui recoit message d'un slot et qui vérifie si séquence doit être mise en cooldown.
/// </summary>

public class SequenceManager : MonoBehaviour
{
    public static SequenceManager main;
    public List<Sequence> sequences;
    public GameObject sequencePrefab;
    public GameObject sequenceLayoutGroup;

    public Player player;
    public Enemy enemy;

    public int currentSequenceNumber;

    public List<SlotController> sequence0slots;
    public List<SlotController> sequence1slots;
    public List<SlotController> sequence2slots;
    public List<SlotController> sequence3slots;
    public List<SlotController> sequence4slots;

    public Text[] cooldownTexts;

    public int lastPlayedSequence;
    public int lastPlayedSlot;
    

    // Start is called before the first frame update
    void Start()
    {
        if (main == null)
        {
            main = this;
        }
        sequences = new List<Sequence>();

        player = CreatureManager.main.GetPlayer();

        for (int i = 0; i < player.maxNbSequences; i++)
        {
            GameObject newSequence = Instantiate(sequencePrefab, sequenceLayoutGroup.transform);
            List<SlotController> slotControllerList = new List<SlotController>();
            foreach (SlotController slotController in newSequence.GetComponentsInChildren<SlotController>())
            {
                slotController.sequenceNumber = i;
                slotControllerList.Add(slotController);
                //Debug.Log(slotController.slotNumber);
            }
            Text cooldownText = newSequence.transform.Find("TextCooldown").GetComponent<Text>();
            //Debug.Log(cooldownText.gameObject.name);
            
            //actionButtonsList.Add(newButton.GetComponent<ActionButtons>());
            sequences.Add(new Sequence(i, slotControllerList, cooldownText));
        }

        //sequences.Add(new Sequence(0, sequence1slots, cooldownTexts[0]));
        //sequences.Add(new Sequence(1, sequence2slots, cooldownTexts[1]));
        //sequences.Add(new Sequence(2, sequence3slots, cooldownTexts[2]));
        //sequences.Add(new Sequence(3, sequence3slots, cooldownTexts[3]));
        //sequences.Add(new Sequence(4, sequence3slots, cooldownTexts[4]));

    }

    public void CreateSequences()
    {
        for (int i = 0; i < player.maxNbSequences; i++)
        {
            sequences.Add(new Sequence(i, sequence1slots, cooldownTexts[i]));
        }

    }

    public void InitializeCombat()
    {
        player = CreatureManager.main.GetPlayer();
        enemy = CreatureManager.main.GetEnemy();
    }


    //fonction qui decrease de 1 le nombre de tours restants pour les séquences en cooldown
    public void DecreaseCooldowns()
    {
        foreach (Sequence sequence in sequences)
        {
            if (sequence.cooldown)
            {
                if (sequence.remainingCooldown <= 0)
                {
                    sequence.RemoveCooldown();
                }
                else
                { 
                sequence.DecreaseCooldown();
                }

            }
        }
    }

    public void ApplySequenceCooldown(int sequenceNumber, int cooldownDuration)
    {
        if (!sequences[sequenceNumber].cooldown)
        {
            sequences[sequenceNumber].ApplyCooldown(cooldownDuration);
        }
    }

    public void SlotPlayCooldownUpdate(int newSequenceNumber, int newSlotNumber)
    {
        //Si une séquence différente que celle en cours est entamée, applique cooldown
        if (newSequenceNumber != currentSequenceNumber && sequences[currentSequenceNumber].currentSlot > 0 /*&& newSlotNumber == 0*/)
        {
            sequences[currentSequenceNumber].ApplyCooldown(1);
            //code pour bridge
            if (newSlotNumber > 0)
            {
                for(int i = 0; i < newSlotNumber; i++)
                {
                    sequences[newSequenceNumber].slots[i].DisableSlotPlay();
                }
                sequences[newSequenceNumber].currentSlot = newSlotNumber;
            }
            //fin code pour bridge
        }

        currentSequenceNumber = newSequenceNumber;
        sequences[currentSequenceNumber].slots[newSlotNumber].DisableSlotPlay();  //Code fonctionnel sans bridge

        //code pour bridge
        if (newSlotNumber > 1)
        {
            foreach (Sequence sequence in sequences)
            {
                sequence.slots[newSlotNumber - 1].DisableSlotPlay();
            }

        }
        //fin code bridge

        //si c'est la dernière slot de la séquence, la met en cooldown
        if (sequences[currentSequenceNumber].currentSlot >= sequences[currentSequenceNumber].slots.Count - 1)
        {
            sequences[currentSequenceNumber].ApplyCooldown(1);
        }
        //ou si la prochaine slot est vide, donc fin de la séquence, met en cooldown
        else if(sequences[currentSequenceNumber].slots[sequences[currentSequenceNumber].currentSlot + 1].containedAction == null)
        {
            sequences[currentSequenceNumber].ApplyCooldown(1);
        }
        //ou sinon ça poursuit avec la suite de la séquence
        else
        {
            sequences[currentSequenceNumber].currentSlot++;
            sequences[currentSequenceNumber].slots[sequences[currentSequenceNumber].currentSlot].EnableSlotPlay(); //active la prochaine slot dans la séquence

            //code pour bridge
            if(sequences[currentSequenceNumber].currentSlot < sequences[currentSequenceNumber].slots.Count - 1)
            foreach (Sequence sequence in sequences)
            {
                if(sequence.sequenceNumber != currentSequenceNumber && !sequence.cooldown) {
                    if(sequence.slots[sequences[currentSequenceNumber].currentSlot - 1].containedAction != null)
                    {
                        sequence.slots[sequences[currentSequenceNumber].currentSlot - 1].EnableSlotPlay();
                    }
                }
            }
            //fin du code bridge
        }
    }

    public void ActivatePlayMode()
    {
        foreach(Sequence sequence in sequences)
        {
            if (!sequence.cooldown) {
                //sequence.EnableSlotsPlay();
                //code de EnableSlotsPlay() déménagé ici pour cohérence
                foreach (SlotController slot in sequence.slots)
                {
                    if (slot.containedAction == null || slot.slotNumber != sequence.currentSlot)
                    {
                        slot.DisableSlotPlay();
                    }
                    else
                    {
                        slot.EnableSlotPlay();
                    }
                }
                //code pour bridge
                if(sequences[currentSequenceNumber].currentSlot > 0 && sequences[currentSequenceNumber].currentSlot < sequences[currentSequenceNumber].slots.Count - 1)
                {
                    if (sequence.sequenceNumber != currentSequenceNumber)
                    {
                        if (sequence.slots[sequences[currentSequenceNumber].currentSlot - 1].containedAction != null)
                        {
                            sequence.slots[sequences[currentSequenceNumber].currentSlot - 1].EnableSlotPlay();
                        }
                    }
                }
                //fin du bloc bridge
                //fin du bloc déménagé
            }
            else
            {
                sequence.DisableSlotsPlay();
            }
        }
    }

    public void ActivateEditMode()
    {
        foreach (Sequence sequence in sequences)
        {
            foreach (SlotController slot in sequence.slots)
            {
                if (CombatManager.main.currentCombatState == CombatManager.CombatStates.InitialSetup)
                {
                    if (slot.slotNumber < player.initialSequenceLength && slot.sequenceNumber < player.initialNbSequences)
                    {
                        slot.EnableSlotEdit();
                        if (slot.containedAction == null)
                        {
                            break;
                        }  
                    }
                }
                else
                {
                    slot.EnableSlotEdit();
                    if (slot.containedAction == null)
                    {
                        break;
                    }
                }
                             
            }
        }
    }

    public bool CheckInitialSetupComplete()
    {
        if (player.initialNbActionsCurrent <= 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void SaveLastPlayed(int sequenceNumber, int slotNumber)
    {
        lastPlayedSequence = sequenceNumber;
        lastPlayedSlot = slotNumber;
    }

    public SlotController GetSlotController(int sequenceNumber, int slotNumber)
    {
        return sequences[sequenceNumber].GetSlotController(slotNumber);
    }

}
