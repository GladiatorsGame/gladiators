﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Liste de slotController. Doit gérer quel slot est actif.
/// </summary>


public class Sequence
{
    public int sequenceNumber;
    public int currentSlot;
    public bool cooldown; //est-ce que la séquence est en cooldown?
    public int remainingCooldown; //nombre de tours restants si le cooldown est actif
    //cooldown duration serait un paramètre intéressant à moduler par un status dans le combat (2 tours plutôt que 1 tour)
    public Text textCooldown;
    public List<SlotController> slots;

    public Sequence(int newSequenceNumber, List<SlotController> newSlots, Text newTextCooldown)
    {
        sequenceNumber = newSequenceNumber;
        slots = newSlots;
        textCooldown = newTextCooldown;
        currentSlot = 0;
        cooldown = false;
        textCooldown.text = "";

        foreach(SlotController slot in slots)
        {
            slot.sequenceNumber = sequenceNumber;
        }
    }

    public void ApplyCooldown(int duration)
    {
        cooldown = true;
        currentSlot = 0;
        remainingCooldown = duration;
        textCooldown.text = "Cooldown " + duration + " turns";
        
        if(SelectionManager.main.clickMode == SelectionManager.ClickModes.Play)
        {
            DisableSlotsPlay();
        }   
    }

    public void DecreaseCooldown()
    {
        remainingCooldown--;
        textCooldown.text = "Cooldown " + remainingCooldown + " turns";
    }

    public void RemoveCooldown()
    {
        currentSlot = 0;
        textCooldown.text = "";
        //EnableAllSlots();
        cooldown = false;
    }

    public void DisableSlotsPlay()
    {
        foreach(SlotController slot in slots)
        {
            slot.DisableSlotPlay();
        }
    }

    public void EnableSlotsPlay()
    {
        //******************Code déménagé dans SequenceManager pour plus de cohérence

        //foreach (SlotController slot in slots)
        //{
        //    if (slot.containedAction == null || slot.slotNumber != currentSlot)
        //    {
        //        slot.DisableSlotPlay();
        //    }
        //    else
        //    {
        //        slot.EnableSlotPlay();
        //    }
        //}
    }

    public SlotController GetSlotController(int slotNumber)
    {
        if (slotNumber < slots.Count)
        {
            return slots[slotNumber];
        }
        return null;
    }


}
