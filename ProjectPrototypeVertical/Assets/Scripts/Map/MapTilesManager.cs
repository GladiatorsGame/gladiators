﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

public class MapTilesManager : MonoBehaviour
{
    public static MapTilesManager main;
    public List<List<string>> directionCodes;
    public List<Sprite> tileSprites;


    private void Awake()
    {
        main = this;

        directionCodes = new List<List<string>>();
        directionCodes.Add(new List<string> { "0", "2", "4", "6" });
        directionCodes.Add(new List<string> { "1", "3", "5", "7" });
        directionCodes.Add(new List<string> { "01", "23", "45", "67" });
        directionCodes.Add(new List<string> { "02", "24", "46", "06" });
        directionCodes.Add(new List<string> { "03", "25", "47", "16" });
        directionCodes.Add(new List<string> { "04", "26" });
        directionCodes.Add(new List<string> { "05", "27", "14", "36" });
        directionCodes.Add(new List<string> { "07", "12", "34", "56" });
        directionCodes.Add(new List<string> { "13", "35", "57", "17" });
        directionCodes.Add(new List<string> { "15", "37" });
        directionCodes.Add(new List<string> { "012", "234", "456", "067" });
        directionCodes.Add(new List<string> { "013", "235", "457", "167" });
        directionCodes.Add(new List<string> { "014", "236", "045", "267" });
        directionCodes.Add(new List<string> { "015", "237", "145", "367" });
        directionCodes.Add(new List<string> { "016", "023", "245", "467" });
        directionCodes.Add(new List<string> { "017", "123", "345", "567" });
        directionCodes.Add(new List<string> { "024", "246", "046", "026" });
        directionCodes.Add(new List<string> { "025", "247", "146", "036" });
        directionCodes.Add(new List<string> { "027", "124", "346", "056" });
        directionCodes.Add(new List<string> { "034", "256", "047", "126" });
        directionCodes.Add(new List<string> { "035", "257", "147", "136" });
        directionCodes.Add(new List<string> { "037", "125", "347", "156" });
        directionCodes.Add(new List<string> { "057", "127", "134", "356" });
        directionCodes.Add(new List<string> { "135", "357", "157", "137" });

        tileSprites = new List<Sprite>();

        string[] guids = AssetDatabase.FindAssets("t:Sprite", new[] { "Assets/Images/MapTiles" });

        foreach (string guid in guids)
        {
            string assetPath = AssetDatabase.GUIDToAssetPath(guid);
            Sprite sprite = AssetDatabase.LoadAssetAtPath<Sprite>(assetPath);
            tileSprites.Add(sprite);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
        
    }


    public MapTileData GetMapTileData(string directionCode)
    {
        int imageIndex = directionCodes.FindIndex(x => x.Contains(directionCode));
        Sprite tileSprite = null;
        

        int angleIndex = -1;
        int rotationAngle = 0;

        if (imageIndex >= 0)
        {
            angleIndex = directionCodes[imageIndex].FindIndex(x => x == directionCode);
            string spriteName = "MapTile" + directionCodes[imageIndex][0];
            //string spriteName = "MapTile0";  //testing only
            if(tileSprites.FindIndex(x => x.name == spriteName) >= 0)
            {
                tileSprite = tileSprites[tileSprites.FindIndex(x => x.name == spriteName)];
            }
            else
            {
                Debug.Log("Code not found:" + directionCode);
                Debug.Log("Sprite:" + spriteName);
            }
            //tileSprite = tileSprites[Random.Range(0, tileSprites.Count)];
            //tileSprite = null;
        }

        if(imageIndex >= 0 && angleIndex >=0)
        {
            switch (angleIndex)
            {
                case 0: rotationAngle = 0;
                    break;

                case 1: rotationAngle = -90;
                    break;

                case 2: rotationAngle = -180;
                    break;

                case 3: rotationAngle = -270;
                    break;

            }

            MapTileData newMapTileData = new MapTileData(tileSprite, rotationAngle);
            return newMapTileData;
        }

        return new MapTileData(null, 0);
        
    }
}
