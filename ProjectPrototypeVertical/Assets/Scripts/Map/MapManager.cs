﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.SceneManagement;

public class MapManager : MonoBehaviour
{
    public static MapManager main;
    public static Map map;
    public GameObject mapContainer;
    public GameObject mapTilePrefab;
    public GameObject mapPathPrefab;
    public Canvas tilesCanvas;
    //public static List<Room> mapRooms;
    public MapSelectionCircle selectionCircle;
    //private static bool mapGenerated = false;

    public int gridSizeX = 10;
    public int gridSizeY = 5;
    [Range(10, 50)]
    public int mapTileGapSize = 40;
    [Range(0,20)]
    public int randomTileDisplacement = 20;

    public int nbSecondaryBranches = 8; //gives one secondary room each
    public int nbTertiaryBranches = 10; //gives one tertiary room each

    public int roomNumberCounter = 0;
    private int createRoomsRetryCounter = 0;

    private void Awake()
    {
        if (main == null)
        {
            main = this;
            //mapGenerated = false;
        }
        else if (main != this)
        {
            Destroy(gameObject);
        }

    }

    // Start is called before the first frame update
    void Start()
    {
        nbSecondaryBranches = Mathf.Clamp(nbSecondaryBranches, 0, gridSizeX - 2);
        nbTertiaryBranches = Mathf.Clamp(nbTertiaryBranches, 0, nbSecondaryBranches * 2);

        if (map == null)
        {
            Debug.Log("Creating Map");
            
            //DontDestroyOnLoad(mapContainer);
            GenerateNewMap();
        }
        else
        {
            //mapGenerated = true;
            Debug.Log("Map Exists");
            DrawMap();
        }

        SceneManager.LoadScene("RoomEventScene", LoadSceneMode.Additive);
        SceneManager.LoadScene("DialogueScene", LoadSceneMode.Additive);
        SceneManager.SetActiveScene(SceneManager.GetSceneByName("MapScene"));
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            
            ClearAllTiles();
            GenerateNewMap();
        }
    }


    public void GenerateNewMap()
    {
        map = new Map();
        CreateMainPath();
        CreateRooms(nbSecondaryBranches, 1, new Color(1.0f, 0.5f, 0));
        CreateRooms(nbTertiaryBranches, 2, new Color(1.0f, 1.0f, 0));
        map.currentRoom = map.mapRooms[0];
        DrawMap();
        
    }

    public void DrawMap()
    {
        CreateMapTiles();
        CreatePaths();
        foreach (Room room in map.mapRooms)
        {
            if (room.explored)
            {
                room.UnlockConnectedRooms();
            }
        }
    }

    public void CreateMainPath()
    {
        roomNumberCounter = 0;
        //mapRooms = new List<Room>();
        int y;
        Room lastRoom = null;

        for (int x = 0; x < gridSizeX; x++)
        {
            //Si ce n'est pas la première case à gauche de la map
            if (x > 0 && lastRoom != null)
            {
                int random = Random.Range(lastRoom.y - 1, lastRoom.y + 2);
                y = Mathf.Clamp(random, 0, gridSizeY - 1);
                //Debug.Log(random + " " + y);
            }
            else
            {
                y = Random.Range(0, gridSizeY);
            }

            //Création de la Room et ajout à la liste de toutes les Rooms de la map
            Room newRoom = new Room(x, y, 0, roomNumberCounter, randomTileDisplacement);
            map.mapRooms.Add(newRoom);
            roomNumberCounter++;


            if (x > 0 && lastRoom != null)
            {
                //Ajout des connections
                newRoom.AddRoomConnection(lastRoom);
                lastRoom.AddRoomConnection(newRoom);
            }
            lastRoom = newRoom;  
        }

        //vérification pour que la map s'étende sur toute la grid en hauteur
        if(map.mapRooms.Max(room => room.y) - map.mapRooms.Min(room => room.y) < gridSizeY - 1)
        {
            map.mapRooms.Clear();
            CreateMainPath();
        }     
    }

    public void ClearAllTiles()
    {
        foreach (Room room in map.mapRooms)
        {
            Destroy(room.mapTile.gameObject);
        }
        foreach (Transform child in tilesCanvas.transform)
        {
            Destroy(child.gameObject);
        }
        map = null;
        //map.mapRooms.Clear();
    }

    public void CreateRooms(int numberOfBranches, int level, Color roomColor)
    {
        List<Room> possibleBranchingRooms = map.mapRooms.FindAll(x => x.branch == level - 1);
        List<MapCoord> freeTiles = new List<MapCoord>();
        bool allRoomsCreated = true;
        
        //boucle pour chaque Room à créer
        for (int i = 0; i < numberOfBranches; i++)
        {
            bool branchCreated = false;
            int branchingRetryCounter = 0;

            //crée la liste des tuiles d'où partir le branch
            possibleBranchingRooms = map.mapRooms.FindAll(x => x.branch == level - 1 && x.roomConnections.Count < 3);

            //Élimine les premières et dernières tuiles si c'est pour le niveau 1
            if (level == 1)
            {
                possibleBranchingRooms.RemoveAll(x => x.x == 0 || x.x == gridSizeX - 1); //enlève la dernière room du main path pour pas que ça branch de là
                //possibleBranchingRooms.RemoveAll(x => x.x == 0); //enlève la première room du main path pour pas que ça branch de là
            }

            int nbRetries = possibleBranchingRooms.Count;

            //boucle pour retry si pas de tuile disponible autour d'une branchingTile
            while (branchCreated == false && branchingRetryCounter < nbRetries) {

                //Si un pool de tuiles pour le branching a été trouvé
                if (possibleBranchingRooms.Count > 0) {

                    //Parmi ce pool de tuiles, choisit une tuile en random
                    Room branchingRoom = possibleBranchingRooms[Random.Range(0, possibleBranchingRooms.Count)];

                    //cherche toutes les tuiles libres autour de la tuile qui branch
                    for (int x = branchingRoom.x - 1; x <= branchingRoom.x + 1; x++)
                    {
                        for (int y = branchingRoom.y - 1; y <= branchingRoom.y + 1; y++)
                        {
                            if (!map.mapRooms.Exists(room => room.x == x && room.y == y) && x > 0 && x < gridSizeX && y >= 0 && y < gridSizeY)
                            {
                                freeTiles.Add(new MapCoord(x, y));
                            }
                        }
                    }

                    List<MapCoord> deleteList = new List<MapCoord>();
                    ///Debug.Log("Tiles available for (" + branchingRoomCoord.x + "," + branchingRoomCoord.y + ") Level:" + level);

                    //filtrage de la liste de tuiles libres pour éviter croisements
                    foreach (MapCoord freeTile in freeTiles)
                    {
                        int tileUp = freeTile.y + 1;
                        int tileDown = freeTile.y - 1;
                        int tileLeft = freeTile.x - 1;
                        int tileRight = freeTile.x + 1;

                        //si la tuile libre est en diagonale de la branchingTile choisie, applique le filtre
                        if(freeTile.x != branchingRoom.x && freeTile.y != branchingRoom.y) {

                            if(map.mapRooms.Exists(room => room.x == freeTile.x && room.y == tileUp) && map.mapRooms.Exists(room => room.x == tileRight && room.y == freeTile.y))
                            {
                                deleteList.Add(freeTile);
                            }
                            if (map.mapRooms.Exists(room => room.x == freeTile.x && room.y == tileDown) && map.mapRooms.Exists(room => room.x == tileRight && room.y == freeTile.y))
                            {
                                deleteList.Add(freeTile);
                            }
                            if (map.mapRooms.Exists(room => room.x == freeTile.x && room.y == tileDown) && map.mapRooms.Exists(room => room.x == tileLeft && room.y == freeTile.y))
                            {
                                deleteList.Add(freeTile);
                            }
                            if (map.mapRooms.Exists(room => room.x == freeTile.x && room.y == tileUp) && map.mapRooms.Exists(room => room.x == tileLeft && room.y == freeTile.y))
                            {
                                deleteList.Add(freeTile);
                            }
                        }
                    }

                    //Supprime les tuiles causant des croisements de chemins
                    foreach (MapCoord mapTile in deleteList)
                    {
                        //Debug.Log(roomCoord.x + ", " + roomCoord.y);
                        freeTiles.Remove(mapTile);
                    }

                    //Vérifie s'il reste des tuiles libres autour de la tuile choisie pour brancher
                    if (freeTiles.Count > 0)
                    {
                        //Choisit une tuile parmi les tuiles libres pour créer la nouvelle room
                        MapCoord newRoomTile = freeTiles[Random.Range(0, freeTiles.Count)];
                        Room newRoom = new Room(newRoomTile.x, newRoomTile.y, level, roomNumberCounter, randomTileDisplacement);
                        newRoom.AddRoomConnection(branchingRoom);
                        branchingRoom.AddRoomConnection(newRoom);
                        map.mapRooms.Add(newRoom);
                        branchCreated = true;
                        roomNumberCounter++;
                    }
                    //Si aucune tuile libre autour de la tuile choisie pour brancher
                    else
                    {
                        //enlève la tuile choisie de la liste de possibilités
                        possibleBranchingRooms.Remove(branchingRoom);
                        branchingRetryCounter++;
                        if (branchingRetryCounter >= nbRetries)
                        {  
                            allRoomsCreated = false;
                        }
                    }
                }
                //Si aucun pool de tuiles a été trouvé pour le branching
                else
                {
                    allRoomsCreated = false;
                }
                freeTiles.Clear();
            }
        }

        if (!allRoomsCreated)
        {
            Debug.Log("Failed to create all rooms on level " + level);
        }
    }

    //Fonction qui instancie les gameobjects des rooms
    public void CreateMapTiles()
    {
        foreach (Room room in map.mapRooms)
        { 
            //instantiate la bonne image
            GameObject newMapTile = Instantiate(mapTilePrefab, tilesCanvas.transform);
            room.SetMapTile(newMapTile.GetComponent<MapTile>(), mapTileGapSize, gridSizeX, gridSizeY, selectionCircle);            
        }
    }

    public Vector2 ConvertPosition(Vector3 tilePosition)
    {
        return Camera.main.ScreenToWorldPoint(tilePosition);
    }

    //Fonction qui instancie les gameobjects des paths et les oriente correctement
    public void CreatePaths()
    {
        foreach (Room room in map.mapRooms)
        {
            foreach (RoomConnection connection in room.roomConnections)
            {
                if (room.branch < connection.connectedRoom.branch || (room.branch == connection.connectedRoom.branch && room.GetRoomNumber() < connection.connectedRoom.GetRoomNumber()))
                {
                    Vector2 pathPosition = (connection.connectedRoom.mapTile.GetComponent<RectTransform>().position + room.mapTile.GetComponent<RectTransform>().position) / 2;
                    GameObject newPath = Instantiate(mapPathPrefab, tilesCanvas.transform);
                    newPath.transform.position = pathPosition;

                    //tourne le path vers la connected room
                    newPath.transform.up = connection.connectedRoom.mapTile.GetComponent<RectTransform>().position - newPath.GetComponent<RectTransform>().position;
                   
                }
            }
        }
    }

    public Map GetMap()
    {
        return map;
    }

    public void ShowMap()
    {
        mapContainer.SetActive(true);
    }

    public void HideMap()
    {
        mapContainer.SetActive(false);
    }
}

public class MapCoord
{
    public int x;
    public int y;

    public MapCoord(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
}
