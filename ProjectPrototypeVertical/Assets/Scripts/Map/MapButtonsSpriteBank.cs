﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class MapButtonsSpriteBank : MonoBehaviour
{
    public static MapButtonsSpriteBank main;
    public List<Sprite> iconButtonSprites;
    void Awake()
    {
        main = this;

        iconButtonSprites = new List<Sprite>();

        string[] guids = AssetDatabase.FindAssets("t:Sprite", new[] { "Assets/Images/MapButtonIcons" });

        foreach (string guid in guids)
        {
            string assetPath = AssetDatabase.GUIDToAssetPath(guid);
            Sprite sprite = AssetDatabase.LoadAssetAtPath<Sprite>(assetPath);
            iconButtonSprites.Add(sprite);
        }

    }

    public Sprite GetMapButtonIconSprite(RoomContent.RoomContentTypes roomContent)
    {

        Sprite sprite = iconButtonSprites[iconButtonSprites.FindIndex(x => x.name == roomContent.ToString() + "MapButtonIcon")];

        return sprite;
    }

    public Sprite GetMapIconByName(string spriteName)
    {

        Sprite sprite = iconButtonSprites[iconButtonSprites.FindIndex(x => x.name == spriteName)];

        return sprite;
    }

}
