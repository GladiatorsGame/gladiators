﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MapRoomButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    private Color mainColor = Color.white;
    public bool isUnlocked;
    public MapTile mapTile;
    public Sprite unexploredSprite;

    public void Initialize()
    {
        mapTile = GetComponentInParent<MapTile>();

        //ChangeMainColor(Color.white);
      
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (isUnlocked && mapTile.room.explored == false)
        {
            GetComponent<Image>().color = Color.green;
        }
        //Debug.Log("Enter");
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (isUnlocked)
        {
            GetComponent<Image>().color = mainColor;
        }
        //Debug.Log("Exit");
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (isUnlocked && mapTile.room.explored == false)
        {
            GetComponent<Image>().color = Color.grey;
            mapTile.selectionCircle.ChangePosition(mapTile.transform.position);
            mapTile.room.UnlockConnectedRooms();
            mapTile.map.currentRoom = mapTile.room;
            mapTile.room.roomContent.PlayRoomContent();
        }
        //Debug.Log("Click");
    }

    public void ChangeMainColor(Color newColor)
    {
        mainColor = newColor;
        GetComponent<Image>().color = mainColor;
    }

    public void ChangeMainAlpha(float newAlpha)
    {
        Color newColor = mainColor;
        newColor.a = newAlpha;
        mainColor = newColor;
        GetComponent<Image>().color = mainColor;
    }

    //Empêche la slot d'être jouée
    public void DisableMapButton()
    {
        ChangeMainAlpha(0.5f);
        isUnlocked = false;
    }

    //Permet à la slot d'être jouée
    public void EnableMapButton()
    {
        ChangeMainAlpha(1.0f);
        isUnlocked = true;
    }

    public void ChangeIcon(Sprite sprite)
    {
        GetComponent<Image>().sprite = sprite;
        //GetComponent<Image>().sprite = buttonIcons[spriteNumber];

    }
}
