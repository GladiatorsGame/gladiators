﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MapTile : MonoBehaviour
{
    private Color mainColor;
    public Map map;
    public Room room;
    private MapRoomButton button;
    public MapSelectionCircle selectionCircle;

    public void Initialize(Room room)
    {
        map = MapManager.main.GetMap();
        this.room = room;
        mainColor = GetComponent<Image>().color;

        button = GetComponentInChildren<MapRoomButton>();
        button.Initialize();

        if (room.explored)
        {
            UnlockRoom();
        }
        else
        {
            LockRoom();
        }

        if(room.GetRoomNumber() == map.currentRoom.GetRoomNumber())
        {
            selectionCircle.ChangePosition(transform.position);
        }
    }

    public void UnlockRoom()
    {
        //if (!room.explored)
        //{
            button.EnableMapButton();
            Sprite sprite = MapButtonsSpriteBank.main.GetMapButtonIconSprite(room.roomContent.roomContentType);
            button.ChangeIcon(sprite);
        //}
        
    }

    public void LockRoom()
    {
        //ChangeMainAlpha(0.5f);
        button.DisableMapButton();
    }

    public void ExploreRoom()
    {
        button.ChangeMainColor(Color.yellow);  
    }

    public void ChangeMainAlpha(float newAlpha)
    {
        Color newColor = mainColor;
        newColor.a = newAlpha;
        mainColor = newColor;
        GetComponent<Image>().color = mainColor;
    }
}
public class MapTileData
{
    public Sprite sprite;
    public int imageRotation;

    public MapTileData(Sprite sprite, int imageRotation)
    {
        this.sprite = sprite;
        this.imageRotation = imageRotation;
    }
}





