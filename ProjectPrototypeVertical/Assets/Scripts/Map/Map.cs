﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map
{
    public List<Room> mapRooms;
    public Room currentRoom;

    public Map()
    {
        currentRoom = null;
        mapRooms = new List<Room>();
    }
}
