﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;


public class Room
{
    public int x;
    public int y;
    public int branch;
    private int roomNumber;
    public string directionCode;
    public bool explored, unlocked;
    public int roomSize;
    private int randomDisplacementX, randomDisplacementY;

    public RoomContent roomContent;
    public MapTile mapTile;  //un objet qui contient l'image du parcours + bouton pour entrer pis interagir
    //connection avec MapTile pour débloquer les rooms connectées
    public List<RoomConnection> roomConnections;


    public Room(int x, int y, int branch, int roomNumber, int randomDisplacement)
    {
        this.x = x;
        this.y = y;
        this.branch = branch;
        this.roomNumber = roomNumber;
        roomSize = 4 * Random.Range(1, 4);
        randomDisplacementX = Random.Range(-randomDisplacement, randomDisplacement);
        randomDisplacementY = Random.Range(-randomDisplacement, randomDisplacement);
        if(roomNumber == 0)
        {
            explored = true;
            unlocked = true;
        }
        else
        {
            explored = false;
            unlocked = false;
        }
        roomConnections = new List<RoomConnection>();
        GenerateRoomContent();
    }

    public void GenerateRoomContent()
    {
        int randomNumber = Random.Range(0, 100);

        //mettre un room content start pour la première tuile

        switch (branch)
        {
            case 0:
                if(randomNumber < 10)
                {
                    roomContent = new TreasureRoomContent(RoomContent.RoomContentTypes.Treasure);
                }
                else if(randomNumber < 10 + 20)
                {
                    roomContent = new CampfireRoomContent(RoomContent.RoomContentTypes.Campfire);
                }
                else if (randomNumber < 10 + 20 + 10)
                {
                    roomContent = new DialogueRoomContent(RoomContent.RoomContentTypes.Dialogue);
                }
                else if (randomNumber < 10 + 20 + 10 + 40)
                {
                    roomContent = new NormalEnemyRoomContent(RoomContent.RoomContentTypes.NormalEnemy);
                }
                else if (randomNumber < 10 + 20 + 10 + 40 + 10)
                {
                    roomContent = new EliteEnemyRoomContent(RoomContent.RoomContentTypes.EliteEnemy);
                }
                else if (randomNumber < 10 + 20 + 10 + 40 + 10 + 10)
                {
                    roomContent = new RoomContent(RoomContent.RoomContentTypes.Boss);
                }
                break;

            case 1:
                if (randomNumber < 10)
                {
                    roomContent = new TreasureRoomContent(RoomContent.RoomContentTypes.Treasure);
                }
                else if (randomNumber < 10 + 15)
                {
                    roomContent = new CampfireRoomContent(RoomContent.RoomContentTypes.Campfire);
                }
                else if (randomNumber < 10 + 15 + 30)
                {
                    roomContent = new TradeoffRoomContent(RoomContent.RoomContentTypes.Tradeoff);
                }
                else if (randomNumber < 10 + 15 + 30 + 35)
                {
                    roomContent = new NormalEnemyRoomContent(RoomContent.RoomContentTypes.NormalEnemy);
                }
                else if (randomNumber < 10 + 15 + 30 + 35 + 10)
                {
                    roomContent = new EliteEnemyRoomContent(RoomContent.RoomContentTypes.EliteEnemy);
                }
                break;

            case 2:
                if (randomNumber < 35)
                {
                    roomContent = new TreasureRoomContent(RoomContent.RoomContentTypes.Treasure);
                }
                else if (randomNumber < 35 + 15)
                {
                    roomContent = new CampfireRoomContent(RoomContent.RoomContentTypes.Campfire);
                }
                else if (randomNumber < 35 + 15 + 10)
                {
                    roomContent = new TradeoffRoomContent(RoomContent.RoomContentTypes.Tradeoff);
                }
                else if (randomNumber < 35 + 15 + 10 + 20)
                {
                    roomContent = new NormalEnemyRoomContent(RoomContent.RoomContentTypes.NormalEnemy);
                }
                else if (randomNumber < 35 + 15 + 10 + 20 + 20)
                {
                    roomContent = new EliteEnemyRoomContent(RoomContent.RoomContentTypes.EliteEnemy);
                }
                break;

            default:
                roomContent = new RoomContent(RoomContent.RoomContentTypes.NormalEnemy);
                break;
        }

        if (roomNumber == 0)
        {
            roomContent = new RoomContent(RoomContent.RoomContentTypes.StartLevel1);
        }
    }

    public void SetMapTile(MapTile newMapTile, int mapTileGapSize, int gridSizeX, int gridSizeY, MapSelectionCircle selectionCircle)
    {
        this.mapTile = newMapTile;

        float sizeX = mapTile.GetComponent<RectTransform>().sizeDelta.x + mapTileGapSize;
        float sizeY = mapTile.GetComponent<RectTransform>().sizeDelta.y + mapTileGapSize;

        mapTile.transform.position = new Vector2(mapTile.transform.position.x + sizeX * x - sizeX * gridSizeX / 2 + sizeX / 2 + randomDisplacementX, mapTile.transform.position.y + sizeY * y - sizeY * gridSizeY / 2 + sizeY / 2 + randomDisplacementY);

        mapTile.gameObject.name = "(" + x + "," + y + ")";

        MapTileData mapTileData = MapTilesManager.main.GetMapTileData(directionCode);

        mapTile.GetComponent<Image>().sprite = mapTileData.sprite;
        mapTile.GetComponent<RectTransform>().rotation = Quaternion.Euler(0, 0, mapTileData.imageRotation);
        mapTile.GetComponentInChildren<MapRoomButton>().GetComponent<RectTransform>().rotation = Quaternion.Euler(0, 0, 0);

        mapTile.selectionCircle = selectionCircle;

        mapTile.Initialize(this);
    }

    public void UnlockConnectedRooms()
    {
        foreach(RoomConnection roomConnection in roomConnections)
        {
            roomConnection.connectedRoom.UnlockRoom();
        }
        explored = true;
        mapTile.ExploreRoom();
    }

    public void UnlockRoom()
    {
        unlocked = true;
        mapTile.UnlockRoom();
    }

    public void AddRoomConnection(Room connectedRoom)
    {
        int connectionDirection = 8;

        if (connectedRoom.x == x && connectedRoom.y > y)
        {
            connectionDirection = 0;
        }
        else if (connectedRoom.x > x && connectedRoom.y > y)
        {
            connectionDirection = 1;
        }
        else if (connectedRoom.x > x && connectedRoom.y == y)
        {
            connectionDirection = 2;
        }
        else if (connectedRoom.x > x && connectedRoom.y < y)
        {
            connectionDirection = 3;
        }
        else if (connectedRoom.x == x && connectedRoom.y < y)
        {
            connectionDirection = 4;
        }
        else if (connectedRoom.x < x && connectedRoom.y < y)
        {
            connectionDirection = 5;
        }
        else if (connectedRoom.x < x && connectedRoom.y == y)
        {
            connectionDirection = 6;
        }
        else if (connectedRoom.x < x && connectedRoom.y > y)
        {
            connectionDirection = 7;
        }
        else
        {
            Debug.Log("Direction not found!");
        }

        if (connectionDirection != 8)
        {
            roomConnections.Add(new RoomConnection(connectionDirection, connectedRoom));
            roomConnections.OrderBy(connection => connection.direction).ToList();
            roomConnections.Sort((connection1, connection2) => connection1.direction.CompareTo(connection2.direction));

            //update du directionCode de la room
            directionCode = "";
            foreach (RoomConnection roomConnection in roomConnections)
            {
                directionCode += roomConnection.direction.ToString();
            }
        }
        else
        {
            Debug.Log("Connection creation failed");
        }
    }
    public int GetRoomNumber()
    {
        return roomNumber;
    }
}

public class RoomConnection
{
    public int direction;
    public Room connectedRoom;

    public RoomConnection(int direction, Room connectedRoom)
    {
        this.direction = direction;
        this.connectedRoom = connectedRoom;
    }
}


