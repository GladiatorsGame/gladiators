﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RoomEventManager : MonoBehaviour
{
    public static RoomEventManager main;
    public Canvas canvas;
    public GameObject sceneContainer;

    [Header("Treasure")]
    public GameObject treasureScreen;
    public Button buttonTreasure;
    public Text textTreasure;
    
    [Header("Campfire")]
    public GameObject campfireScreen;
    public Button buttonCampfireRest, buttonCampfireMeditate, buttonCampfireBackToMap;
    public Text textCampfire;

    [Header("Tradeoff")]
    public GameObject tradeoffScreenCrystal;
    public GameObject tradeoffScreenBowl;
    public GameObject tradeoffScreenWaterfall;
    public Button buttonTradeoffCrystal, buttonTradeoffBowl, buttonTradeoffWaterfall;

    public Player player;

    void Awake()
    {
        if(main == null)
        {
            main = this;
            //DontDestroyOnLoad(this);
        }
    }


    void Start()
    {
        player = CreatureManager.main.GetPlayer();
        buttonTreasure.onClick.AddListener(ButtonBackToMapPressed);

        buttonCampfireRest.onClick.AddListener(ButtonCampfireRestPressed);
        buttonCampfireMeditate.onClick.AddListener(ButtonCampfireMeditatePressed);
        buttonCampfireBackToMap.onClick.AddListener(ButtonBackToMapPressed);
        
        buttonTradeoffCrystal.onClick.AddListener(ButtonBackToMapPressed);
        buttonTradeoffBowl.onClick.AddListener(ButtonBackToMapPressed);
        buttonTradeoffWaterfall.onClick.AddListener(ButtonBackToMapPressed);
    }

    public void SetupScene()
    {
        MapManager.main.HideMap();
        SceneManager.SetActiveScene(SceneManager.GetSceneByName("RoomEventScene"));
        sceneContainer.SetActive(true);
        campfireScreen.SetActive(false);
        treasureScreen.SetActive(false);
        tradeoffScreenBowl.SetActive(false);
        tradeoffScreenCrystal.SetActive(false);
        tradeoffScreenWaterfall.SetActive(false);
    }


    public void SetupTreasureScreen()
    {
        SetupScene();

        treasureScreen.SetActive(true);
        TreasureRoomContent treasureRoomContent = MapManager.main.GetMap().currentRoom.roomContent as TreasureRoomContent;
        textTreasure.text = treasureRoomContent.ApplyTreasureContent();
    }

    public void SetupCampfireScreen()
    {
        SetupScene();

        textCampfire.text = "You have found a safe place to camp.  Will you sleep to recover from your wounds or meditate and try to remember your past?";
        campfireScreen.SetActive(true);
        buttonCampfireBackToMap.gameObject.SetActive(false);
        buttonCampfireRest.gameObject.SetActive(true);
        buttonCampfireMeditate.gameObject.SetActive(true);
        
        campfireScreen.transform.position = canvas.transform.position;
    }

    public void SetupTradeoffScreen()
    {
        SetupScene();

        TradeoffRoomContent tradeoffRoomContent = MapManager.main.GetMap().currentRoom.roomContent as TradeoffRoomContent;
        switch (tradeoffRoomContent.GetSettingNumber())
        {
            case 0:
                tradeoffScreenCrystal.SetActive(true);
                tradeoffScreenCrystal.transform.position = canvas.transform.position;
                break;

            case 1:
                tradeoffScreenBowl.SetActive(true);
                tradeoffScreenBowl.transform.position = canvas.transform.position;
                break;

            case 2:
                tradeoffScreenWaterfall.SetActive(true);
                tradeoffScreenWaterfall.transform.position = canvas.transform.position;
                break;
        }
    }


    public void ButtonBackToMapPressed()
    {
        sceneContainer.SetActive(false);
        MapManager.main.ShowMap();
        SceneManager.SetActiveScene(SceneManager.GetSceneByName("MapScene"));
    }

    public void ButtonCampfireRestPressed()
    {
        ResolveCampfire(CampfireRoomContent.CampfireOption.Rest);
    }

    public void ButtonCampfireMeditatePressed()
    {
        ResolveCampfire(CampfireRoomContent.CampfireOption.Meditate);
    }

    public void ResolveCampfire(CampfireRoomContent.CampfireOption campfireOption)
    {
        CampfireRoomContent campfireRoomContent = MapManager.main.GetMap().currentRoom.roomContent as CampfireRoomContent;
        textCampfire.text = campfireRoomContent.ApplyCampfireContent(campfireOption);
        buttonCampfireRest.gameObject.SetActive(false);
        buttonCampfireMeditate.gameObject.SetActive(false);
        buttonCampfireBackToMap.gameObject.SetActive(true);
    }

    
}
