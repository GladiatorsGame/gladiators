﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomContent
{
    public enum RoomContentTypes { NormalEnemy, EliteEnemy, Campfire, Dialogue, Treasure, Tradeoff, Boss, StartLevel1 };
    public RoomContentTypes roomContentType;

    public RoomContent(RoomContentTypes roomContentType)
    {
        this.roomContentType = roomContentType;
    }

    public virtual void PlayRoomContent()
    {

    }

}
