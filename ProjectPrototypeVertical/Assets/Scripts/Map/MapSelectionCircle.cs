﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapSelectionCircle : MonoBehaviour
{

    public void ChangePosition(Vector2 newPosition)
    {
        transform.position = newPosition;
    }
}
