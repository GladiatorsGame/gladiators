﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrusherEnemy : Enemy
{
    public CrusherEnemy():base(Random.Range(100, 106), "Crusher")
    {
        enemyActions = new List<Action>();
        enemyActions.Add(new BlockMoveForwardAction(15, 2, "Block\nMove"));
        enemyActions.Add(new AttackAction(12, "Attack"));
        enemyActions.Add(new CloseAttackAction(30, "Crushing Attack"));
        enemyActions.Add(new BlockAction(8, "Block\nBuff"));
        
    }
    public override void SetNextAction()
    {
        if(CombatDistanceManager.main.GetCombatDistance() != CombatDistanceManager.combatDistances.Close)
        {
            int r = Random.Range(0, 100);
            if (r < 80)
            {
                nextActionNumber = 0; //Advance + block
            }
            else if (r < 80 + 20)
            {
                nextActionNumber = 1;//simple attack
            }
        }
        else
        {
            int r = Random.Range(0, 100);
            if (r < 65)
            {
                nextActionNumber = 2;//crushing attack
            }
            else if (r < 65 + 35)
            {
                nextActionNumber = 3;//attack + STR buff
            }
        }
        
    }

    public override void PlayAction()
    {
        if (nextActionNumber == 3)
        {
            strCurrent += 3;
        }
        enemyActions[nextActionNumber].Play(this, CreatureManager.main.GetPlayer(), enemyActionState);
    }

    public override Enemy GetCopy()
    {
        return new CrusherEnemy();
    }
}
