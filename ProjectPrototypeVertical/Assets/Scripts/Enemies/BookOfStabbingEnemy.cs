﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BookOfStabbingEnemy : Enemy
{
    int multiStabCount;

    public BookOfStabbingEnemy():base(Random.Range(160, 163), "Book of Stabbing")
    {
        multiStabCount = 0;
        enemyActions = new List<Action>();
        enemyActions.Add(new AttackAction(6 * (2 + multiStabCount), "Multi-Stab"));
        enemyActions.Add(new AttackAction(21, "Single Stab"));        
    }
    public override void SetNextAction()
    {
        {
            int r = Random.Range(0, 100);
            if (r < 85)
            {
                nextActionNumber = 0;//Multi-Stab
            }
            else if (r < 85 + 15)
            {
                nextActionNumber = 1;//Single Stab
            }
        } 
    }

    public override void PlayAction()
    {
        
        enemyActions[nextActionNumber].Play(this, CreatureManager.main.GetPlayer(), enemyActionState);
        if (nextActionNumber == 0)
        {
            multiStabCount++;
            enemyActions[0] = new AttackAction(6 * (2 + multiStabCount), "Multi-Stab");
        }
    }

    public override Enemy GetCopy()
    {
        return new BookOfStabbingEnemy();
    }
}
