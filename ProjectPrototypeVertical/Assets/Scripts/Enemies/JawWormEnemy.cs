﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JawWormEnemy : Enemy
{
    public JawWormEnemy():base(Random.Range(40, 45), "Jaw Worm")
    {
        enemyActions = new List<Action>();
        enemyActions.Add(new AttackAction(11, "Attack"));
        enemyActions.Add(new AttackBlockAction(7, 5, "Attack\nBlock"));
        enemyActions.Add(new BlockAction(6, "Buff\nBlock"));
    }
    //public JawWormEnemy(JawWormEnemy jawWormEnemyCopy) : base(Random.Range(40, 45), "Jaw Worm")
    //{
    //    enemyActions = new List<Action>();
    //    enemyActions.Add(new AttackAction(11, "Attack"));
    //    enemyActions.Add(new AttackBlockAction(7, 5, "Attack\nBlock"));
    //    enemyActions.Add(new BlockAction(6, "Buff\nBlock"));
    //}
    public override void SetNextAction()
    {
        int r = Random.Range(0, 100);
        if (r < 45)
        {
            nextActionNumber = 0;
        }
        else if (r < 45 + 30)
        {
            nextActionNumber = 1;
        }
        else if (r < 45 + 30 + 25)
        {
            nextActionNumber = 2;
        }
    }

    public override void PlayAction()
    {
        if (nextActionNumber == 2)
        {
            strCurrent += 3;
        }
        enemyActions[nextActionNumber].Play(this, CreatureManager.main.GetPlayer(), enemyActionState);
    }

    public override Enemy GetCopy()
    {
        return new JawWormEnemy();
    }
}
