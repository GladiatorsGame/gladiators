﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Player : Creature
{
    public int initialSequenceLength;
    public IDictionary<Action, int> playerActionSet;
    public int initialNbSequences;  //pour tests de fun
    public int initialNbActionsMax;//pour tests de fun
    public int initialNbActionsCurrent;//pour tests de fun
    public int maxSequenceLength; //pour tests de fun
    public int maxNbSequences;
    public PlayerActionSetComparer comparer;

    public int fightingPosition;
    public enum fightingRanges { Close, Mid, Far};
    public fightingRanges fightingRange;



    public Player(int hpMax, int apMax, string name): base(hpMax, apMax, name)
    {
        comparer = new PlayerActionSetComparer();
        playerActionSet = new Dictionary<Action, int>(comparer);
        playerActionSet.Add(new AttackAction(), 5);
        playerActionSet.Add(new BigAttackAction(), 1);
        playerActionSet.Add(new BlockAction(), 4);
        playerActionSet.Add(new BigBlockAction(), 1);
        playerActionSet.Add(new ChargeAttackAction(), 1);
        playerActionSet.Add(new MoveForwardAction(), 1);
        playerActionSet.Add(new MoveBackAction(), 1);
        playerActionSet.Add(new KickAttackAction(), 1);
        playerActionSet.Add(new CloseAttackAction(), 1);
        playerActionSet.Add(new NextAttackX2Action(), 1);


        initialSequenceLength = 2; //longueur maximale de la sequence pour setup du combat
        initialNbSequences = 3; //nombre de sequences disponibles pour setup du combat
        initialNbActionsMax = 5; //nombre d'actions à placer au setup du combat
        maxNbSequences = 5; //nombre de sequences disponibles au total pour le combat
        initialNbActionsCurrent = initialNbActionsMax;
    }

    public void IncreaseInitialNbActions(int amount)
    {
        initialNbActionsCurrent = Mathf.Min(initialNbActionsCurrent + amount, initialNbActionsMax);
    }

    public void DecreaseInitialNbActions(int amount)
    {
        initialNbActionsCurrent = Mathf.Max(initialNbActionsCurrent - amount, 0);
    }

    public void ResetInitialNbActions()
    {
        initialNbActionsCurrent = initialNbActionsMax;
    }

    public void AddPlayerAction(Action newAction)
    {
        if (playerActionSet.Keys.Contains(newAction))
        {
            //player.playerActions.Add(new AttackAction(), 1);
            playerActionSet[newAction]++;
            Debug.Log(newAction.actionName + " already existing in action list");
        }
        else
        {
            playerActionSet.Add(newAction, 1);
            Debug.Log("Not found in action list, adding " + newAction.actionName);
        }

    }


   

    
    //ajouter une liste d'actions disponibles (pool)

    //avoir une liste d'actions complète potentiellement réalisables, avec leurs valeurs associées.
}
