﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemy : Creature
{ 
    public List<Action> enemyActions;
    public int nextActionNumber;
    public ActionState enemyActionState;

    public Enemy(int hpMax, string name) : base(hpMax, 0, name)
    {
        nextActionNumber = 0;
        enemyActionState = new ActionState();
    }

    public abstract void SetNextAction();


    public abstract void PlayAction();

    public abstract Enemy GetCopy();
    
}
