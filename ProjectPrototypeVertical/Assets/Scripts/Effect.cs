﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Effect
{
    public abstract void ApplyEffect(Creature activeCreature, Creature passiveCreature, ActionState actionState);

    public virtual int GetEffectPower(Creature activeCreature, Creature passiveCreature)
    {
        return 0;
    }
}
