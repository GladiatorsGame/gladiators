﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatureManager : MonoBehaviour
{
    public static CreatureManager main;
    public static Player player;
    public static Enemy enemy;
    public List<Enemy> normalEnemyList, eliteEnemyList;

    // Start is called before the first frame update

    private void Awake()
    {
        if (main == null)
        {
            main = this;
            DontDestroyOnLoad(this);
            //mapGenerated = false;
        }
        else if (main != this)
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
   
            normalEnemyList = new List<Enemy>();
            eliteEnemyList = new List<Enemy>();

            normalEnemyList.Add(new JawWormEnemy());

            eliteEnemyList.Add(new CrusherEnemy());
            eliteEnemyList.Add(new BookOfStabbingEnemy());

        
        if(player == null)
        {
            player = new Player(80, 4, "Player");
        }

        

        //int r = Random.Range(0, 100);
        //if (r < 50)
        //{
        //    enemy = enemyList[0];
        //}
        //else if(r < 50 + 35)
        //{
        //    enemy = enemyList[1];
        //}
        //else if (r < 50 + 35 + 15)
        //{
        //    enemy = enemyList[2];
        //}
    }

    public Player GetPlayer()
    {
        return player;
    }

    public Enemy GetEnemy()
    {
        return enemy;
    }

    public void SetEnemy(Enemy newEnemy)
    {
        enemy = newEnemy;
    }

    public Enemy GetNormalEnemy()
    {
        int randomEnemyNumber = Random.Range(0, normalEnemyList.Count);
        return normalEnemyList[randomEnemyNumber].GetCopy();
    }

    public Enemy GetEliteEnemy()
    {
        int randomEnemyNumber = Random.Range(0, eliteEnemyList.Count);
        return eliteEnemyList[randomEnemyNumber];
    }
}
