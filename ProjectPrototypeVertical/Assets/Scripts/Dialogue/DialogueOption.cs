﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueOption
{

    public string text;
    public int destinationID = -1;

    public DialogueOption(string text, int destinationID)
    {
        this.text = text;
        this.destinationID = destinationID;
    }
}
