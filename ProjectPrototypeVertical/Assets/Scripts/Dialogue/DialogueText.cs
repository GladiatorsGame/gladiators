﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueText
{
    public DialogueCharacter speaker;
    public string text;

    public DialogueText(DialogueCharacter speaker, string text)
    {
        this.speaker = speaker;
        this.text = text;
    }
}
