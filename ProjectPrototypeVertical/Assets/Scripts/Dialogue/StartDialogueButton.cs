﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartDialogueButton : MonoBehaviour
{
    Button button;
    // Start is called before the first frame update
    void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(ButtonPressed);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void ButtonPressed()
    {
        DialogueManager.main.SetDialogue(0);
        DialogueManager.main.SetNode(0);
        DialogueManager.main.StartDialogue();
    }
}
