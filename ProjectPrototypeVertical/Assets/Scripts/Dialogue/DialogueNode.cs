﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueNode
{
    public int nodeID = -1;

    public Queue<DialogueText> dialogueLines;

    public List<DialogueOption> options;

    public DialogueNode(Queue<DialogueText> text, List<DialogueOption> options, int nodeID)
    {
        this.dialogueLines = text;
        this.options = options;
        this.nodeID = nodeID;
    }
}
