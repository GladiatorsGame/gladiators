﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DialogueManager : MonoBehaviour
{
    public static DialogueManager main;
    public List<Dialogue> dialogues;
    public List<DialogueCharacter> dialogueCharactersList;

    public GameObject sceneContainer;

    public Text textDialogueBox;
    public Text textSpeakerName;
    public Text textNodeID;
    public TextOptionController[] textOptions;

    public TextAsset[] textFiles;

    private string[] lineSeparator;
    private string[] sectionSeparator;
    private string[] optionSeparator;
    private string[] nodeSeparator;

    private int currentDialogueID = -1;
    private int currentNodeID = -1;
    private bool dialogueStarted = false;

    void Awake()
    {
        if(main == null)
        {
            main = this;
            //DontDestroyOnLoad(this);
        }
        else if(main != this)
        {
            //Destroy(gameObject);
        }

        //QualitySettings.vSyncCount = 0;  // VSync must be disabled
        //Application.targetFrameRate = -1;

    }
    // Start is called before the first frame update
    void Start()
    {
        //Création des séparateurs de blocs
        sectionSeparator = new string[] { "//" };
        lineSeparator = new string[] { Environment.NewLine };
        optionSeparator = new string[] { "->" };
        nodeSeparator = new string[] { Environment.NewLine + Environment.NewLine };

        //Création de la liste de tous les dialogues
        dialogues = new List<Dialogue>();
        
        //Création des personnages (liste de tous les personnages participant à au moins une conversation)
        dialogueCharactersList = new List<DialogueCharacter>();
        dialogueCharactersList.Add(new DialogueCharacter("Warrior", Color.white));
        dialogueCharactersList.Add(new DialogueCharacter("Philosopher", new Color(1, 0.5f, 0)));
        dialogueCharactersList.Add(new DialogueCharacter("Creature", Color.blue));

        foreach(TextAsset textAsset in textFiles)
        {
            dialogues.Add(new Dialogue());
            CreateDialogue(textAsset.text);
        }
    }

    private void Update()
    {
        if(dialogueStarted && Input.GetMouseButtonDown(0))
        {
            DisplayNextLine();
        }

    }

    public void SetDialogue(int newDialogueID)
    {
        currentDialogueID = newDialogueID;
    }

    public void SetNode(int newNodeID)
    {
        currentNodeID = newNodeID;
    }

    public void StartDialogue()
    {
        textSpeakerName.text = "";
        textDialogueBox.text = "";

        foreach (TextOptionController textOption in textOptions)
        {
            textOption.ChangeText("");
        }
        dialogueStarted = true;
        DisplayNextLine();
    }

    public void DisplayNextLine()
    {
        if(dialogues[currentDialogueID].nodes[currentNodeID].dialogueLines.Count <= 0)
        {
            EndDialogue();
            DisplayOptions();
            return;
        }
        DialogueText dialogueText = dialogues[currentDialogueID].nodes[currentNodeID].dialogueLines.Dequeue();

        string speakerName = dialogueText.speaker.name;
        Color textColor = dialogueText.speaker.textColor;
        string text = dialogueText.text;

        textSpeakerName.text = speakerName;
        textDialogueBox.color = textColor;
        //textDialogueBox.text = text;
        StopAllCoroutines();
        StartCoroutine(TypeText(text));
        textNodeID.text = currentNodeID.ToString();
    }

    IEnumerator TypeText (string text)
    {
        textDialogueBox.text = "";

        float counter = 0;
        int charIndex = 0;
        int lastCharIndex = 0;
        int typeSpeed = 60;

        while (charIndex < text.ToCharArray().Length)
        {
            counter = counter + typeSpeed * Time.deltaTime;
            charIndex = (int)Mathf.Floor(counter);
            charIndex = Mathf.Min(charIndex, text.ToCharArray().Length);
            for (int i = lastCharIndex; i < charIndex; i++)
            {
                textDialogueBox.text += text.ToCharArray()[i];
            }
            lastCharIndex = charIndex;
            yield return null;
        }
    }

    public void EndDialogue()
    {
        dialogueStarted = false;
        Debug.Log("Dialogue End");
    }

    public bool GetDialogueStarted()
    {
        return dialogueStarted;
    }

    public void DisplayOptions()
    {
        for (int i = 0; i < dialogues[currentDialogueID].nodes[currentNodeID].options.Count; i++)
        {
            if (i < textOptions.Length)
            {
                int nodeID = dialogues[currentDialogueID].nodes[currentNodeID].options[i].destinationID;
                textOptions[i].SetLinkedNode(nodeID);

                string text = (i + 1) + ". " + dialogues[currentDialogueID].nodes[currentNodeID].options[i].text;
                textOptions[i].ChangeText(text);
                
            }
        }
    }

    public void ExitDialogue()
    {
        SceneManager.LoadScene("MapScene");
    }


    public void CreateDialogue(string textFile)
    {
        
        string[] nodes = SplitString(textFile, nodeSeparator);

        //Code pour inclure personnages dans la conversation
        string[] characterNames = SplitString(nodes[0], lineSeparator);

        DialogueCharacter[] dialogueCharacters = new DialogueCharacter[characterNames.Length];
        string[] characterIDs = new string[characterNames.Length];

        for (int i = 0; i < dialogueCharacters.Length; i++)
        {
            dialogueCharacters[i] = dialogueCharactersList.Find(x => x.name == characterNames[i]);
            characterIDs[i] = "[" + characterNames[i] + "]";
        }

        for (int i = 1; i < nodes.Length; i++)
        {
            //Sépare en 3 sections (Node info, Dialogue, Options)
            string[] sections = SplitStringCount(nodes[i], sectionSeparator, 3);

            //Enlève les keywords
            sections[0] = sections[0].Replace("[Node]=", "");
            sections[1] = sections[1].Replace("[Dialogue]", "");
            sections[2] = sections[2].Replace("[Options]", "");

            //NODE
            //Récupère ID du Node
            int nodeID = int.Parse(sections[0]);

            //DIALOGUE
            //Sépare la section dialogue en lignes
            string[] lines = SplitString(sections[1], lineSeparator);

            //Création d'une Queue de dialogueTexts
            Queue<DialogueText> dialogueTexts = CreateDialogueQueue(characterIDs, dialogueCharacters, lines);
              
            //OPTIONS
            //Sépare la section options en lignes
            lines = SplitString(sections[2], lineSeparator);

            //Prépare une liste d'options à envoyer en param
            List<DialogueOption> dialogueOptions = new List<DialogueOption>();

            //Récupère le texte et le ID de chaque option
            foreach(string line in lines)
            {
                string[] option = SplitStringCount(line, optionSeparator, 2);

                string optionText = option[0];
                int destinationID = int.Parse(option[1]);

                dialogueOptions.Add(new DialogueOption(optionText, destinationID));
            }

            //Création d'un nouveau Node au dernier dialogue dans la liste
            dialogues[dialogues.Count - 1].nodes.Add(new DialogueNode(dialogueTexts, dialogueOptions, nodeID));
        }
    }


    public string[] SplitString(string text, string[] separator)
    {
        return text.Split(separator, StringSplitOptions.RemoveEmptyEntries);
    }

    public string[] SplitStringCount(string text, string[] separator, int count)
    {
        return text.Split(separator, count, StringSplitOptions.RemoveEmptyEntries);
    }

    public Queue<DialogueText> CreateDialogueQueue(string[] characterIDs, DialogueCharacter[] characters, string[] lines)
    {

        Queue<DialogueText> dialogueQueue = new Queue<DialogueText>();

        foreach (string line in lines)
        {
            foreach (string characterID in characterIDs)
            {
                if (line.StartsWith(characterID))
                {
                    DialogueCharacter character = characters[Array.IndexOf(characterIDs, characterID)];
                    dialogueQueue.Enqueue(new DialogueText(character, line.Replace(characterID, "")));
                }
            }
        }

        return dialogueQueue;
    }

    public void SetupDialogue(int dialogueID)
    {
        SetDialogue(dialogueID);
        SetNode(0);
        MapManager.main.HideMap();
        SceneManager.SetActiveScene(SceneManager.GetSceneByName("DialogueScene"));
        sceneContainer.SetActive(true);
        StartDialogue();
        
    }
}
