﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//classe qui contient un personnage de dialogue et ses paramètres de texte (couleur, font, etc.)
public class DialogueCharacter
{
    public string name;
    public Color textColor;
    //public Font font;
    //public Image portrait;



    public DialogueCharacter(string name, Color textColor)
    {
        this.name = name;
        this.textColor = textColor;
    }
}
