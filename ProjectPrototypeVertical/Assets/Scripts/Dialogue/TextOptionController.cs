﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TextOptionController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    private Text textOption;
    private int linkedNode = -1;

    // Start is called before the first frame update
    private void Awake()
    {
        textOption = GetComponent<Text>();
        textOption.color = Color.white;
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        textOption.color = Color.red;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        textOption.color = Color.white;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (!DialogueManager.main.GetDialogueStarted())
        {
            if (linkedNode >= 0)
            {
                DialogueManager.main.SetNode(linkedNode);
                DialogueManager.main.StartDialogue();
            }
            else
            {
                DialogueManager.main.ExitDialogue();
            }
        }   
    }

    public void ChangeText(string newText)
    {
        textOption.text = newText;
    }

    public void SetLinkedNode(int nodeID)
    {
        linkedNode = nodeID;
    }
}
