﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class NormalEnemyRoomContent : RoomContent
{
    public Enemy containedEnemy;
    public NormalEnemyRoomContent(RoomContentTypes roomContentType) : base(roomContentType)
    {
        containedEnemy = CreatureManager.main.GetNormalEnemy();
    }

    public override void PlayRoomContent()
    {
        Debug.Log(containedEnemy.name);
        CreatureManager.main.SetEnemy(containedEnemy);
        SceneManager.LoadScene("CombatScene");
    }
}
