﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TradeoffRoomContent : RoomContent
{
    private int settingNumber;
    public TradeoffRoomContent(RoomContentTypes roomContentType) : base(roomContentType)
    {
        settingNumber = Random.Range(0, 3);
    }

    public override void PlayRoomContent()
    {
        //SceneManager.LoadScene("RoomEventScene");
        RoomEventManager.main.SetupTradeoffScreen();
    }

    public int GetSettingNumber()
    {
        return settingNumber;
    }

}
