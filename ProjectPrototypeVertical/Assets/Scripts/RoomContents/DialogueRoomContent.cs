﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class DialogueRoomContent : RoomContent
{
    public int dialogueID;

    public DialogueRoomContent(RoomContentTypes roomContentType) : base(roomContentType)
    {
        dialogueID = 0;
    }

    public override void PlayRoomContent()
    {
        DialogueManager.main.SetupDialogue(dialogueID);
    }
}
