﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class EliteEnemyRoomContent : RoomContent
{
    public Enemy containedEnemy;
    public EliteEnemyRoomContent(RoomContentTypes roomContentType) : base(roomContentType)
    {
        containedEnemy = CreatureManager.main.GetEliteEnemy();
    }

    public override void PlayRoomContent()
    {
        Debug.Log(containedEnemy.name);
        CreatureManager.main.SetEnemy(containedEnemy);
        SceneManager.LoadScene("CombatScene");
    }
}
