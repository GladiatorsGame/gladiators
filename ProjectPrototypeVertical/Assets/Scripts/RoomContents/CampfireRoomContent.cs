﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class CampfireRoomContent : RoomContent
{
    public Player player;
    public Action containedAction;
    public enum CampfireOption { Rest, Meditate};


    public CampfireRoomContent(RoomContentTypes roomContentType) : base(roomContentType)
    {
        containedAction = null;
        player = CreatureManager.main.GetPlayer();

        int randomNumber = UnityEngine.Random.Range(0, 100);

        if (randomNumber < 25)
        {
            int randomActionNumber = UnityEngine.Random.Range(0, player.playerActionSet.Count);

            List<Action> actionList = new List<Action>(player.playerActionSet.Keys);

            containedAction = actionList[randomActionNumber].GetCopy();
        }
    }

    public override void PlayRoomContent()
    {
        //communiquer direct au RoomEventManager de préparer le bon screen et le bon UI
        //SceneManager.SetActiveScene(SceneManager.GetSceneByName("RoomEventScene"));
        RoomEventManager.main.SetupCampfireScreen();
    }

    public string ApplyCampfireContent(CampfireOption campfireOption)
    {
        string campfireText;

        if (campfireOption == CampfireOption.Rest)
        {
            player.IncreaseCurrentHp(player.hpCurrent / 2);
            campfireText = "You feel rested.";
            return campfireText;
        }
        else
        {
            if (containedAction != null)
            {
                player.AddPlayerAction(containedAction);
                campfireText = "You remembered a move! \n <b>" + containedAction.actionName + "</b>";
                return campfireText;
            }
            else
            {
                campfireText = "You failed to remember anything...";
                return campfireText;
            }
        }
        
    }
}
