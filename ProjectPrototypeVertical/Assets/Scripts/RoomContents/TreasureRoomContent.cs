﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TreasureRoomContent : RoomContent
{
    public Player player;
    public Action containedAction;

    public TreasureRoomContent(RoomContentTypes roomContentType) : base(roomContentType)
    {
        player = CreatureManager.main.GetPlayer();

        int randomActionNumber = UnityEngine.Random.Range(0, player.playerActionSet.Count);

        List<Action> actionList = new List<Action>(player.playerActionSet.Keys);

        containedAction = actionList[randomActionNumber].GetCopy();

        //containedAction = new BigBlockAction();
    }

    public override void PlayRoomContent()
    {
        RoomEventManager.main.SetupTreasureScreen();
    }

    public string ApplyTreasureContent()
    {
        player.AddPlayerAction(containedAction);
        string getTreasureText = "You have learned a new move! \n <b>" + containedAction.actionName + "</b>";
        return getTreasureText;
    }

}
