﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockAction : Action
{
    public Effect effect;
    // Start is called before the first frame update
    public BlockAction(int blockAmount = 5, string actionName = "Defend")
    {
        this.actionName = actionName;
        effect = new GainBlockEffect(blockAmount);
    }

    public override void Play(Creature activeCreature, Creature passiveCreature, ActionState actionState)
    {
        effect.ApplyEffect(activeCreature, passiveCreature, actionState);
        activeCreature.DecreaseCurrentAp(1);
        //CombatUiManager.main.UpdateTextFields();
    }

    public override Action GetCopy()
    {
        return new BlockAction();
    }
}
