﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockMoveForwardAction : Action
{
    public Effect[] effects;
    // Start is called before the first frame update
    public BlockMoveForwardAction(int blockAmount = 4, int moveAmount = 1, string actionName = "Guarded Advance")
    {
        this.actionName = actionName;
        effects = new Effect[]
        {
            new GainBlockEffect(blockAmount),
            new MoveEffect(moveAmount)
        };
    }

    public override void Play(Creature activeCreature, Creature passiveCreature, ActionState actionState)
    {
        foreach (Effect effect in effects)
        {
            effect.ApplyEffect(activeCreature, passiveCreature, actionState);
        }
        activeCreature.DecreaseCurrentAp(1);
        //CombatUiManager.main.UpdateTextFields();
    }

    public override Action GetCopy()
    {
        return new BlockMoveForwardAction();
    }
}
