﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KickAttackAction : Action
{
    //Liste d'effets Effects
    public Effect[] effects;

    // Start is called before the first frame update
    public KickAttackAction(int damage = 8, string actionName = "Kick")
    {
        this.actionName = actionName;
        effects = new Effect[]
        {
            new DealDamageEffect(damage),
            new PushEffect(-2)
        };
    }

    public override void Play(Creature activeCreature, Creature passiveCreature, ActionState actionState)
    {
        foreach(Effect effect in effects)
        {
            effect.ApplyEffect(activeCreature, passiveCreature, actionState);
        }
        
        activeCreature.DecreaseCurrentAp(1);
        //CombatUiManager.main.UpdateTextFields();
    }

    //quel serait le meilleur moyen pour récupérer le damage et autres infos sur le power des actions??
    public int GetDamage(Creature activeCreature, Creature passiveCreature)
    {
        return effects[0].GetEffectPower(activeCreature, passiveCreature);
    }

    public override Action GetCopy()
    {
        return new KickAttackAction();
    }
}
