﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseAttackAction : Action
{
    //Liste d'effets Effects
    public Effect effect;

    // Start is called before the first frame update
    public CloseAttackAction(int damage = 20, string actionName = "Throat cut")
    {
        this.actionName = actionName;
        effect = new CloseDamageEffect(damage);
    }

    public override void Play(Creature activeCreature, Creature passiveCreature, ActionState actionState)
    {
        effect.ApplyEffect(activeCreature, passiveCreature, actionState);
        activeCreature.DecreaseCurrentAp(1);
        //CombatUiManager.main.UpdateTextFields();
    }

    //quel serait le meilleur moyen pour récupérer le damage et autres infos sur le power des actions??
    public int GetDamage(Creature activeCreature, Creature passiveCreature)
    {
        return effect.GetEffectPower(activeCreature, passiveCreature);
    }

    public override Action GetCopy()
    {
        return new CloseAttackAction();
    }
}
