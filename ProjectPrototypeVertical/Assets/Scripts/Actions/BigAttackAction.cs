﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigAttackAction : Action
{
    //Liste d'effets Effects
    public Effect effect;

    // Start is called before the first frame update
    public BigAttackAction(int damage = 10, string actionName = "Big Strike")
    {
        this.actionName = actionName;
        effect = new DealDamageEffect(damage);
    }

    public override void Play(Creature activeCreature, Creature passiveCreature, ActionState actionState)
    {
        effect.ApplyEffect(activeCreature, passiveCreature, actionState);
        activeCreature.DecreaseCurrentAp(1);
        //CombatUiManager.main.UpdateTextFields();
    }

    public override Action GetCopy()
    {
        return new BigAttackAction();
    }
}
