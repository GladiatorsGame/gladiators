﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveForwardAction : Action
{
    //Liste d'effets Effects
    public Effect effect;

    // Start is called before the first frame update
    public MoveForwardAction(int moveAmount = 3, string actionName = "Somersault")
    {
        this.actionName = actionName;
        effect = new MoveEffect(moveAmount);
    }

    public override void Play(Creature activeCreature, Creature passiveCreature, ActionState actionState)
    {
        effect.ApplyEffect(activeCreature, passiveCreature, actionState);
        activeCreature.DecreaseCurrentAp(1);
        //CombatUiManager.main.UpdateTextFields();
    }

    public override Action GetCopy()
    {
        return new MoveForwardAction();
    }
}
