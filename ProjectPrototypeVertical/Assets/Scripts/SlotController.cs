﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SlotController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    // Start is called before the first frame update

    public Text textActionName;
    public int slotNumber;
    public Color mainColor;
    public int sequenceNumber;
    public Action containedAction;
    public bool isPlayable;
    public bool isEditable;
    public ActionState actionState;

    void Start()
    {
        isPlayable = false;
        isEditable = false;
        containedAction = null;
        actionState = new ActionState();

        ChangeMainColor(Color.white);
        ChangeMainAlpha(0.5f);
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if((SelectionManager.main.clickMode == SelectionManager.ClickModes.Edit && isEditable) || (SelectionManager.main.clickMode == SelectionManager.ClickModes.Play && isPlayable))
        {
            SelectionManager.main.ChangeHoveredSlot(this);
            if (SelectionManager.main.clickMode == SelectionManager.ClickModes.Play && slotNumber >= 1 && sequenceNumber != SequenceManager.main.currentSequenceNumber)
            {
                GetComponent<Image>().color = Color.yellow;
            }
            else
            {
                GetComponent<Image>().color = Color.green;
            }    
        }     
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        GetComponent<Image>().color = mainColor;
        SelectionManager.main.ClearHoveredSlot();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if ((SelectionManager.main.clickMode == SelectionManager.ClickModes.Edit && isEditable) || (SelectionManager.main.clickMode == SelectionManager.ClickModes.Play && isPlayable))
        {
            SelectionManager.main.ClickSlot();
            GetComponent<Image>().color = Color.cyan;
        }
    }

    public void SetActionName(Action newAction)
    {
        containedAction = newAction;

        if(containedAction.actionName != null)
        {
            textActionName.text = containedAction.actionName;
            //SequenceManager.main.UpdateSequence(sequenceNumber, slotNumber);
        }
        else
        {
            textActionName.text = null;
        }
    }

    public void ChangeMainColor(Color newColor)
    {
        mainColor = newColor;
        GetComponent<Image>().color = mainColor;
    }

    public void ChangeMainAlpha(float newAlpha)
    {
        Color newColor = mainColor;
        newColor.a = newAlpha;
        mainColor = newColor;
        GetComponent<Image>().color = mainColor;
    }

    //public void SetSequenceNumber(int newSequenceNumber)
    //{
    //    sequenceNumber = newSequenceNumber;
    //}

    //Empêche la slot d'être jouée
    public void DisableSlotPlay()
    {
        ChangeMainAlpha(0.5f);
        isPlayable = false;
    }

    //Permet à la slot d'être jouée
    public void EnableSlotPlay()
    {
        ChangeMainAlpha(1.0f);
        isPlayable = true;
    }

    //Empêche la slot d'être éditée
    public void DisableSlotEdit()
    {
        ChangeMainAlpha(0.5f);
        isEditable = false;
    }

    //Permet à la slot d'être éditée
    public void EnableSlotEdit()
    {
        ChangeMainAlpha(1.0f);
        isEditable = true;
    }

    //Joue l'action contenue dans la slot
    public void PlayAction()
    {
        //check si la slot n'est pas vide
        if (containedAction != null && isPlayable)
        {
            containedAction.Play(CreatureManager.main.GetPlayer(), CreatureManager.main.GetEnemy(), actionState);
            //SequenceManager.main.SlotPlayCooldownUpdate(sequenceNumber, slotNumber);
            //CombatManager.main.CheckCombatEnd();
            //containedAction.Play();            
        }
    }
}
