﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargeDamageEffect: Effect
{
    public int mainDamage;

    public ChargeDamageEffect(int damage)
    {
        mainDamage = damage;
    }
    public override void ApplyEffect(Creature activeCreature, Creature passiveCreature, ActionState actionState)
    {
        int totalDamage = mainDamage + activeCreature.strCurrent;

        if (actionState.GetDoubleDamageActiveState())
        {
            totalDamage *= 2;
        }

        if (CombatDistanceManager.main.GetCombatDistance() != CombatDistanceManager.combatDistances.Far)
        {
            totalDamage /= 2;
        }
        int adjustedDamage = Mathf.Max(totalDamage - passiveCreature.blockCurrent, 0);
        Debug.Log("total damage: " + totalDamage);
        Debug.Log("adjusted damage: " + adjustedDamage);
        Debug.Log("passive creature block : " + passiveCreature.blockCurrent);
        passiveCreature.DecreaseCurrentHp(adjustedDamage);

        passiveCreature.blockCurrent = Mathf.Max(passiveCreature.blockCurrent - totalDamage, 0);
        CombatUiManager.main.UpdateTextFields();
        //Debug.Log(targetCreature.name + " HP:" + targetCreature.hpCurrent);
    }

    public override int GetEffectPower(Creature activeCreature, Creature passiveCreature)
    {
        return mainDamage + activeCreature.strCurrent;
    }

}
