﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushEffect: Effect
{
    //Effect that moves the opponent backwards, if not in close position, moves you forward 1 step
    public int moveAmount;

    public PushEffect(int moveAmount)
    {
        this.moveAmount = moveAmount;
    }

    public override void ApplyEffect(Creature activeCreature, Creature passiveCreature, ActionState actionState)
    {
        if(CombatDistanceManager.main.GetCombatDistance() == CombatDistanceManager.combatDistances.Close) {
            int maxPositionValue = CombatDistanceManager.main.roomSize - activeCreature.combatPositionValue;
            passiveCreature.ChangeCombatPositionValue(moveAmount, maxPositionValue);
        }
        else
        {
            int maxPositionValue = CombatDistanceManager.main.roomSize - passiveCreature.combatPositionValue;
            activeCreature.ChangeCombatPositionValue(1, maxPositionValue);
        }

    }

    public override int GetEffectPower(Creature activeCreature, Creature passiveCreature)
    {
        return moveAmount;
    }

}
