﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveEffect: Effect
{
    public int moveAmount;

    public MoveEffect(int moveAmount)
    {
        this.moveAmount = moveAmount;
    }

    public override void ApplyEffect(Creature activeCreature, Creature passiveCreature, ActionState actionState)
    {
        int maxPositionValue = CombatDistanceManager.main.roomSize - passiveCreature.combatPositionValue;
        activeCreature.ChangeCombatPositionValue(moveAmount, maxPositionValue);
        
    }

    public override int GetEffectPower(Creature activeCreature, Creature passiveCreature)
    {
        return moveAmount;
    }

}
