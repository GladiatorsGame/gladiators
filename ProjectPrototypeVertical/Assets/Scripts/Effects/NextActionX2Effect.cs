﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextActionX2Effect : Effect
{

    public NextActionX2Effect()
    {
    }
    public override void ApplyEffect(Creature activeCreature, Creature passiveCreature, ActionState actionState)
    {
        SlotController nextSlot = SequenceManager.main.GetSlotController(SequenceManager.main.lastPlayedSequence, SequenceManager.main.lastPlayedSlot + 1);

        if (nextSlot != null) //Is null if there is no next slot
        {
            nextSlot.actionState.SetDoubleDamageActiveState(true);
        }

    }
}
