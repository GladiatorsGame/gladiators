﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;



// Class that contains all the possible moves for the enemys
public static class PlayerComboSet
{
    static private List<Combo> playerComboList = new List<Combo> {
        //HEAVY COMBO - donne accès aux HEAVY SKILLS mais charge moins le SPECIAL, charge de 1/8 le special
        new Combo(new List<string> { "HEAVYATTACK", "HEAVYATTACK", "HEAVYATTACK" }, 3, Combo.ChargeType.STR, true, 40, 0, 0, 20, "HEAVY COMBO", true, false),
        
        //2 HEAVY, 1 LIGHT COMBO, chage de 1/4 le special
        new Combo(new List<string> { "HEAVYATTACK", "HEAVYATTACK", "LIGHTATTACK" }, 6, Combo.ChargeType.STR, true, 25, 0, 10, 15, "HEAVY MIXED COMBO", false, false),
        new Combo(new List<string> { "HEAVYATTACK", "LIGHTATTACK", "HEAVYATTACK" }, 6, Combo.ChargeType.STR, true, 25, 0, 10, 15, "HEAVY MIXED COMBO", false, false),
        new Combo(new List<string> { "LIGHTATTACK", "HEAVYATTACK", "HEAVYATTACK" }, 6, Combo.ChargeType.STR, true, 25, 0, 10, 15, "HEAVY MIXED COMBO", false, false),

        //LIGHT COMBO, charge de 1/8 le special
        new Combo(new List<string> { "LIGHTATTACK", "LIGHTATTACK", "LIGHTATTACK" }, 3, Combo.ChargeType.SPD, true, 0, 0, 40, 20, "LIGHT COMBO", false, true),

        //2 LIGHT, 1 HEAVY COMBO, charge de 1/4 le special
        new Combo(new List<string> { "LIGHTATTACK", "LIGHTATTACK", "HEAVYATTACK" }, 6, Combo.ChargeType.SPD, true, 10, 0, 25, 15, "LIGHT MIXED COMBO", false, false),
        new Combo(new List<string> { "HEAVYATTACK", "LIGHTATTACK", "LIGHTATTACK" }, 6, Combo.ChargeType.SPD, true, 10, 0, 25, 15, "LIGHT MIXED COMBO", false, false),
        new Combo(new List<string> { "LIGHTATTACK", "HEAVYATTACK", "LIGHTATTACK" }, 6, Combo.ChargeType.SPD, true, 10, 0, 25, 15, "LIGHT MIXED COMBO", false, false),

        //BLOCK COMBO, charge de 1/8 le special
        new Combo(new List<string> { "BLOCK", "BLOCK", "BLOCK" }, 3, Combo.ChargeType.END, false, 0, 40, 0, 20, "BLOCK COMBO", false, false),

        //2 BLOCK, 1 COUNTER COMBO, charge de 1/4 le special
        new Combo(new List<string> { "BLOCK", "BLOCK", "COUNTER"}, 6, Combo.ChargeType.END, false, 0, 25, 10, 15, "BLOCK MIXED COMBO", false, false),
        new Combo(new List<string> { "BLOCK", "COUNTER", "BLOCK"}, 6, Combo.ChargeType.END, false, 0, 25, 10, 15, "BLOCK MIXED COMBO", false, false),
        new Combo(new List<string> { "COUNTER", "BLOCK", "BLOCK"}, 6, Combo.ChargeType.END, false, 0, 25, 10, 15, "BLOCK MIXED COMBO", false, false),

        //COUNTER COMBO, charge de 1/8 le special
        new Combo(new List<string> { "COUNTER", "COUNTER", "COUNTER"}, 3, Combo.ChargeType.SPD, false, 0, 0, 40, 20, "COUNTER COMBO", false, false),

        //2 COUNTER, 1 BLOCK COMBO, charge de 1/4 le special
        new Combo(new List<string> { "COUNTER", "BLOCK", "COUNTER"}, 6, Combo.ChargeType.SPD, false, 0, 10, 25, 15, "COUNTER MIXED COMBO", false, false),
        new Combo(new List<string> { "COUNTER", "COUNTER", "BLOCK"}, 6, Combo.ChargeType.SPD, false, 0, 10, 25, 15, "COUNTER MIXED COMBO", false, false),
        new Combo(new List<string> { "BLOCK", "COUNTER", "COUNTER"}, 6, Combo.ChargeType.SPD, false, 0, 10, 25, 15, "COUNTER MIXED COMBO", false, false),
        
        //FOCUS COMBO ne pas ajouter à la liste de combos mais donner la charge si les 3 sont activés
        //new Combo(new List<string> { "STRFOCUS", "ENDFOCUS", "SPDFOCUS" }, 1, Combo.ChargeType.ALL, false, 20, 20, 20, 0),
        //new Combo(new List<string> { "STRFOCUS", "SPDFOCUS", "ENDFOCUS" }, 1, Combo.ChargeType.ALL, false, 20, 20, 20, 0),
        //new Combo(new List<string> { "SPDFOCUS", "STRFOCUS", "ENDFOCUS" }, 1, Combo.ChargeType.ALL, false, 20, 20, 20, 0),
        //new Combo(new List<string> { "SPDFOCUS", "ENDFOCUS", "STRFOCUS" }, 1, Combo.ChargeType.ALL, false, 20, 20, 20, 0),
        //new Combo(new List<string> { "ENDFOCUS", "STRFOCUS", "SPDFOCUS" }, 1, Combo.ChargeType.ALL, false, 20, 20, 20, 0),
        //new Combo(new List<string> { "ENDFOCUS", "SPDFOCUS", "STRFOCUS" }, 1, Combo.ChargeType.ALL, false, 20, 20, 20, 0),
    };

    public static Combo GetCombo(int comboNumber)
    {
        return playerComboList[comboNumber];
    }
    
    public static List<Combo> GetComboList()
    {
        return playerComboList;
    }
}


