﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;



// Class that contains all the possible moves for the enemys

public static class EnemyActionSet
{
    //private Enemy enemy = EntityManager.main.GetActiveEnemy();

    static private List<EnemyAction> enemyActionList = new List<EnemyAction> {
        //GENERAL ENEMY ACTIONS
        new EnemyAction("ATTACK", EnemyAction.EnemyActionType.Attack),
        new EnemyAction("SUPERATTACK", EnemyAction.EnemyActionType.SuperAttack),
        new EnemyAction("WAIT", EnemyAction.EnemyActionType.Wait),
        new EnemyAction("HEAL", EnemyAction.EnemyActionType.Heal),
        new EnemyAction("STRFOCUS", EnemyAction.EnemyActionType.StrFocus),
        new EnemyAction("ENDFOCUS", EnemyAction.EnemyActionType.EndFocus),
        new EnemyAction("SPDFOCUS", EnemyAction.EnemyActionType.SpdFocus),
        new EnemyAction("DEFENSEUP", EnemyAction.EnemyActionType.Wait,power:25,duration:1),//DEF UP 25%
        new EnemyAction("DEFENSEDOWN", EnemyAction.EnemyActionType.Wait,power:25,duration:1),//DEF DOWN 25%

        //ATTACKS WITH TAKEDAMAGE EFFECT
        new EnemyAction("ACIDATTACK", EnemyAction.EnemyActionType.Attack, duration: 3),  //DEFENSE DOWN, TAKEDAMAGE
        new EnemyAction("POISONATTACK", EnemyAction.EnemyActionType.Attack, duration: 3), //END DOWN, TAKEDAMAGE
        new EnemyAction("BLEEDINGATTACK", EnemyAction.EnemyActionType.Attack, duration:3),//TAKEDAMAGE
        new EnemyAction("BURNINGATTACK", EnemyAction.EnemyActionType.Attack, duration:3),//TAKEDAMAGE

        //ATTACKS WITH EFFECTS ON PRIMARY STATS
        new EnemyAction("WEAKENING ATTACK", EnemyAction.EnemyActionType.Attack, power:25, duration:2),  //STR DOWN -25%
        new EnemyAction("DRAINING ATTACK", EnemyAction.EnemyActionType.Attack, power: 25, duration:2),  //END DOWN -25%
        new EnemyAction("SLOWING ATTACK", EnemyAction.EnemyActionType.Attack, power:25, duration:2),  //SPD DOWN -25%

        new EnemyAction("PARALYZING ATTACK", EnemyAction.EnemyActionType.Attack, power:1, duration:2),
        new EnemyAction("CONFUSING ATTACK", EnemyAction.EnemyActionType.Attack, power:1, duration:2),
        new EnemyAction("BLINDING ATTACK", EnemyAction.EnemyActionType.Attack, power:1, duration:2), //NB de QTE +1


        //FLYME SPECIAL MOVES

        //FIREBALL SPECIAL MOVES
        new EnemyAction("BLINDING STEAM", EnemyAction.EnemyActionType.SuperAttack, power:1, duration:2) //NB de QTE +1, BLINDING, BURNING
    };


    public static EnemyAction GetEnemyAction(EnemyAction.EnemyActionType enemyActionType)
    {
        return enemyActionList.Find(x => x.GetEnemyActionType().Equals(enemyActionType));
    }



}
