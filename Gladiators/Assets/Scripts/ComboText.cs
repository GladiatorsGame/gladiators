﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ComboText : MonoBehaviour
{
    private float speed;
    private Vector2 direction;
    private float fadeTime;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
       
        float translation = speed * Time.deltaTime / Time.timeScale;
        transform.Translate(direction * translation);
        transform.Rotate(new Vector3(0, 0, 0.1f));

    }
    public void Initialize(float speed, Vector2 direction, float fadeTime)
    {
        this.speed = speed;
        this.fadeTime = fadeTime;
        this.direction = direction;
        
        StartCoroutine(FadeOut());
    }

    private IEnumerator FadeOut()
    {
        float startRed = GetComponent<Text>().color.r;
        float startAlpha = GetComponent<Text>().color.a;
        float startSize = GetComponent<Text>().fontSize;

        float rate = 1.0f / fadeTime;
        float progress = 0.0f;

        while (progress < 1.0)
        {
            Color tmpColor = GetComponent<Text>().color;
            GetComponent<Text>().fontSize = (int)Mathf.Lerp(startSize, 150, progress);
            GetComponent<Text>().color = new Color(Mathf.Lerp(startRed, 50, progress), tmpColor.g, tmpColor.b, Mathf.Lerp(startAlpha, 0, progress));
            progress += rate * Time.deltaTime / Time.timeScale;
            yield return null;
        }
        Destroy(gameObject);
    }

}
