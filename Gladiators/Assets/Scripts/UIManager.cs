﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * Classe qui gère les inputs;
 */

public class UIManager : MonoBehaviour
{
    private Player player;
    private Enemy enemy;
    public static UIManager main;
    public StatBar hpBar;
    public StatBar apBar;
    public StatBar specialBar;
    public MovingText creatureTurnText;
    public MovingText[] actionText;
    public GameObject creatureInfoContainer;
    public Text enemyStatusText;
    public Text enemyNameText;
    public Text enemyPhaseText;
    public Text enemySequenceText;
    public Text enemyActionText;

    public Text textPlayerHpAmount, textCreatureHpAmount, textSpecialStr, textSpecialEnd, textSpecialSpd;
    public Text squareButtonText, xButtonText, circleButtonText, triangleButtonText; 
    public Button buttonPlayerAttack, buttonLoadFirstCombat, ButtonEndTurn;
    public Button buttonPlusStrength, buttonMinusStrength, buttonPlusEndurance, buttonMinusEndurance, buttonPlusSpeed, buttonMinusSpeed, buttonPlusWeaponDamage, buttonMinusWeaponDamage, buttonPlusArmor, buttonMinusArmor;
    public Image switchTurnBG;
    public GameObject optionsMenu;
    public GameObject uiContainer;
    public GameObject scoreBoard;
    public Text combatResultText, perfectCounterText, quickTurnsCounterText, counterAttackCounterText, blockCounterText, turnCounterText, enemyExpBonusText, totalExpText;
    public Text strengthText, enduranceText, speedText, weaponDamageText, armorText, levelExpText;
    public List<Image> QTEImageList;
    public APFlash[] apFlash;
    public PlayerAnimation playerAnimation;

    private bool isSwitchingTurn;
    private int switchTurnStep;
    private bool attackButtonActive = false;
    private bool endTurnButtonActive = false;

    private int displayEnemyInfoTaskCounter = 0;
    
    private int actionsCount = 0;

    private bool playerBlockInput = false;
    private int blockCounter;

    private bool playerCounterInput = false;
    public bool hasCountered;

    private bool titleScreenActive = true;
    public GameObject titleScreen;

    private float enemyInfoTargetAlpha;
    private float enemyInfoLerpSpeed;


    private int lightAttackDamageDisplay, heavyAttackDamageDisplay, superAttackDamageDisplay;

    void Start()
    {
        //buttonPlayerAttack.onClick.AddListener(TriggerPlayerAttack);
        buttonLoadFirstCombat.onClick.AddListener(TriggerLoadFirstCombat);
        //ButtonEndTurn.onClick.AddListener(EndTurnEvent);
        buttonPlayerAttack.interactable = false;
        ButtonEndTurn.interactable = false;
        isSwitchingTurn = false;
        switchTurnStep = 0;
        switchTurnBG.gameObject.SetActive(true);
        switchTurnBG.canvasRenderer.SetAlpha(0.0f);
        scoreBoard.SetActive(false);
        
        squareButtonText.text = "HEAVY ATTACK";
        triangleButtonText.text = "SPECIAL MOVE";
        circleButtonText.text = "USE ITEM";
        xButtonText.text = "ATTACK";

        buttonPlusStrength.onClick.AddListener(TriggerPlusStrength);
        buttonMinusStrength.onClick.AddListener(TriggerMinusStrength);
        buttonPlusEndurance.onClick.AddListener(TriggerPlusEndurance);
        buttonMinusEndurance.onClick.AddListener(TriggerMinusEndurance);
        buttonPlusSpeed.onClick.AddListener(TriggerPlusSpeed);
        buttonMinusSpeed.onClick.AddListener(TriggerMinusSpeed);
        buttonPlusWeaponDamage.onClick.AddListener(TriggerPlusWeaponDamage);
        buttonMinusWeaponDamage.onClick.AddListener(TriggerMinusWeaponDamage);
        buttonPlusArmor.onClick.AddListener(TriggerPlusArmor);
        buttonMinusArmor.onClick.AddListener(TriggerMinusArmor);

        textSpecialStr.text = "0";
        textSpecialEnd.text = "0";
        textSpecialSpd.text = "0";

        enemyStatusText.text = "NORMAL";

        enemyInfoTargetAlpha = 1;
        enemyInfoLerpSpeed = 10;


}

void Awake()
    {
        main = this;
        
    }

    void Update()
    {
        lightAttackDamageDisplay = player.GetDamage() - enemy.GetDefense();
        heavyAttackDamageDisplay = (player.GetDamage() - enemy.GetDefense()) * 3 / 2;
        superAttackDamageDisplay = (player.GetDamage() - enemy.GetDefense()) * (Mathf.Max(1, player.GetSpecialStrCharge() / 20));
        textSpecialStr.text = player.GetSpecialStrCharge().ToString();
        textSpecialEnd.text = player.GetSpecialEndCharge().ToString();
        textSpecialSpd.text = player.GetSpecialSpdCharge().ToString();
        

        if (attackButtonActive)
        {
            //Shoulder pads sur la manette
            if (Input.GetButton("L1") || Input.GetButton("R1") || Input.GetKeyDown(KeyCode.RightShift))
            {
                squareButtonText.text = "STR FOCUS";
                squareButtonText.color = Color.white;
                triangleButtonText.text = "END FOCUS";
                triangleButtonText.color = Color.white;
                circleButtonText.text = "SPD FOCUS";
                circleButtonText.color = Color.white;
                xButtonText.text = "";

                if (Input.GetButtonDown("Square") || Input.GetKeyDown(KeyCode.A))
                {
                    TriggerPlayerFocus(1);
                }
                if (Input.GetButtonDown("Triangle") || Input.GetKeyDown(KeyCode.W))
                {
                    TriggerPlayerFocus(2);
                }
                if (Input.GetButtonDown("Circle") || Input.GetKeyDown(KeyCode.D))
                {
                    TriggerPlayerFocus(3);
                }
            }
            //LIGHT ATTACK SKILLS
            else if (Input.GetAxisRaw("R2") >= 1 || Input.GetKeyDown(KeyCode.RightShift))
            {
                squareButtonText.text = "STEAL GOLD (" + lightAttackDamageDisplay + ")";
                triangleButtonText.text = "BLINDING ATTACK (" + lightAttackDamageDisplay + ")";
                circleButtonText.text = "DEBUFFING ATTACK (" + lightAttackDamageDisplay + ")";
                xButtonText.text = "";

                if (player.GetLightAttackSkills())
                {
                    squareButtonText.color = Color.white;
                    triangleButtonText.color = Color.white;
                    circleButtonText.color = Color.white;
                }
                else
                {
                    squareButtonText.color = new Color(255, 255, 255, 0.25f);
                    triangleButtonText.color = new Color(255, 255, 255, 0.25f);
                    circleButtonText.color = new Color(255, 255, 255, 0.25f);
                }

                if ((Input.GetButtonDown("Square") || Input.GetKeyDown(KeyCode.A)) && player.GetLightAttackSkills())
                {
                    TriggerPlayerLightAttack();
                }
                if ((Input.GetButtonDown("Triangle") || Input.GetKeyDown(KeyCode.W)) && player.GetLightAttackSkills())
                {
                    TriggerPlayerLightAttack();
                }
                if ((Input.GetButtonDown("Circle") || Input.GetKeyDown(KeyCode.D)) && player.GetLightAttackSkills())
                {
                    TriggerPlayerLightAttack();
                }
            }
            //HEAVY ATTACK SKILLS
            else if (Input.GetAxisRaw("L2") >= 1 || Input.GetKeyDown(KeyCode.LeftControl))
            {
                squareButtonText.text = "BLEEDING ATTACK (" + heavyAttackDamageDisplay + ")";
                triangleButtonText.text = "ARMOR BREAKING ATTACK (" + heavyAttackDamageDisplay + ")";
                circleButtonText.text = "WEAPON BREAKING ATTACK (" + heavyAttackDamageDisplay + ")";
                xButtonText.text = "";

                if (player.GetHeavyAttackSkills())
                {
                    squareButtonText.color = Color.white;
                    triangleButtonText.color = Color.white;
                    circleButtonText.color = Color.white;
                }
                else
                {
                    squareButtonText.color = new Color(255, 255, 255, 0.25f);
                    triangleButtonText.color = new Color(255, 255, 255, 0.25f);
                    circleButtonText.color = new Color(255, 255, 255, 0.25f);
                }
                
                if ((Input.GetButtonDown("Square") || Input.GetKeyDown(KeyCode.A)) && player.GetHeavyAttackSkills())
                {
                    TriggerPlayerHeavyAttack();
                }
                if ((Input.GetButtonDown("Triangle") || Input.GetKeyDown(KeyCode.W)) && player.GetHeavyAttackSkills())
                {
                    TriggerPlayerHeavyAttack();
                }
                if ((Input.GetButtonDown("Circle") || Input.GetKeyDown(KeyCode.D)) && player.GetHeavyAttackSkills())
                {
                    TriggerPlayerHeavyAttack();
                }
            }
            //MENU NORMAL
            else
            {
                squareButtonText.color = Color.white;
                squareButtonText.text = "HEAVY ATTACK (" + heavyAttackDamageDisplay + ")";
                
                //squareButtonText.text = "SUPER ATTACK (" + superAttackDamageDisplay + ")"; 
                triangleButtonText.text = "SPECIAL (" + superAttackDamageDisplay + ")";
                if (player.GetCurrentSpecialCharge() >= player.GetMaxSpecialCharge()  && !player.GetSpecialActive())
                {
                    triangleButtonText.color = Color.white;
                }
                else
                {
                    triangleButtonText.color = new Color(255, 255, 255, 0.25f);
                }
                if (player.GetNbHealingPotions() > 0)
                {
                    circleButtonText.text = "USE ITEM";
                    circleButtonText.color = Color.white;
                }
                else
                {
                    circleButtonText.text = "USE ITEM";
                    circleButtonText.color = new Color(255, 255, 255, 0.25f);
                }

                xButtonText.color = Color.white;
                xButtonText.text = "LIGHT ATTACK (" + lightAttackDamageDisplay + ")";

                if (Input.GetButtonDown("X") || Input.GetKeyDown(KeyCode.S))
                {
                    TriggerPlayerLightAttack();
                }
                if (Input.GetButtonDown("Square") || Input.GetKeyDown(KeyCode.A))
                {
                    TriggerPlayerHeavyAttack();
                }
                if ((Input.GetButtonDown("Triangle") || Input.GetKeyDown(KeyCode.W)) && player.GetCurrentSpecialCharge() >= player.GetMaxSpecialCharge() && !player.GetSpecialActive())
                {
                    TriggerPlayerSuperAttack();
                }
                if (endTurnButtonActive)
                {
                    if (Input.GetButtonDown("Start") || Input.GetKeyDown(KeyCode.Return))
                    {
                        EndTurnEvent();
                    }
                }
                if ((Input.GetButtonDown("Circle") || Input.GetKeyDown(KeyCode.D)) && player.GetNbHealingPotions() > 0)
                {
                    TriggerPlayerUseItem();
                }

            }
        }
        else if(!attackButtonActive && GameController.main.getPlayingCreature()==EntityManager.main.GetPlayer())
        {
            triangleButtonText.color = new Color(255, 255, 255, 0.25f);
            squareButtonText.color = new Color(255, 255, 255, 0.25f);
            xButtonText.color = new Color(255, 255, 255, 0.25f);
            circleButtonText.color = new Color(255, 255, 255, 0.25f);

            if (endTurnButtonActive)
            {
                if (Input.GetButtonDown("Start") || Input.GetKeyDown(KeyCode.Return))
                {
                    EndTurnEvent();
                }
            }
        }
        else
        {
            if (playerBlockInput)
            {
                squareButtonText.color = Color.white;
            }
            else
            {
                squareButtonText.color = new Color(255, 255, 255, 0.25f);
            }

            if (playerCounterInput)
            {
                xButtonText.color = Color.white;
            }
            else
            {
                xButtonText.color = new Color(255, 255, 255, 0.25f);
            }
            squareButtonText.text = "BLOCK";
            triangleButtonText.text = "";
            circleButtonText.text = "";
            //if (player.GetNbHealingPotions() > 0)
            //{
            //    circleButtonText.text = "USE ITEM";
            //}
            //else
            //{
            //    circleButtonText.text = "";
            //}
            xButtonText.text = "COUNTER (" + lightAttackDamageDisplay + ")";

            if (endTurnButtonActive)
            {
                if (Input.GetButtonDown("Start"))
                {
                    EndTurnEvent();
                }
            }
        }
        //BLOCK ACTION CODE
        if (playerBlockInput && (Input.GetButtonDown("Square") || Input.GetKeyDown(KeyCode.A)) && player.GetCurrentAp() > 0 && blockCounter < 3)
        {
            blockCounter++;
            player.blockCounter++;
            DisplayActionText("BLOCK");
            ApFlash(5 - player.GetCurrentAp());
            player.RemoveAp(1);
            playerAnimation.playerAnimator.SetBool("Block", true);
        }


        //COUNTER ACTION CODE
        if (playerCounterInput && (Input.GetButtonDown("X") || Input.GetKeyDown(KeyCode.S)) && player.GetCurrentAp() > 0)
        {
            Time.timeScale = 1;
            playerAnimation.playerAnimator.SetBool("Block", false);
            hasCountered = true;
            SetPlayerCounterInput(false);
            //Debug.Log("CounterInput!");
            DisplayActionText("COUNTER");
            ApFlash(5 - player.GetCurrentAp());
            player.RemoveAp(1);
            //GameController.main.CounterAttack();
            //playerAnimation.playerAnimator.SetBool("Block", true); Probablement juste mettre counter?
        }

        // Load new combat
        if(GameController.main.GetGameState() == GameController.GameState.BetweenCombat && (Input.GetButtonDown("X") || Input.GetKeyDown(KeyCode.Space)))
        {
            scoreBoard.SetActive(false);
            GameController.main.LoadNextCombat();
        }

        // Open Menu
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            // Check whether it's active / inactive
            bool isActive = optionsMenu.activeSelf;
            optionsMenu.SetActive(!isActive);
            uiContainer.SetActive(!uiContainer.activeSelf);
        }
        
        //HP bar update
        hpBar.StatBarUpdate((float)player.GetCurrentHp() / player.GetMaxHp());

        //AP bar update
        apBar.StatBarUpdate((float)player.GetCurrentAp() / player.GetMaxAp());

        //Special bar update
        specialBar.StatBarUpdate((float)player.GetCurrentSpecialCharge() / player.GetMaxSpecialCharge());

        FadeEnemyInfo();

        for (int i = 0; i < actionText.Length; i++)
        {
            if (actionText[i].GetIsMoving())
            {
                actionText[i].MoveText();

                // Check if useful
                if (System.Math.Round(actionText[i].transform.position.x, 3) >= System.Math.Round(actionText[i].GetTargetPosition().x, 3))
                {
                    actionText[i].StopMoving();
                }
            }
        }

        //Animation du changement de tour
        if (isSwitchingTurn)//est-ce qu'on est en train de changer de tour?
        {
            
            EndTurnBGFade(0.5f);//fadein du background

            if (creatureTurnText.GetIsMoving())//est-ce que le texte est en train de bouger?
            {
                if (System.Math.Round(creatureTurnText.transform.position.x, 1) >= System.Math.Round(creatureTurnText.GetTargetPosition().x, 1))
                {
                    creatureTurnText.StopMoving();//si le texte a atteint sa position, il arrête son déplacement
                    switchTurnStep++;//on passe à l'étape suivante

                }
                else
                {
                    creatureTurnText.MoveText();//sinon, on bouge le texte vers la droite
                }
            }
            else
            {
                //gère l'animation en 3 étapes
                switch (switchTurnStep)
                {
                    case 0://première étape, bouge le texte vers le centre
                        //envoie la speed(0=lent, 1= medium, 2=fast) et la coordonnée en x où bouger le texte
                        creatureTurnText.MoveTo(2, new Vector2(-0.3f, creatureTurnText.transform.position.y));
                        SoundManagerN.instance.PlaySfx(0f, SoundManagerN.AudioBank.SWOOSH);
                        break;

                    case 1: //2e étape, bouge le texte lentement
                        creatureTurnText.MoveTo(0, new Vector2(0.3f, creatureTurnText.transform.position.y));
                        break;
                    case 2://3e étape, bouge le texte rapidement vers la droite
                        creatureTurnText.MoveTo(2, new Vector2(13.10f, creatureTurnText.transform.position.y));
                        SoundManagerN.instance.PlaySfx(0f, SoundManagerN.AudioBank.SWOOSHREVERSE);
                        break;
                    case 3:
                        isSwitchingTurn = false;//indique que le processus d'animation est fini
                        switchTurnStep = 0;//réinitialisation pour le prochain tour
                        creatureTurnText.MoveToStartingPosition();//remise de texte à sa position initiale à gauche
                        if (GameController.main.GetGameState() == GameController.GameState.PlayerTurn)//changement du texte pour le prochain tour
                        {
                            creatureTurnText.ChangeText("PLAYER'S\nTURN");
                        }
                        else if ((GameController.main.GetGameState() == GameController.GameState.EnemyTurn))
                        {
                            creatureTurnText.ChangeText("ENEMY'S\nTURN");
                            TogglePlayerActionButtons(); //réactivation des contrôles
                            TogglePlayerEndTurn();
                        }
                        
                        EndTurnBGFade(0);//fadeout du background
                        GameController.main.SwitchTurn(); //changement de tour
                        break;
                }
            }
        }

        if (titleScreenActive)
        {
            if (Input.GetButtonDown("X") || Input.anyKeyDown)
            {
                
                TriggerLoadFirstCombat();
            }
        }
    }
    public void TriggerLoadFirstCombat()
    {
        buttonLoadFirstCombat.interactable = false;
        titleScreen.SetActive(false);
        titleScreenActive = false;
        GameController.main.LoadFirstCombat();
    }

    public void TriggerPlayerAttack()
    {
        //SoundManagerN.instance.PlaySfx(0f, SoundManagerN.AudioBank.UI);
        ////changement du texte du textaction
        DisplayActionText("ATTACK");
        ApFlash(5 - player.GetCurrentAp());
        player.RemoveAp(1);
        TogglePlayerActionButtons();
        TogglePlayerEndTurn();
        GameController.main.SetPendingAction(GameController.main.NormalAttack, "Attack");
        GameController.main.StartQTE(3,1);  
    }

    public void TriggerPlayerLightAttack()
    {
        ////changement du texte du textaction
        DisplayActionText("LIGHT ATTACK");
        ApFlash(5 - player.GetCurrentAp());
        player.RemoveAp(1);
        TogglePlayerActionButtons();
        TogglePlayerEndTurn();
        GameController.main.SetPendingAction(GameController.main.LightAttack, "Attack");
        GameController.main.StartQTE(2, 1);
    }

    public void TriggerPlayerHeavyAttack()
    {
        ////changement du texte du textaction
        DisplayActionText("HEAVY ATTACK");
        ApFlash(5 - player.GetCurrentAp());
        player.RemoveAp(1);
        TogglePlayerActionButtons();
        TogglePlayerEndTurn();
        GameController.main.SetPendingAction(GameController.main.HeavyAttack, "Attack");
        GameController.main.StartQTE(4, 1);
        //CODE POUR 2 AP COST
        //if (player.GetCurrentAp() >= 2)
        //{
        //    ////changement du texte du textaction
        //    DisplayActionText("HEAVY ATTACK");
        //    for (int i = 5 - player.GetCurrentAp(); i < 5 - player.GetCurrentAp() + 2; i++)
        //    {
        //        ApFlash(i);
        //    }
        //    player.RemoveAp(2);
        //    TogglePlayerActionButtons();
        //    TogglePlayerEndTurn();
        //    GameController.main.SetPendingAction(GameController.main.HeavyAttack, "Attack");
        //    GameController.main.StartQTE(4, 1);
        //}
        ////Sinon fait flasher les boîtes de la ApBar pour les AP nécessaires mais manquants 
        //else
        //{
        //    for (int i = apFlash.Length - 2; i <= apFlash.Length - 1; i++)
        //    {
        //        ApFlash(i);
        //    }
        //}
    }

    public void TriggerPlayerSuperAttack()
    {
        //ANCIEN CODE DE SUPERATTACK AVEC COST DE 3 AP

        ////si assez d'AP pour faire l'action (3)
        //if (player.GetCurrentAp() >= 3)
        //{
        //    //SoundManagerN.instance.PlaySfx(0f, SoundManagerN.AudioBank.UI);
        //    ////changement du texte du textaction
        //    DisplayActionText("SUPER ATTACK");
        //    for (int i = 5 - player.GetCurrentAp(); i < 5 - player.GetCurrentAp() + 3; i++)
        //    {
        //        ApFlash(i);
        //    }
        //    player.RemoveAp(3);
        //    TogglePlayerActionButtons();
        //    TogglePlayerEndTurn();
        //    player.ToggleSpecialActive();
        //    Debug.Log("SPECIAL ACTIVATED");
        //    Debug.Log("STR charge: " + player.GetSpecialStrCharge());
        //    Debug.Log("SPD charge: " + player.GetSpecialSpdCharge());
        //    Debug.Log("END charge: " + player.GetSpecialEndCharge());
        //    GameController.main.SetPendingAction(GameController.main.SuperAttack, "Attack");
        //    GameController.main.StartQTE(5,1);
        //}
        ////Sinon fait flasher les boîtes de la ApBar pour les AP nécessaires mais manquants 
        //else
        //{
        //    for (int i = apFlash.Length - 3; i <= apFlash.Length - 1; i++)
        //    {
        //        ApFlash(i);
        //    }
        //}

        //NOUVEAU CODE DE SPECIAL ATTACK AVEC 0 AP COST
        DisplayActionText("SUPER ATTACK");
        ApFlash(5 - player.GetCurrentAp());
        player.RemoveAp(1);
        TogglePlayerActionButtons();
        TogglePlayerEndTurn();
        player.ToggleSpecialActive();
        GameController.main.SetPendingAction(GameController.main.SuperAttack, "Attack");
        GameController.main.StartQTE(5, 1);
    }

    public void TriggerPlayerFocus(int focusType)
    {
        //si aucun autre focus du même type n'est activé, on active le focus désiré
        if (player.GetFocus(focusType) == false)
        {
            //SoundManagerN.instance.PlaySfx(0f, SoundManagerN.AudioBank.UI);
            switch (focusType)
            {
                case 1:
                    DisplayActionText("STR FOCUS");
                    break;

                case 2:
                    DisplayActionText("END FOCUS");
                    break;

                case 3:
                    DisplayActionText("SPD FOCUS");
                    break;
            }
            //changement du texte du textaction
            ApFlash(5 - player.GetCurrentAp());
            player.RemoveAp(1);
            TogglePlayerActionButtons();
            TogglePlayerEndTurn();
            //CustomAction newCustomAction = GameController.main.Focus;
            GameController.main.SetFocusType(focusType);
            GameController.main.SetPendingAction(GameController.main.Focus, "Focus");
            GameController.main.StartQTE(2,2);
        }
    }

    public void TriggerPlayerUseItem()
    {
        if (player.GetCurrentAp() >= 2)
        {
            if (player.GetNbHealingPotions() > 0)
            {
                SoundManagerN.instance.PlaySfx(0f, SoundManagerN.AudioBank.UI);
                ////changement du texte du textaction
                DisplayActionText("HEALTH POTION");
                for(int i = 5 - player.GetCurrentAp(); i < 5 - player.GetCurrentAp() + 2; i++)
                {
                    ApFlash(i);
                }         
                player.RemoveAp(2);
                TogglePlayerActionButtons();
                TogglePlayerEndTurn();
                GameController.main.SetPendingAction(GameController.main.UseItem, "Item");
                GameController.main.StartAnimation();
            }
            else
            {
                for (int i = apFlash.Length - 3; i <= apFlash.Length - 1; i++)
                {
                    ApFlash(i);
                }
            }
        }
    }

    

    public void UpdateText()
    {
        main.textPlayerHpAmount.text = player.GetCurrentHp().ToString() + "/" + player.GetMaxHp().ToString();
        main.textCreatureHpAmount.text = enemy.GetCurrentHp().ToString();
    }


    public void StartTurnUIUpdate()
    {
        ButtonEndTurn.interactable = true;
        buttonPlayerAttack.interactable = true;
        attackButtonActive = true;
        endTurnButtonActive = true;
    }
    public void TogglePlayerEndTurn()
    {
        ButtonEndTurn.interactable = !ButtonEndTurn.IsInteractable();
        endTurnButtonActive = !endTurnButtonActive;
    }
    public void TogglePlayerActionButtons()
    {
        buttonPlayerAttack.interactable = !buttonPlayerAttack.IsInteractable();
        attackButtonActive = !attackButtonActive;
    }
 
    public void EndTurnEvent()
    {
        GameController.main.CheckTurnTime();
        TogglePlayerEndTurn();
        isSwitchingTurn = true;
        ClearActionText();
        if (player.GetCurrentAp() > 0)
        {
            TogglePlayerActionButtons();
        }
        DisplayEnemyInfo(0.1f);

    }
    public void DisplayActionText(string attackType)
    {
        //changement du texte du textaction
        actionText[actionsCount].ChangeText(attackType);
        actionText[actionsCount].ChangeColor(new Color(1f, 1f, 1f, 1f), 0.1f);
        actionText[actionsCount].MoveTo(2, new Vector2(-6.1f, actionText[actionsCount].transform.position.y));
        //changement d'alpha du textaction précédent
        if (actionsCount>0)
        {
            Color newColor = actionText[actionsCount - 1].GetColor();
            newColor.a = 0.3f;
            actionText[actionsCount-1].ChangeColor(newColor, 0.4f);
        }
        actionsCount++;
    }
    public void ClearActionText()
    {
        for (int i = 0; i < actionText.Length; ++i)
        {
            actionText[i].ChangeText("");
            actionText[i].MoveToStartingPosition();
            actionText[i].ChangeColor(Color.white, 0.1f);
        }
        actionsCount = 0;
    }


    public void UpdateQTEImage(int imageNumber, Color color)
    {
        QTEImageList[imageNumber].color = color;
    }

    public void QTEImageInactive()
    {
        foreach (Image image in QTEImageList)
        {
            image.color = new Color(0f, 255f, 255f, 0.25f);
        }
    }
    public void ClearQTEImage()
    {
        foreach (Image anImage in QTEImageList)
        {
            anImage.color = new Color(255f, 255f, 255f, 0.25f);
        }
    }
    public void ApFlash(int barNumber)
    {
        apFlash[barNumber].Flash();
    }
    private void EndTurnBGFade(float newAlpha)
    {
        switchTurnBG.CrossFadeAlpha(newAlpha, 0.3f, false);
    }

    public int GetActionsCount()
    {
        return actionsCount;
    }

    public void PreEndTurnCooldown()
    {
        Invoke(nameof(EndTurnEvent), 1f);
    }

    public void DisplayScoreBoard(bool victory)
    {
        DisplayEnemyInfo(0); //Enlève le display du enemy info
        scoreBoard.SetActive(true);

        if (victory)
        {
            combatResultText.text = "YOU WIN!";
            combatResultText.color = Color.cyan;
        }
        else
        {
            combatResultText.text = "YOU DIED";
            combatResultText.color = Color.red;
        }

        //calcul du bonus d'exp selon difficulté de l'ennemi
        int playerStats = player.GetExpStats();
        int enemyStats = enemy.GetExpStats();
        int enemyExpBonus = Mathf.Max(0, enemyStats * 10 / playerStats);

        int perfectCounter = player.perfectCounter;
        int quickTurnsCounter = player.quickTurnsCounter;
        int blockCounter = player.blockCounter;
        int counterAttackCounter = player.counterAttackCounter;
        int turnCounter = player.turnCounter;
        //int turnPenalty = Mathf.Min(0, 5 - turnCounter);

        int strenght = player.GetStrengthStat();
        int endurance = player.GetEnduranceStat();
        int speed = player.GetSpeedStat();
        int weaponDamage = player.GetWeaponDamageStat();
        int armor = player.GetArmorStat();
        int levelExp = player.levelExp;

        perfectCounterText.text = perfectCounter.ToString();
        quickTurnsCounterText.text = quickTurnsCounter.ToString();
        blockCounterText.text = blockCounter.ToString();
        counterAttackCounterText.text = counterAttackCounter.ToString();
        turnCounterText.text = turnCounter.ToString();
        enemyExpBonusText.text = enemyExpBonus.ToString() + " %";

        strengthText.text = strenght.ToString();
        enduranceText.text = endurance.ToString();
        speedText.text = speed.ToString();
        weaponDamageText.text = weaponDamage.ToString();
        armorText.text = armor.ToString();
        levelExpText.text = levelExp.ToString();

        int combatExp = player.combatExp;
        //int totalExp = perfectCounter + quickTurnsCounter + counterAttackCounter + blockCounter + turnPenalty;
        //totalExp += totalExp * enemyExpBonus / 100;

        totalExpText.text = combatExp.ToString();
    }

    private void UpdateScoreBoardStats()
    {
        int strength = player.GetStrengthStat();
        int endurance = player.GetEnduranceStat();
        int speed = player.GetSpeedStat();
        int weaponDamage = player.GetWeaponDamageStat();
        int armor = player.GetArmorStat();
        int levelExp = player.levelExp;

        strengthText.text = strength.ToString();
        enduranceText.text = endurance.ToString();
        speedText.text = speed.ToString();
        weaponDamageText.text = weaponDamage.ToString();
        armorText.text = armor.ToString();
        levelExpText.text = levelExp.ToString();

        UpdateText();

    }

    public void TogglePlayerBlockInput()
    {
        playerBlockInput = !playerBlockInput;
    }

    public void SetPlayerCounterInput(bool newPlayerCounterInput)
    {
        playerCounterInput = newPlayerCounterInput;
    }

    public int GetBlockCounter()
    {
        return blockCounter;
    }

    public void ResetBlockCounter()
    {
        blockCounter = 0;
    }

    private void TriggerPlusStrength()
    {
        player.IncreaseStat(Player.scoreBoardStats.STR);
        UpdateScoreBoardStats();
    }
    private void TriggerMinusStrength()
    {
        player.DecreaseStat(Player.scoreBoardStats.STR);
        UpdateScoreBoardStats();
    }
    private void TriggerPlusEndurance()
    {
        player.IncreaseStat(Player.scoreBoardStats.END);
        UpdateScoreBoardStats();
    }
    private void TriggerMinusEndurance()
    {
        player.DecreaseStat(Player.scoreBoardStats.END);
        UpdateScoreBoardStats();
    }
    private void TriggerPlusSpeed()
    {
        player.IncreaseStat(Player.scoreBoardStats.SPD);
        UpdateScoreBoardStats();
    }
    private void TriggerMinusSpeed()
    {
        player.DecreaseStat(Player.scoreBoardStats.SPD);
        UpdateScoreBoardStats();
    }
    private void TriggerPlusWeaponDamage()
    {
        player.IncreaseStat(Player.scoreBoardStats.WPNDMG);
        UpdateScoreBoardStats();
    }
    private void TriggerMinusWeaponDamage()
    {
        player.DecreaseStat(Player.scoreBoardStats.WPNDMG);
        UpdateScoreBoardStats();
    }
    private void TriggerPlusArmor()
    {
        player.IncreaseStat(Player.scoreBoardStats.ARMOR);
        UpdateScoreBoardStats();
    }
    private void TriggerMinusArmor()
    {
        player.DecreaseStat(Player.scoreBoardStats.ARMOR);
        UpdateScoreBoardStats();
    }

    public void SetupEnemyInfo(Vector3 newPosition)
    {
        creatureInfoContainer.GetComponent<CanvasGroup>().alpha = 0;
        creatureInfoContainer.SetActive(true);
        creatureInfoContainer.transform.position = newPosition;
        enemyNameText.text = enemy.GetName();
        enemyActionText.text = "";
        displayEnemyInfoTaskCounter = 0;
        enemyInfoTargetAlpha = 1;
        //StartCoroutine(FadeEnemyInfo(creatureInfoContainer, 1));
    }

    public void DisplayEnemyInfo(float targetAlpha)
    {
        if (targetAlpha == 1)
        {
            displayEnemyInfoTaskCounter--;
            if (displayEnemyInfoTaskCounter == 0)
            {
                enemyInfoTargetAlpha = targetAlpha;
            }
        }
        else
        {
            displayEnemyInfoTaskCounter++;
            enemyInfoTargetAlpha = targetAlpha;
        }
        //Debug.Log("Target alpha:" + enemyInfoTargetAlpha);
    }

    private void FadeEnemyInfo()
    {
        float currentAlpha = creatureInfoContainer.GetComponent<CanvasGroup>().alpha;
        creatureInfoContainer.GetComponent<CanvasGroup>().alpha = Mathf.Lerp(currentAlpha, enemyInfoTargetAlpha, Time.deltaTime * enemyInfoLerpSpeed / Time.timeScale);
    }

    public void UpdateEnemyStatus(string newStatusText)
    {
        enemyStatusText.text = newStatusText;
    }

    public void SetupPlayerAndEnemy()
    {
        player = EntityManager.main.GetPlayer();
        enemy = EntityManager.main.GetActiveEnemy();
    }

    public void DisplayEnemySequence()
    {
        enemyPhaseText.text = enemy.GetCurrentPhase().GetPhaseName();
        enemySequenceText.text = enemy.GetCurrentPhase().GetCurrentSequence().GetSequenceName() + " " + (enemy.GetCurrentPhase().GetCurrentSequence().GetCurrentMoveNumber() + 1) + "/" + enemy.GetCurrentPhase().GetCurrentSequence().GetEnemyMoveList().Count;
    }

    public void DisplayEnemyAction(string text)
    {
        enemyActionText.text = text;
    }
}

    
