﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MovingText : MonoBehaviour
{
    //private MovingText instance;
    private float movingSpeed;
    private Vector2 startingPosition;
    private Vector2 targetPosition;
    private bool isMoving;

    //private Vector2 currentPosition;
    [SerializeField]
    private float speedSlow = 0.5f;
    [SerializeField]
    private float speedMedium = 4.0f;
    [SerializeField]
    private float speedFast = 32.0f;
    private float[] movingSpeeds;


    void Start()
    {
        isMoving = false;
        startingPosition = transform.position;
        //currentPosition = transform.position;
        movingSpeeds = new float[] { speedSlow, speedMedium, speedFast };
        //Debug.Log(this.name + "  new color = " + GetComponent<Text>().color);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void MoveText()
    {

        float translation = movingSpeed * Time.deltaTime / Time.timeScale;
        transform.position = Vector2.MoveTowards(transform.position, targetPosition, translation);
    }

    public void MoveTo(int newSpeed, Vector2 newPosition)
    {
        movingSpeed = movingSpeeds[newSpeed];
        targetPosition = newPosition;
        isMoving = true;
    }

    public bool GetIsMoving()
    {
        return isMoving;
    }
    public Vector2 GetTargetPosition()
    {
        return targetPosition;
    }
    public void StopMoving()
    {
        movingSpeed = 0;
        targetPosition = transform.position;//changer pour tous les vecteurs
        isMoving = false;
    }
    public void MoveToStartingPosition()
    {
        transform.position = startingPosition;
    }
    public void ChangeText(string text)
    {
        GetComponent<Text>().text = text;
    }
    public void ChangeColor(Color newColor, float newChangeColorDuration)
    {
        //GetComponent<Text>().CrossFadeColor(newColor, newChangeColorDuration, false, true);
        StartCoroutine(SetColor(newColor, newChangeColorDuration));
    }

    public Color GetColor()
    {
        return GetComponent<Text>().color;
    }

    private IEnumerator SetColor(Color newColor, float newChangeColorDuration)
    {
        //float startAlpha = GetComponent<Text>().color.a;

        float rate = 1.0f / newChangeColorDuration;
        float progress = 0.0f;

        while (progress < 1.0)
        {
            Color tmpColor = GetComponent<Text>().color;
            GetComponent<Text>().color = new Color(Mathf.Lerp(tmpColor.r, newColor.r, progress), Mathf.Lerp(tmpColor.g, newColor.g, progress), Mathf.Lerp(tmpColor.b, newColor.b, progress), Mathf.Lerp(tmpColor.a, newColor.a, progress));
            progress += rate * Time.deltaTime / Time.timeScale;
            yield return null;
        }
       // Debug.Log(this.name + "  new color = " + GetComponent<Text>().color);
    }
}
