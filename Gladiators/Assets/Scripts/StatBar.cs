﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatBar : MonoBehaviour
{
    private Image main;

    [SerializeField]
    private float lerpSpeed;

    // Start is called before the first frame update
    void Start()
    {
        main = GetComponent<Image>();
        lerpSpeed = 4.0f;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void StatBarUpdate(float newFillAmount)
    {
        if (newFillAmount >= 0)
        {
            float currentFillAmount = main.fillAmount;
            main.fillAmount = Mathf.Lerp(currentFillAmount, newFillAmount, Time.deltaTime * lerpSpeed / Time.timeScale);
        }
        else
        {
            main.fillAmount = 0;
        }
    }
}
