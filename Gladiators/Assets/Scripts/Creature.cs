﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;


public class Creature
{
    ////Essayer d'éliminer cette classe pour utiliser seulement la classe Status
    //internal class CurrentStatus
    //{
    //    public Status currentStatus;
    //    public int currentStatusRemainingDuration;
    //    public int currentStatusEffectPower;
        
    //    public Status.StatusStartTurn currentStatusStartTurn;

    //    //public CurrentStatus(Status newStatus, int newEffectPower, int newDuration, Status.StatusStartTurn newStartTurn)
    //    //{
    //    //    currentStatus = newStatus;
    //    //    currentStatusEffectPower = newEffectPower;
    //    //    currentStatusRemainingDuration = newDuration;
    //    //    currentStatusStartTurn = newStartTurn;
    //    //}
    //}

    //private List<CurrentStatus> currentStatusList = new List<CurrentStatus> { };
    public List<Status> currentStatusList = new List<Status> { };
    private Status.StatusStartTurn currentStatusStartTurn;
    private int currentHp, maxHp, baseHp, currentAp, maxAp, strength, endurance, speed;
    private int weaponDamage, armor, superAttackMultiplier, nbHealingPotions;
    private int currentStrength, currentEndurance, currentSpeed, currentArmor, currentWeaponDamage;
    private float currentChainedAttacksMultiplier, firstChainedAttackMultiplier, secondChainedAttackMultiplier;
    private int bleedingEffectBaseDuration, blindingEffectBaseDuration;
    private float healingPower;

    private bool strFocusOn, endFocusOn, spdFocusOn;
    //public enum scoreBoardStats {STR, END, SPD, WPNDMG, ARMOR};
    private string statusText;

    public Creature(int baseHp, int strenght, int endurance, int speed, int weaponDamage, int armor, int superAttackMultiplier, float healingPower)
    {
        this.baseHp = baseHp;
        maxHp = baseHp + endurance * 20;
        
        currentHp = maxHp;
        
        maxAp = 5;
        currentAp = maxAp;

        this.strength = strenght;
        this.endurance = endurance;
        this.speed = speed;

        this.weaponDamage = weaponDamage;
        this.armor = armor;
        this.superAttackMultiplier = superAttackMultiplier;

        strFocusOn = false;
        endFocusOn = false;
        spdFocusOn = false;
        currentChainedAttacksMultiplier = 1;
        firstChainedAttackMultiplier = 1.5f;
        secondChainedAttackMultiplier = 2;

        bleedingEffectBaseDuration = 1;
        blindingEffectBaseDuration = 1;

        this.healingPower = healingPower;

        currentStatusList.Clear();
    }

    //public List<Status> GetStatus()
    //{
    //   return currentStatus;
    //}

    //public void AddStatus(Status newStatus, int newEffectPower, int newDuration)
    //{
    //    //SI LE STATUS N'EST PAS DÉJÀ DANS LA LISTE
    //    if(!currentStatusList.Exists(x => x.currentStatus.GetName().Equals(newStatus.GetName())))
    //    {
    //        if(GameController.main.GetGameState() == GameController.GameState.PlayerTurn)
    //        {
    //            currentStatusStartTurn = Status.StatusStartTurn.PLAYER;
    //        }
    //        else if(GameController.main.GetGameState() == GameController.GameState.EnemyTurn)
    //        {
    //            currentStatusStartTurn = Status.StatusStartTurn.ENEMY;
    //        }

    //        CurrentStatus newCurrentStatus = new CurrentStatus(newStatus, newEffectPower, newDuration, currentStatusStartTurn);
            
    //        //AJOUTE LE NOUVEAU STATUS
    //        currentStatusList.Add(newCurrentStatus);

    //        statusText = GenerateStatusText();
    //        //UPDATE VISUEL DU STATUS
    //        UIManager.main.UpdateEnemyStatus(statusText);
    //    }   
    //}

    // Ajoute un status dans la liste de l'ennemi.
    public void AddStatus(Status newStatus)
    {
        Debug.Log(newStatus.GetName() + "Status Added");
        bool uniqueStatus = true;
        //Regarde si le status est unique dans la liste
        foreach (Status s in currentStatusList)

        //if (currentStatusList.Exists(x => x.GetName().Equals(newStatus.GetName())))
        {
            
            if (s.GetName().Equals(newStatus.GetName()))
            {
                uniqueStatus = false;
                s.IncreaseDuration(newStatus.GetRemainingDuration());
            }
        }

        if(uniqueStatus)
        {
            currentStatusList.Add(newStatus);
            statusText = GenerateStatusText();
            //UPDATE VISUEL DU STATUS
            UIManager.main.UpdateEnemyStatus(statusText);
        }
        UpdateStatus(false);

    //if (GameController.main.GetGameState() == GameController.GameState.PlayerTurn)
    //{
    //    currentStatusStartTurn = Status.StatusStartTurn.PLAYER;
    //}
    //else if (GameController.main.GetGameState() == GameController.GameState.EnemyTurn)
    //{
    //    currentStatusStartTurn = Status.StatusStartTurn.ENEMY;
    //}



    //Status newCurrentStatus = new Status(newStatusName, newEffectsList, currentStatusStartTurn, newDuration);

    //AJOUTE LE NOUVEAU STATUS
    //currentStatusList.Add(newCurrentStatus);

           
        
    }


    //GÉNÉRATION DU DISPLAY DU STATUS
    private string GenerateStatusText()
    {
        string newStatusText = "NORMAL";

        if (currentStatusList.Count > 0) {
            newStatusText = "";
            foreach (Status s in currentStatusList)
            {
                newStatusText = newStatusText + s.GetName() + "(" + s.GetRemainingDuration() + ")";
                //SI CE N'EST PAS LE DERNIER STATUS DANS LA LISTE
                if (currentStatusList.IndexOf(s) < currentStatusList.Count - 1)
                {
                    newStatusText += "/";
                }
            }
        }
        
        return newStatusText;  
    }


    //Passe à travers la liste de status de la créature et applique les différents effets.
    public void UpdateStatus(bool startTurn)
    {
       
        //Réinitialisation des attributs

        currentStrength = strength;
        currentEndurance = endurance;
        currentSpeed = speed;
        currentArmor = armor;
        currentWeaponDamage = weaponDamage;


        Debug.Log("Count : " + currentStatusList.Count);
        foreach (Status s in currentStatusList)
        {
            Debug.Log("Looking at : " + s.GetName());
            foreach(Effect effect in s.GetStatusEffects())
            {

                //Switch qui pourrait être sorti de la fonction
                switch (effect.statusEffectId)
                {
                    case Effect.EffectId.TAKEDAMAGE:
                        if (startTurn) { 
                            currentHp -= effect.statusEffectPower;
                            Debug.Log("Damage " + effect.statusEffectPower);
                        }
                        break;
                    case Effect.EffectId.ATTACKUP:
                        Debug.Log("AttackUp " + effect.statusEffectPower);
                        currentWeaponDamage += effect.statusEffectPower;
                        break;
                    case Effect.EffectId.ATTACKDOWN:
                        Debug.Log("AttackDown " + effect.statusEffectPower);
                        currentWeaponDamage -= effect.statusEffectPower;
                        break;
                    case Effect.EffectId.DEFENSEUP:
                        Debug.Log("DefenseUp " + effect.statusEffectPower);
                        armor += effect.statusEffectPower;
                        break;
                    case Effect.EffectId.DEFENSEDOWN:
                        Debug.Log("DefenseDown " + effect.statusEffectPower);
                        armor -= effect.statusEffectPower;
                        break;
                    case Effect.EffectId.SKILLUP:
                        Debug.Log("SkillUp " + effect.statusEffectPower);
                        break;
                    case Effect.EffectId.SKILLDOWN:
                        Debug.Log("SkillDown " + effect.statusEffectPower);
                        break;
                    case Effect.EffectId.FOCUSLOCKED:
                        Debug.Log("FocusLocked " + effect.statusEffectPower);
                        break;


                }
            }
            if (startTurn) { 
                s.DecrementDuration();
            }
            //if (s.GetStatusStartTurn() == Status.StatusStartTurn.PLAYER && GameController.main.GetGameState() == GameController.GameState.EnemyTurn)
            //{
            //    s.IncrementDuration(-1);
            //}
            //else if(s.GetStatusStartTurn() == Status.StatusStartTurn.ENEMY && GameController.main.GetGameState() == GameController.GameState.PlayerTurn)
            //{
            //    s.IncrementDuration(-1);
            //}
        }
        currentStatusList.RemoveAll(x => x.GetRemainingDuration() <= 0);
        //foreach (Status s in currentStatusList)
        //{
        //    if (s.GetRemainingDuration() <= 0)
        //    {
        //        currentStatusList.Remove(s);
        //    }
        //}

        //GENERATION DU STATUS TEXT
        statusText = GenerateStatusText();
       
        //UPDATE VISUEL DU STATUS
        UIManager.main.UpdateEnemyStatus(statusText);
    }

    public void ClearStatusList()
    {
        currentStatusList.Clear();
        UIManager.main.UpdateEnemyStatus("NORMAL");
    }

    //public int CheckForStatusEffect(Status.StatusEffectId newEffect)
    //{
    //    //RETOURNE LE NOMBRE DE FOIS QUE L'EFFET A ÉTÉ TROUVÉ DANS LA CURRENTSTATUS LIST
    //    //DANS LE FUTUR: AVOIR UN PARAMÈTRE POWER QUI MULTIPLIE CET EFFET? TRAITER CHAQUE EFFET INDIVIDUELLEMENT?
    //    return currentStatusList.FindAll(x => x.currentStatus.GetStatusEffects().Exists(e => e.Equals(newEffect))).Count;  
    //}

    public int CheckForStatusEffect(Effect.EffectId newEffect)
    {
        //RETOURNE LE NOMBRE DE FOIS QUE L'EFFET A ÉTÉ TROUVÉ DANS LA CURRENTSTATUS LIST
        //DANS LE FUTUR: AVOIR UN PARAMÈTRE POWER QUI MULTIPLIE CET EFFET? TRAITER CHAQUE EFFET INDIVIDUELLEMENT?
        return currentStatusList.FindAll(x => x.GetStatusEffects().Exists(e => e.statusEffectId.Equals(newEffect))).Count;
    }


    public int GetDamage()
    {
        if (strFocusOn)
        {
            return (int)(weaponDamage * currentChainedAttacksMultiplier) + strength * 2;
        }
        return (int)(weaponDamage * currentChainedAttacksMultiplier) + strength;
    }

    public float GetHealingPower()
    {
        return healingPower;
    }

    public int GetMaxHp()
    {
        return maxHp;
    }
    public int GetCurrentHp()
    {
        return currentHp;
    }
    public int GetMaxAp()
    {
        return maxAp;
    }
    public int GetCurrentAp()
    {
        return currentAp;
    }
    public void RemoveAp(int apCost)
    {
        currentAp -= apCost;
    }

    public void ResetAp()
    {
        currentAp = maxAp;
    }

    public void TakeDamage(int damage)
    {
        currentHp = Math.Max(0, currentHp - damage);
    }
    public void DamageTarget(Creature targetCreature, int damage)
    {
        targetCreature.TakeDamage(damage);
    }
    public int GetSuperAttackMultiplier()
    {
        return superAttackMultiplier;
    }

    public void HealDamage(int healedDamage)
    {
        currentHp = Math.Min(maxHp, currentHp + healedDamage);
    }

    public int GetDefense()
    {
        int baseDefense;
        float defenseUpMultiplier;
        int defenseDownMultiplier;
        int totalDefense;

        if (endFocusOn)
        {
            baseDefense = currentEndurance * 2 + currentArmor;
            //defenseUpMultiplier = 1 + CheckForStatusEffect(Effect.EffectId.DEFENSEUP) / 2;
            //defenseDownMultiplier = 1 + CheckForStatusEffect(Effect.EffectId.DEFENSEDOWN);
            //totalDefense = (int)((baseDefense * defenseUpMultiplier) / defenseDownMultiplier);
            //Debug.Log("Total defense:" + totalDefense);
            return baseDefense;
            //return ((endurance * 2 + armor) * (1 + CheckForStatusEffect(Status.Effect.DEFENSEUP) / 2)) / (2 + CheckForStatusEffect(Status.Effect.DEFENSEDOWN));
        }
        baseDefense = currentEndurance + currentArmor;
        //Debug.Log("Total defense:" + totalDefense);
        return baseDefense;
        //return ((endurance + armor) * (1 + CheckForStatusEffect(Status.Effect.DEFENSEUP) / 2)) / (2 + CheckForStatusEffect(Status.Effect.DEFENSEDOWN));
    }

    public int GetSpeed()
    {
        if (spdFocusOn)
        {
            return speed * 2;
        }
        return speed;
    }

    public int GetStrength()
    {
        if (strFocusOn)
        {
            return strength * 2;
        }
        return strength;
    }

    public int GetBleedingEffectDuration()
    {
        return bleedingEffectBaseDuration + GetStrength() / 25;
    }

    public int GetBlindingEffectDuration()
    {
        return blindingEffectBaseDuration + GetSpeed() / 25;
    }

    public int GetBleedingEffectPower()
    {
        return (GetStrength() + weaponDamage) / 2;
    }

    public int GetBlindingEffectPower()
    {
        return 10 + GetSpeed();
    }

    public void ActivateFocus(int focusType)
    {
        switch (focusType)
        {
            case 1: strFocusOn = true;
                break;

            case 2: endFocusOn = true;
                break;

            case 3: spdFocusOn = true;
                break;
        }
    }

    public void DeactivateFocus()
    {
        strFocusOn = false;
        endFocusOn = false;
        spdFocusOn = false;
    }

    public bool GetFocus(int focusType)
    {
        if (strFocusOn && focusType == 1)
        {
            return true;
        }
        if (endFocusOn && focusType == 2)
        {
            return true;
        }
        if (spdFocusOn && focusType == 3)
        {
            return true;
        }
        return false;
    }

    public void SetStats(int baseHp, int strength, int endurance, int speed, int weaponDamage, int armor, int superAttackMultiplier, int nbHealingPotions)
    {
        this.baseHp = baseHp;
        this.strength = strength;
        this.endurance = endurance;
        this.speed = speed;

        this.weaponDamage = weaponDamage;
        this.armor = armor;
        this.superAttackMultiplier = superAttackMultiplier;
        this.nbHealingPotions = nbHealingPotions;
    }

    public int GetExpStats()
    {
        return maxHp + strength + endurance + speed;
    }

    public void UseItem(Creature targetCreature)
    {
        targetCreature.nbHealingPotions--;
        int healedDamage = targetCreature.maxHp / 2;
        targetCreature.HealDamage(healedDamage);
    }

    public int GetNbHealingPotions()
    {
        return nbHealingPotions;
    }

    public void ResetStats()
    {
        DeactivateFocus();
        currentHp = maxHp; 
    }

    public void LevelUp()
    {
        baseHp = baseHp * 5 / 4;
        strength += 2;
        endurance += 2;
        speed += 2;
        maxHp = baseHp + endurance * 20;
        currentHp = maxHp;
        weaponDamage += 2;
        armor += 1;
    }

    public void IncreaseStrengthStat(int increment)
    {
        strength += increment;
    }

    public void IncreaseEnduranceStat(int increment)
    {
        endurance += increment;
    }

    public void IncreaseSpeedStat(int increment)
    {
        speed += increment;
    }

    public void IncreaseWeaponDamage(int increment)
    {
        weaponDamage += increment;
    }

    public void IncreaseArmor(int increment)
    {
        armor += increment;
    }

    public void SetMaxHp(int newMaxHp)
    {
        maxHp = newMaxHp;
    }

    public void SetCurrentHp(int newCurrentHp)
    {
        currentHp = newCurrentHp;
    }

    public int GetBaseHp()
    {
        return baseHp;
    }

    public int GetWeaponDamage()
    {
        return weaponDamage;
    }

    public int GetStrengthStat()
    {
        return strength;
    }

    public int GetEnduranceStat()
    {
        return endurance;
    }
    public int GetSpeedStat()
    {
        return speed;
    }
    public int GetWeaponDamageStat()
    {
        return weaponDamage;
    }
    public int GetArmorStat()
    {
        return armor;
    }

    public void SetChainedAttacks(int chainedAttackNumber)
    {
        switch (chainedAttackNumber)
        {
            case 0:
                currentChainedAttacksMultiplier = 1;
                Debug.Log("CHAIN ATTACKS RESET TO NORMAL");
                break;

            case 1:
                currentChainedAttacksMultiplier = firstChainedAttackMultiplier;
                Debug.Log("CHAINING ATTACKS FIRST LEVEL");
                break;

            case 2:
                currentChainedAttacksMultiplier = secondChainedAttackMultiplier;
                Debug.Log("CHAINED ATTACKS SECOND LEVEL");
                break;
        }
    }

    public float GetChainedAttacksMultiplier()
    {
        return currentChainedAttacksMultiplier;
    }

    
}
