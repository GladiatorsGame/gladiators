﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;

/*
 * Classe qui contient le joueur et les enemy
 */


public class EntityManager : MonoBehaviour
{
    public static EntityManager main;

    [Header("Player Settings")]
    public int playerBaseHp;
    public int playerStrenght;
    public int playerEndurance;
    public int playerSpeed;
    public int playerWeaponDamage;
    public int playerArmor;
    public int playerNbHealingPotions;
    public int superAttackMultiplier;

    [Header("Enemy Settings")]
    public List<EnemyData> enemyDataList;

    private Player mainCharacter;

    private List<Enemy> enemyList;
    private Enemy activeEnemy, moucheReal;
    private int currentEnemyNumber;
    private bool firstCombat = true;

    

    


    // Start is called before the first frame update
    void Start()
    {
        enemyList = new List<Enemy>();
        mainCharacter = new Player(playerBaseHp, playerStrenght, playerEndurance, playerSpeed, playerWeaponDamage, playerArmor, superAttackMultiplier, playerNbHealingPotions);

        foreach(EnemyData enemyData in enemyDataList)
        {
            Enemy newEnemy = new Enemy(enemyData.enemyName, enemyData.enemyTag, enemyData.prefab, enemyData.baseHp, enemyData.strenght, enemyData.endurance, enemyData.speed, enemyData.weaponDamage, enemyData.armor, enemyData.superAttackMultiplier, enemyData.healingPower, enemyData.perfectCheck, enemyData.doubleCheck, enemyData.halfCheck);
            enemyList.Add(newEnemy);
        }

        currentEnemyNumber = 0;
        activeEnemy = enemyList[0];
        UIManager.main.SetupPlayerAndEnemy();
        UIManager.main.UpdateText();
    }
    void Awake()
    {
        main = this;
    }

    void Update()
    {

    }

    public Player GetPlayer()
    {
        return mainCharacter;
    }
    public Enemy GetActiveEnemy()
    {
        return activeEnemy;
    }
    public void SetNewEnemy()
    {
        //if(firstCombat)
        //{
        //    firstCombat = false;
        //    enemyList[0] = moucheReal;
        //}

        if (currentEnemyNumber < 2)
        {
            currentEnemyNumber++;
            activeEnemy = enemyList[currentEnemyNumber];
        }
        else
        {
            
            foreach(Enemy enemy in enemyList)
            {
                enemy.ResetStats();
                enemy.LevelUp();
            }
            currentEnemyNumber = 0;
            activeEnemy = enemyList[currentEnemyNumber];
        }

    }

    public int GetCurrentEnemyNumber()
    {
        return currentEnemyNumber;
    }

    //public void CreateEnemyObject(Vector3 position)
    //{
    //    Instantiate(activeEnemy.GetPrefab(), activeEnemy.GetPrefab().transform.position, Quaternion.identity);
    //}

    public GameObject CreateEnemyObject(Vector3 position)
    {
        GameObject enemyObject = Instantiate(activeEnemy.GetPrefab(), activeEnemy.GetPrefab().transform.position, Quaternion.identity);
        return enemyObject;
    }

}