﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using System.Collections;
using System.Linq;

public class Loop
{
    public AudioSource audioSource;
    public int index;

}

public class SoundManagerN : MonoBehaviour
{
    public static SoundManagerN instance = null;     //Allows other scripts to call functions from SoundManager.   

    public enum AudioBank { PLAYER_ATTACK, PLAYER_HIT, PLAYER_FOCUS, ENEMY_ATTACK_START, ENEMY_ATTACK_LOOP, ENEMY_ATTACK_END, ENEMY_SUPERATTACK_START, ENEMY_SUPERATTACK_LOOP, ENEMY_SUPERATTACK_END, ENEMY_HIT, ENEMY_FOCUS, UI, QTESUCCESS, QTEFAIL, QTESTART, QTEPROGRESS, QTEEND, SWOOSH, SWOOSHREVERSE}
    public AudioClip[] clipsPlayerAttack;
    public AudioClip[] clipsPlayerHit;
    public AudioClip[] clipsPlayerFocus;
    public EnemySoundbank[] enemySoundbank;
    public AudioClip[] clipsUI;
    public AudioClip[] clipsQteSuccess;
    public AudioClip[] clipsQteFail;
    public AudioClip[] clipsQteStart;
    public AudioClip[] clipsQteProgress;
    public AudioClip[] clipsQteEnd;
    public AudioClip[] clipsSwoosh;
    public AudioClip[] clipsSwooshReverse;
    private AudioClip[] currentAudioClips;

    private int currentEnemyNumber;
    private int enemyLoopRandomIndex;
    public List<Loop> loopList = new List<Loop>();


    public AudioMixer audioMixer;
    private AudioMixerGroup[] audioMixerGroups;

    private AudioSource enemySfxSource;
    private AudioSource enemySfxLoopSource;
    private AudioSource playerSfxSource;
    private AudioSource uiSfxSource;
    private AudioSource musicSource;
    private AudioSource qteAnimationSfxSource;
    private AudioSource currentAudioSource;
    private AudioSource qteAnimationSfxLoopSource;
    private List<AudioSource> SfxAudioSourceList = new List<AudioSource>();



    public float lowPitchRange = 0.95f;              //The lowest a sound effect will be randomly pitched.
    public float highPitchRange = 1.05f;            //The highest a sound effect will be randomly pitched.
    private AudioSource qteProgressSfxSource;
    
    


    void Awake()
    {
        //Check if there is already an instance of SoundManager
        if (instance == null)
            //if not, set it to this.
            instance = this;
        //If instance already exists: 
        else if (instance != this)
            //Destroy this, this enforces our singleton pattern so there can only be one instance of SoundManager.
            Destroy(gameObject);

        //Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
        DontDestroyOnLoad(gameObject);


        ////SETUP des audiosources
        playerSfxSource = new GameObject("PlayerSfxAudioSource", typeof(AudioSource)).GetComponent<AudioSource>();
        enemySfxSource = new GameObject("EnemySfxAudioSource", typeof(AudioSource)).GetComponent<AudioSource>();
        enemySfxLoopSource = new GameObject("EnemySfxLoopAudioSource", typeof(AudioSource)).GetComponent<AudioSource>();
        uiSfxSource = new GameObject("UISfxAudioSource", typeof(AudioSource)).GetComponent<AudioSource>();
        musicSource = new GameObject("MusicAudioSource", typeof(AudioSource)).GetComponent<AudioSource>();
        qteAnimationSfxSource = new GameObject("QTEAnimationSfxAudioSource", typeof(AudioSource)).GetComponent<AudioSource>();
        qteAnimationSfxLoopSource = new GameObject("QTEAnimationSfxAudioSource", typeof(AudioSource)).GetComponent<AudioSource>();


        playerSfxSource.gameObject.hideFlags = HideFlags.HideAndDontSave;
        enemySfxSource.gameObject.hideFlags = HideFlags.HideAndDontSave;
        enemySfxLoopSource.gameObject.hideFlags = HideFlags.HideAndDontSave;
        enemySfxLoopSource.loop = true;
        uiSfxSource.gameObject.hideFlags = HideFlags.HideAndDontSave;
        musicSource.gameObject.hideFlags = HideFlags.HideAndDontSave;
        qteAnimationSfxSource.gameObject.hideFlags = HideFlags.HideAndDontSave;
        qteAnimationSfxLoopSource.gameObject.hideFlags = HideFlags.HideAndDontSave;
        qteAnimationSfxLoopSource.loop = true;

        ////ENVOYER SIGNAL DANS AUDIOMIXER
        ////récupère le groupe SFX du MasterMixer
        audioMixerGroups = audioMixer.FindMatchingGroups("SFX");

        playerSfxSource.outputAudioMixerGroup = audioMixerGroups[0];
        enemySfxSource.outputAudioMixerGroup = audioMixerGroups[0];
        uiSfxSource.outputAudioMixerGroup = audioMixerGroups[0];
        qteAnimationSfxSource.outputAudioMixerGroup = audioMixerGroups[0];

        SfxAudioSourceList.Add(playerSfxSource);
        SfxAudioSourceList.Add(enemySfxSource);
        SfxAudioSourceList.Add(enemySfxLoopSource);
        SfxAudioSourceList.Add(uiSfxSource);
        SfxAudioSourceList.Add(qteAnimationSfxSource);
        SfxAudioSourceList.Add(qteAnimationSfxLoopSource);

        audioMixerGroups = audioMixer.FindMatchingGroups("Music");

        musicSource.outputAudioMixerGroup = audioMixerGroups[0];
    }

    private void SetClipsAndAudioSource(AudioBank audiobank)
    {
        switch (audiobank)
        {
            case AudioBank.PLAYER_ATTACK:
                currentAudioSource = playerSfxSource;
                currentAudioClips = clipsPlayerAttack;
                return;
            case AudioBank.PLAYER_HIT:
                currentAudioSource = playerSfxSource;
                currentAudioClips = clipsPlayerHit;
                return;
            case AudioBank.PLAYER_FOCUS:
                currentAudioSource = playerSfxSource;
                currentAudioClips = clipsPlayerFocus;
                return;
            case AudioBank.ENEMY_ATTACK_START:
                currentAudioSource = enemySfxSource;
                currentAudioClips = enemySoundbank[EntityManager.main.GetCurrentEnemyNumber()].clipsEnemyAttackStart;
                return;
            case AudioBank.ENEMY_ATTACK_LOOP:
                currentAudioSource = enemySfxLoopSource;
                currentAudioClips = enemySoundbank[EntityManager.main.GetCurrentEnemyNumber()].clipsEnemyAttackLoop;
                return;
            case AudioBank.ENEMY_ATTACK_END:
                currentAudioSource = enemySfxSource;
                currentAudioClips = enemySoundbank[EntityManager.main.GetCurrentEnemyNumber()].clipsEnemyAttackEnd;
                return;
            case AudioBank.ENEMY_SUPERATTACK_START:
                currentAudioSource = enemySfxSource;
                currentAudioClips = enemySoundbank[EntityManager.main.GetCurrentEnemyNumber()].clipsEnemySuperAttackStart;
                return;
            case AudioBank.ENEMY_SUPERATTACK_LOOP:
                currentAudioSource = enemySfxLoopSource;
                currentAudioClips = enemySoundbank[EntityManager.main.GetCurrentEnemyNumber()].clipsEnemySuperAttackLoop;
                return;
            case AudioBank.ENEMY_SUPERATTACK_END:
                currentAudioSource = enemySfxSource;
                currentAudioClips = enemySoundbank[EntityManager.main.GetCurrentEnemyNumber()].clipsEnemySuperAttackEnd;
                return;
            case AudioBank.ENEMY_HIT:
                currentAudioSource = enemySfxSource;
                currentAudioClips = enemySoundbank[EntityManager.main.GetCurrentEnemyNumber()].clipsEnemyHit;
                return;
            case AudioBank.ENEMY_FOCUS:
                currentAudioSource = enemySfxSource;
                currentAudioClips = enemySoundbank[EntityManager.main.GetCurrentEnemyNumber()].clipsEnemyFocus;
                return;
            case AudioBank.UI:
                currentAudioSource = uiSfxSource;
                currentAudioClips = clipsUI;
                return;
            case AudioBank.QTESUCCESS:
                currentAudioSource = uiSfxSource;
                currentAudioClips = clipsQteSuccess;
                return;
            case AudioBank.QTEFAIL:
                currentAudioSource = uiSfxSource;
                currentAudioClips = clipsQteFail;
                return;
            case AudioBank.QTESTART:
                currentAudioSource = qteAnimationSfxSource;
                currentAudioClips = clipsQteStart;
                return;
            case AudioBank.QTEPROGRESS:
                currentAudioSource = qteAnimationSfxLoopSource;
                currentAudioClips = clipsQteProgress;
                return;
            case AudioBank.QTEEND:
                currentAudioSource = qteAnimationSfxSource;
                currentAudioClips = clipsQteEnd;
                return;
            case AudioBank.SWOOSH:
                currentAudioSource = uiSfxSource;
                currentAudioClips = clipsSwoosh;
                return;
            case AudioBank.SWOOSHREVERSE:
                currentAudioSource = uiSfxSource;
                currentAudioClips = clipsSwooshReverse;
                return;

            default:
                currentAudioClips = new AudioClip[] { };
                return;
        }
    }

    //Used to play single sound clips.
    public void PlaySingle(AudioClip clip)
    {
        ////Set the clip of our efxSource audio source to the clip passed in as a parameter.
        //sfxSource.clip = clip;

        ////Play the clip.
        //sfxSource.Play();
    }

    public void PlayOrderedSfx(float delayTime, AudioBank audiobank, int index)
    {

    }

    public void PlayRandomSfx(float delayTime, AudioBank audiobank)
    {

    }


    //RandomizeSfx chooses randomly between various audio clips and slightly changes their pitch.
    public void PlaySfx(float delayTime, AudioBank audiobank)
    {
        SetClipsAndAudioSource(audiobank);
        if (currentAudioClips.Length > 0) {
            int randomIndex = Random.Range(0, currentAudioClips.Length);
            currentAudioSource.clip = currentAudioClips[randomIndex];
            //currentAudioSource.PlayDelayed(delayTime);
            currentAudioSource.PlayOneShot(currentAudioClips[randomIndex]);
        }
        else
        {
            Debug.Log("Missing Sound:" + audiobank.ToString());
        }

    }


    //Fonction qui joue le starting sound d'une loop et le son loopé, retourne l'audiosource du son
    //loopé pour future Stop et Destroy
    public void PlaySfxLoop(AudioBank audiobankStart, AudioBank audiobankLoop)
    {
        //récupération du clip qui commence la loop
        SetClipsAndAudioSource(audiobankStart);
        if (currentAudioClips.Length > 0)
        {
            int randomIndex = Random.Range(0, currentAudioClips.Length);
            currentAudioSource.clip = currentAudioClips[randomIndex];
            currentAudioSource.PlayOneShot(currentAudioClips[randomIndex]);
            float delayTime = currentAudioSource.clip.length;
            //Récupération du clip pour la loop
            SetClipsAndAudioSource(audiobankLoop);
            loopList.Add(new Loop() { audioSource = currentAudioSource, index = randomIndex });
            Debug.Log("Start Loop Index:" + randomIndex);
            if (currentAudioClips.Length > 0)
            {
                currentAudioSource.clip = currentAudioClips[randomIndex];
                currentAudioSource.loop = true;
                currentAudioSource.PlayDelayed(delayTime);
            }
            else
            {
                Debug.Log("Missing Sound:" + audiobankLoop.ToString());
            }
        }
        else
        {
            Debug.Log("Missing Sound:" + audiobankStart.ToString());
        }
    }

    //fonction qui arrête un son joué en loop et détruit l'audiosource
    public void StopSfxLoop(AudioBank audiobankLoop, AudioBank audiobankEnd)
    {
        SetClipsAndAudioSource(audiobankLoop);
        AudioSource loopAudioSource = currentAudioSource;
        Loop currentLoop = loopList.Find(x => x.audioSource == currentAudioSource);
        loopList.Remove(currentLoop);
        SetClipsAndAudioSource(audiobankEnd);
        currentAudioSource.clip = currentAudioClips[currentLoop.index];
        currentAudioSource.PlayOneShot(currentAudioClips[currentLoop.index]);
        //loopAudioSource.Stop();
        StartCoroutine(FadeOut(loopAudioSource, 0.1f));
        Debug.Log("End Loop Index:" + currentLoop.index);
    }

    private IEnumerator FadeOut(AudioSource audioSource, float duration)
    {
        float startVol = audioSource.volume;
        while (audioSource.volume > 0)
        {
            audioSource.volume -= startVol * Time.deltaTime / duration * Time.timeScale;
            yield return null;
        }
        audioSource.Stop();
        audioSource.volume = startVol;
        //yield return new WaitForSeconds(duration);
    }

    public void PrioritizeAudioSources(AudioBank[] audiobanks)
    {
        List<AudioSource> prioritizedAudioSources = new List<AudioSource>();
        foreach (AudioBank audiobank in audiobanks)
        {
            SetClipsAndAudioSource(audiobank);
            prioritizedAudioSources.Add(currentAudioSource);
        }
        foreach (AudioSource audioSource in SfxAudioSourceList)
        {
            if (!prioritizedAudioSources.Contains(audioSource))
            {
                audioSource.volume = 0.25f;
            }
        }
    }
    public void DeprioritizeAudioSources()
    {
        foreach (AudioSource audioSource in SfxAudioSourceList)
        {
             audioSource.volume = 1;
        }
    }



    //public void PlaySfx(float delayTime, AudioBank audiobank)
    //{
    //    AudioClip[] clips = GetClipsOfAudioBank(audiobank);

    //    //Generate a random number between 0 and the length of our array of clips passed in.
    //    int randomIndex = Random.Range(0, clips.Length);

    //    //PROBABLEMENT SI clips.Length = 1 (1 seul clip) ne pas faire de random pitch.
    //    //Choose a random pitch to play back our clip at between our high and low pitch ranges.
    //    // float randomPitch = Random.Range(lowPitchRange, highPitchRange);

    //    //Set the pitch of the audio source to the randomly chosen pitch.
    //    //currentAudioSource.pitch = randomPitch;

    //    //Set the clip to the clip at our randomly chosen index.
    //    currentAudioSource.clip = clips[randomIndex];

    //    ////ENVOYER SIGNAL DANS AUDIOMIXER
    //    ////récupère le groupe SFX du MasterMixer
    //    //AudioMixerGroup[] audioMixGroup = audioMixer.FindMatchingGroups("SFX");

    //    ////Assign the AudioMixerGroup to AudioSource (Use first index)
    //    //currentAudioSource.outputAudioMixerGroup = audioMixGroup[0];

    //    //JOUER LE SON
    //    //s'il y a un délai sur le son, il est joué avec le délai, sinon joué normalement
    //    currentAudioSource.PlayDelayed(delayTime);

    //}



    //public void PlayQteProgressSfx(int index)
    //{

    //    //CODE QUI CRÉÉ UNE NOUVELLE AUDIOSOURCE À CHAQUE FOIS QUE LA MÉTHODE EST APPELÉE
    //    qteProgressSfxSource = new GameObject("SFX - " + AudioBank.QTEPROGRESS.ToString(), typeof(AudioSource)).GetComponent<AudioSource>();
    //    qteProgressSfxSource.gameObject.hideFlags = HideFlags.HideAndDontSave;

    //    AudioClip[] clips = GetClipsOfAudioBank(AudioBank.QTEPROGRESS);

    //    //Set the clip to the clip at our randomly chosen index.
    //    qteProgressSfxSource.clip = clips[index];

    //    //ENVOYER SIGNAL DANS AUDIOMIXER
    //    //récupère le groupe SFX du MasterMixer
    //    AudioMixerGroup[] audioMixGroup = audioMixer.FindMatchingGroups("SFX");

    //    //Assign the AudioMixerGroup to AudioSource (Use first index)
    //    qteProgressSfxSource.outputAudioMixerGroup = audioMixGroup[0];

    //    //JOUER LE SON
    //    //s'il y a un délai sur le son, il est joué avec le délai, sinon joué normalement
    //    qteProgressSfxSource.Play();

    //    //Détruit l'audiosource sfxSource avec un délai de 1 seconde de plus que la durée du clip joué
    //    Destroy(qteProgressSfxSource.gameObject, qteProgressSfxSource.clip.length + 1);

    //}

    //public void PlayQteProgressSfx(int index)
    //{

    //    //CODE QUI CRÉÉ UNE NOUVELLE AUDIOSOURCE À CHAQUE FOIS QUE LA MÉTHODE EST APPELÉE
    //    qteProgressSfxSource = new GameObject("SFX - " + AudioBank.QTEPROGRESS.ToString(), typeof(AudioSource)).GetComponent<AudioSource>();
    //    qteProgressSfxSource.gameObject.hideFlags = HideFlags.HideAndDontSave;

    //    AudioClip[] clips = GetClipsOfAudioBank(AudioBank.QTEPROGRESS);

    //    //Set the clip to the clip at our randomly chosen index.
    //    qteProgressSfxSource.clip = clips[index];

    //    //ENVOYER SIGNAL DANS AUDIOMIXER
    //    //récupère le groupe SFX du MasterMixer
    //    AudioMixerGroup[] audioMixGroup = audioMixer.FindMatchingGroups("SFX");

    //    //Assign the AudioMixerGroup to AudioSource (Use first index)
    //    qteProgressSfxSource.outputAudioMixerGroup = audioMixGroup[0];

    //    //JOUER LE SON
    //    //s'il y a un délai sur le son, il est joué avec le délai, sinon joué normalement
    //    qteProgressSfxSource.Play();

    //    //Détruit l'audiosource sfxSource avec un délai de 1 seconde de plus que la durée du clip joué
    //    Destroy(qteProgressSfxSource.gameObject, qteProgressSfxSource.clip.length + 1);

    //}
}

