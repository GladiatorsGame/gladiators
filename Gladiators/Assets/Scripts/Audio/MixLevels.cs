﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class MixLevels : MonoBehaviour
{
    public AudioMixer masterMixer; //mastermixer
    public Slider sliderSfx; //slider de réglage du volume des SFX
    public Slider sliderMusic; //slider de réglage du volume de la musique
    
    public void SetSfxLvl(float sfxLvl)
    {
        //si le volume est à la valeur minimum du slider, mettre le volume à -80dB (off)
        if (sfxLvl == sliderSfx.minValue)
        {
            masterMixer.SetFloat("sfxVol", -80);  //sfx
        }
        else
        {
            masterMixer.SetFloat("sfxVol", sfxLvl);
        }
        
    }
    public void SetMusicLvl(float musicLvl)
    {
        //si le volume est à la valeur minimum du slider, mettre le volume à -80dB (off)
        if (musicLvl == sliderMusic.minValue)
        {
            masterMixer.SetFloat("musicVol", -80);
        }
        else
        {
            masterMixer.SetFloat("musicVol", musicLvl);
        }

        masterMixer.SetFloat("musicVol", musicLvl);
    }
    //pour retourner aux niveaux du snapshot
    public void ClearVolume()
    {
        masterMixer.ClearFloat("sfxVol");
        masterMixer.ClearFloat("musicVol");
    }
}
