﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using System.Collections;

[System.Serializable]
public class EnemySoundbank
{
    public AudioClip[] clipsEnemyAttackStart;
    public AudioClip[] clipsEnemyAttackLoop;
    public AudioClip[] clipsEnemyAttackEnd;
    public AudioClip[] clipsEnemySuperAttackStart;
    public AudioClip[] clipsEnemySuperAttackLoop;
    public AudioClip[] clipsEnemySuperAttackEnd;
    public AudioClip[] clipsEnemyHit;
    public AudioClip[] clipsEnemyFocus;
    //private EnemySoundbank enemySoundbank;

    //public EnemySoundbank GetEnemySoundbank()
    //{
    //    return enemySoundbank;
    //}
}