﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class Combo
{
    private List<string> actionList;
    //public int currentAction;
    private int specialChargeValue, strCharge, endCharge, spdCharge, focusBonus;
    public enum ChargeType{STR, SPD, END, ALL};
    private ChargeType specialChargeType;
    private bool chainingAttacks, heavyAttackSkills, lightAttackSkills;
    private string name;

    public Combo(List<string> newActionList, int newSpecialChargeValue, ChargeType newSpecialChargeType, bool newChainingAttacks, int newStrCharge, int newEndCharge, int newSpdCharge, int newFocusBonus, string newName, bool newHeavyAttackSkills, bool newLightAttackSkills)
    {
        actionList = newActionList;
        specialChargeValue = newSpecialChargeValue;
        specialChargeType = newSpecialChargeType;
        chainingAttacks = newChainingAttacks;
        heavyAttackSkills = newHeavyAttackSkills;
        lightAttackSkills = newLightAttackSkills;
        strCharge = newStrCharge;
        endCharge = newEndCharge;
        spdCharge = newSpdCharge;
        focusBonus = newFocusBonus;
        name = newName;
        
    }

    public ChargeType GetSpecialChargeType()
    {
        return specialChargeType;
    }

    public List<string> GetActionList()
    {
        return actionList;
    }

    public int GetSpecialChargeValue()
    {
        return specialChargeValue;
    }

    public int GetFocusBonus()
    {
        return focusBonus;
    }

    public int GetStrCharge()
    {
        return strCharge;
    }
    public int GetEndCharge()
    {
        return endCharge;
    }

    public int GetSpdCharge()
    {
        return spdCharge;
    }

    public bool GetChainingAttacksValue()
    {
        return chainingAttacks;
    }

    public string GetName()
    {
        return name;
    }

    public bool GetHeavyAttackSkills()
    {
        return heavyAttackSkills;
    }

    public bool GetLightAttackSkills()
    {
        return lightAttackSkills;
    }

}