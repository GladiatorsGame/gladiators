﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Enemy : Creature
{
    private string enemyName;
    public enum EnemyTags { Tutorial, Flyme, Fireball };
    public EnemyTags enemyTag;
    private GameObject prefab;

    private List<EnemyPhase> phaseList;
    private int currentPhase;
    
    public enum enemyPhaseTags { NORMAL, DISRUPTED, PANIC };

    private int perfectCheck, doubleCheck, halfCheck;

    private int skillCheck, skillSpeedModifier, skillStatusModifier;
    private int turnDamageCounter;
    
    
    public enum enemySequenceTags { ATTACK, DEFEND, WAIT, CHARGE, HEAL};
 


    private Player player;

    
    public Enemy(string enemyName, EnemyTags enemyTag, GameObject prefab, int baseHp, int strenght, int endurance, int speed, int weaponDamage, int armor, int superAttackMultiplier, float healingPower, int perfectCheck, int doubleCheck, int halfCheck) : base(baseHp, strenght, endurance, speed, weaponDamage, armor, superAttackMultiplier, healingPower)
    {

        this.enemyName = enemyName;
        this.enemyTag = enemyTag;
        this.prefab = prefab;
        this.perfectCheck = perfectCheck;
        this.halfCheck = halfCheck;
        this.doubleCheck = doubleCheck;


        currentPhase = 0;
        GenerateEnemyActions();

        player = EntityManager.main.GetPlayer();
    }

    public int GetSkillCheck()
    {
        skillCheck = Random.Range(0, 100);
        //Debug.Log("Skill Check: " + skillCheck);

        //MODIFICATION DU SKILL DE L'ENNEMI SELON SPEED DU PLAYER
        if (player.GetSpecialActive())
        {
            skillSpeedModifier = (GetSpeed() - Mathf.Min(100, player.GetSpeed() + player.GetSpeed() * player.GetSpecialSpdCharge() / 100)) / 4;

        }
        else
        {
            skillSpeedModifier = (GetSpeed() - player.GetSpeed()) / 4;
        }
        Debug.Log("Enemy skill Speed Modifier:" + skillSpeedModifier);
               
        //MODIFICATION DU SKILL SELON STATUS
        //DANS LE FUTUR: FACTEUR DE 25 DEVRAIT ÊTRE BASÉ SUR LE POWER DU PLAYER OU QQCH DU GENRE
        skillStatusModifier = CheckForStatusEffect(Effect.EffectId.SKILLUP) * 25 - CheckForStatusEffect(Effect.EffectId.SKILLDOWN) * 25;

        //APPLICATION DES MODIFICATEURS
        skillCheck = skillCheck + skillSpeedModifier + skillStatusModifier;

        Debug.Log("Skill status mod:" + skillStatusModifier);
        
        //VALEUR DU SKILLCHECK AJUSTÉE ENTRE 0 ET 100
        skillCheck = Mathf.Clamp(skillCheck, 0, 100);
                     
        return skillCheck;
    }

    public GameObject GetPrefab()
    {
        return prefab;
    }

    public int GetPerfectCheck()
    {
        return perfectCheck;
    }

    public int GetDoubleCheck()
    {
        return doubleCheck;
    }

    public int GetHalfCheck()
    {
        return halfCheck;
    }

    public string GetName()
    {
        return enemyName;
    }

    public List<EnemyPhase> GetPhasesList()
    {
        return phaseList;
    }

    public int GetCurrentPhaseNumber()
    {
        return currentPhase;
    }

    public EnemyPhase GetCurrentPhase()
    {
        return phaseList[currentPhase];
    }


    //Méthode qui change la Phase d'un enemy
    public void SetPhase(int newPhaseNumber)
    {
        //Reset des séquences de la phase quittée
        foreach (EnemySequence s in phaseList[currentPhase].GetEnemySequenceList())
        {
            s.ResetMoveCounter();
        }
        currentPhase = newPhaseNumber;
        //Si la Phase implique un changement de status, le status est appliqué
        if(phaseList[currentPhase].GetPhaseStatusName() != "")
        {
            //AddStatus(StatusSet.GetStatus(phaseList[currentPhase].GetPhaseStatusName()), phaseList[currentPhase].GetPhaseStatusPower(),  phaseList[currentPhase].GetPhaseStatusDuration());
            Status newStatus = new Status(phaseList[currentPhase].GetPhaseStatusName(), phaseList[currentPhase].GetPhaseStatusEffects(), phaseList[currentPhase].GetPhaseStatusDuration());
            AddStatus(newStatus);

        }
        //Reset des séquences de la nouvelle phase
        foreach (EnemyPhase p in phaseList)
        {
            p.ResetSequenceCounter();
        }
    //Update du UI
    UIManager.main.DisplayEnemySequence();
    }

    public void ResetPhaseCounter()
    {
        currentPhase = 0;
    }

    //Méthode qui fait le setup correct du next Move de l'ennemi, en suivant ses Sequences et ses Phases
    public void NextMove()
    {
        Debug.Log("NEXT ENEMY MOVE");
        //Fait passer au prochain Move de la current Sequence
        phaseList[currentPhase].GetCurrentSequence().IncreaseMoveCounter();

        //Si on est arrivé au bout de la Sequence
        if (phaseList[currentPhase].GetCurrentSequence().GetCurrentMoveNumber() >= phaseList[currentPhase].GetCurrentSequence().GetEnemyMoveList().Count)
        {
            //On vérifie si la fin de la Sequence implique un changement de Phase
            if (phaseList[currentPhase].GetCurrentSequence().GetEndPhase())
            {
                //Si oui, on change de Phase (la prochaine Phase est déterminée par le NextPhaseNumber)
                SetPhase(phaseList[currentPhase].GetCurrentSequence().GetNextPhaseNumber());
                
            }
            else
            {
                //Sinon on passe à la prochaine Sequence de la Phase
                phaseList[currentPhase].IncreaseSequenceCounter();

                //Si c'est la dernière Sequence de la Phase
                if (phaseList[currentPhase].GetCurrentSequenceNumber() >= phaseList[currentPhase].GetEnemySequenceList().Count)
                {
                    //On reset le compteur de Sequence pour repartir à zéro
                    phaseList[currentPhase].ResetSequenceCounter();

                    //On reset le compteur de Move pour chaque Sequence de la current Phase.
                    foreach (EnemySequence s in phaseList[currentPhase].GetEnemySequenceList())
                    {
                        s.ResetMoveCounter();
                    }
                }   
            }
            Debug.Log("Current Sequence:" + phaseList[currentPhase].GetCurrentSequenceNumber());
        }
        Debug.Log("Current Move:" + phaseList[currentPhase].GetCurrentSequence().GetCurrentMoveNumber());

        //Update du UI
        UIManager.main.DisplayEnemySequence();
    }

    //Compteur de Damage pour les changements de Phase basés sur Damage
    public void AddToTurnDamageCounter(int damage)
    {
        turnDamageCounter += damage;
    }

    public int GetTurnDamageCounter()
    {
        return turnDamageCounter;
    }

    //Vérification si le Damage cap de l'enemy a été atteint
    public void CheckDamageCap()
    {
        if(phaseList[currentPhase].GetTurnDamageCap()> 0 && turnDamageCounter >= phaseList[currentPhase].GetTurnDamageCap())
        {
            Debug.Log("DAMAGE CAP BUSTED!!!!");
            SetPhase(1); // 1 FOR TEST PURPOSES ONLY, ce paramètre va varier pour chaque enemy
        }
        turnDamageCounter = 0;
    }

    private void GenerateEnemyActions()
    {
        switch (enemyTag)
        {
            case EnemyTags.Tutorial:
                phaseList = new List<EnemyPhase>() {
                    new EnemyPhase(enemyPhaseTags.NORMAL,turnDamageCap: 50),
                    new EnemyPhase(enemyPhaseTags.PANIC),
                    new EnemyPhase(enemyPhaseTags.DISRUPTED,
                        phaseStatusName:"DEFENSE DOWN",
                        phaseStatusEffects: new List<Effect> {
                            new Effect(Effect.EffectId.DEFENSEDOWN, 1)})
                };

                //PHASE NORMAL

                //SEQUENCE 0, MOVE 0
                phaseList[0].GetEnemySequenceList().Add(new EnemySequence(enemySequenceTags.ATTACK, false, 0));
                phaseList[0].GetEnemySequenceList()[0].GetEnemyMoveList().Add(new EnemyMove(new List<EnemyAction>() {
                    new EnemyAction("PINCER STRIKE", EnemyAction.EnemyActionType.Attack),
                    new EnemyAction("PINCER STRIKE", EnemyAction.EnemyActionType.Attack)
                    }));

                //SEQUENCE 0, MOVE 1
                phaseList[0].GetEnemySequenceList()[0].GetEnemyMoveList().Add(new EnemyMove(new List<EnemyAction>(){
                    new EnemyAction("PINCER STRIKE", EnemyAction.EnemyActionType.Attack),
                    new EnemyAction("SHREDDING ATTACK", EnemyAction.EnemyActionType.Attack, statusList:new List<Status>(){
                        new Status("BLEEDING",new List<Effect>(){
                            new Effect(Effect.EffectId.TAKEDAMAGE, GetStrength())
                            },3)
                        })
                    }));

                //SEQUENCE 0, MOVE 2
                phaseList[0].GetEnemySequenceList()[0].GetEnemyMoveList().Add(new EnemyMove(new List<EnemyAction>() {
                    new EnemyAction("ACID SPIT", EnemyAction.EnemyActionType.SuperAttack, statusList:new List<Status>(){
                        new Status("CORRODED",new List<Effect>(){
                            new Effect(Effect.EffectId.TAKEDAMAGE, GetStrength()),
                            new Effect(Effect.EffectId.DEFENSEDOWN, 20)
                            },3)
                        })
                    }));

                phaseList[0].GetEnemySequenceList().Add(new EnemySequence(enemySequenceTags.DEFEND, false, 0));
                phaseList[0].GetEnemySequenceList()[1].GetEnemyMoveList().Add(new EnemyMove(new List<EnemyAction>() {
                    new EnemyAction("SQUEEZE", EnemyAction.EnemyActionType.EndFocus, statusList:new List<Status>(){
                        new Status("DEFENSE UP",new List<Effect>(){
                            new Effect(Effect.EffectId.DEFENSEUP, 25)
                            },3)
                        })
                    }));

                phaseList[0].GetEnemySequenceList().Add(new EnemySequence(enemySequenceTags.HEAL, false, 0));
                phaseList[0].GetEnemySequenceList()[2].GetEnemyMoveList().Add(new EnemyMove(new List<EnemyAction>() {
                    new EnemyAction("CONSISTENCY", EnemyAction.EnemyActionType.EndFocus),
                    new EnemyAction("REGROWTH", EnemyAction.EnemyActionType.Heal),
                    }));

                //PHASE PANIC
                phaseList[1].GetEnemySequenceList().Add(new EnemySequence(enemySequenceTags.ATTACK, true, 2));
                phaseList[1].GetEnemySequenceList()[0].GetEnemyMoveList().Add(new EnemyMove(new List<EnemyAction>() {
                    new EnemyAction("PINCER STRIKE", EnemyAction.EnemyActionType.Attack),
                    new EnemyAction("PINCER STRIKE", EnemyAction.EnemyActionType.Attack),
                    new EnemyAction("ACID SPIT", EnemyAction.EnemyActionType.SuperAttack, statusList:new List<Status>(){
                        new Status("CORRODED",new List<Effect>(){
                            new Effect(Effect.EffectId.TAKEDAMAGE, GetStrength()),
                            new Effect(Effect.EffectId.DEFENSEDOWN, 25)
                            },3)
                        })
                    }));

                //PHASE DISRUPTED
                phaseList[2].GetEnemySequenceList().Add(new EnemySequence(enemySequenceTags.WAIT, true, 0));
                phaseList[2].GetEnemySequenceList()[0].GetEnemyMoveList().Add(new EnemyMove(new List<EnemyAction>() {
                    new EnemyAction("EXHAUSTED", EnemyAction.EnemyActionType.Wait)  //Status inclus dans la phase
                    }));

                break;

            case EnemyTags.Flyme:
                phaseList = new List<EnemyPhase>() {
                    new EnemyPhase(enemyPhaseTags.NORMAL,turnDamageCap: 50),
                    new EnemyPhase(enemyPhaseTags.PANIC),
                    new EnemyPhase(enemyPhaseTags.DISRUPTED,
                        phaseStatusName:"DEFENSE DOWN",
                        phaseStatusEffects: new List<Effect> {
                            new Effect(Effect.EffectId.DEFENSEDOWN, 1)})
                };

                //PHASE NORMAL

                //SEQUENCE 0, MOVE 0
                phaseList[0].GetEnemySequenceList().Add(new EnemySequence(enemySequenceTags.ATTACK, false, 0));
                phaseList[0].GetEnemySequenceList()[0].GetEnemyMoveList().Add(new EnemyMove(new List<EnemyAction>() {
                    new EnemyAction("PINCER STRIKE", EnemyAction.EnemyActionType.Attack),
                    new EnemyAction("PINCER STRIKE", EnemyAction.EnemyActionType.Attack)
                    }));

                //SEQUENCE 0, MOVE 1
                phaseList[0].GetEnemySequenceList()[0].GetEnemyMoveList().Add(new EnemyMove(new List<EnemyAction>() {
                    new EnemyAction("PINCER STRIKE", EnemyAction.EnemyActionType.Attack),
                    new EnemyAction("SHREDDING ATTACK", EnemyAction.EnemyActionType.Attack, effectsList:new List<Effect>(){
                        new Effect(Effect.EffectId.TAKEDAMAGE, GetStrength())})
                    }));

                //SEQUENCE 0, MOVE 2
                phaseList[0].GetEnemySequenceList()[0].GetEnemyMoveList().Add(new EnemyMove(new List<EnemyAction>() {
                    new EnemyAction("ACID SPIT", EnemyAction.EnemyActionType.SuperAttack, effectsList:new List<Effect>(){
                        new Effect(Effect.EffectId.TAKEDAMAGE, GetStrength()),
                        new Effect(Effect.EffectId.DEFENSEDOWN, 20)})
                    }));

                phaseList[0].GetEnemySequenceList().Add(new EnemySequence(enemySequenceTags.DEFEND, false, 0));
                phaseList[0].GetEnemySequenceList()[1].GetEnemyMoveList().Add(new EnemyMove(new List<EnemyAction>() {
                    new EnemyAction("SQUEEZE", EnemyAction.EnemyActionType.EndFocus, effectsList:new List<Effect>(){
                        new Effect(Effect.EffectId.DEFENSEUP, 25)}),
                    }));

                phaseList[0].GetEnemySequenceList().Add(new EnemySequence(enemySequenceTags.HEAL, false, 0));
                phaseList[0].GetEnemySequenceList()[2].GetEnemyMoveList().Add(new EnemyMove(new List<EnemyAction>() {
                    new EnemyAction("CONSISTENCY", EnemyAction.EnemyActionType.EndFocus),
                    new EnemyAction("REGROWTH", EnemyAction.EnemyActionType.Heal),
                    }));

                //PHASE PANIC
                phaseList[1].GetEnemySequenceList().Add(new EnemySequence(enemySequenceTags.ATTACK, true, 2));
                phaseList[1].GetEnemySequenceList()[0].GetEnemyMoveList().Add(new EnemyMove(new List<EnemyAction>() {
                    new EnemyAction("PINCER STRIKE", EnemyAction.EnemyActionType.Attack),
                    new EnemyAction("PINCER STRIKE", EnemyAction.EnemyActionType.Attack),
                    new EnemyAction("ACID SPIT", EnemyAction.EnemyActionType.SuperAttack, effectsList:new List<Effect>(){
                        new Effect(Effect.EffectId.TAKEDAMAGE, GetStrength()),
                        new Effect(Effect.EffectId.DEFENSEDOWN, 20)})
                    }));

                //PHASE DISRUPTED
                phaseList[2].GetEnemySequenceList().Add(new EnemySequence(enemySequenceTags.WAIT, true, 0));
                phaseList[2].GetEnemySequenceList()[0].GetEnemyMoveList().Add(new EnemyMove(new List<EnemyAction>() {
                    new EnemyAction("EXHAUSTED", EnemyAction.EnemyActionType.Wait),
                    }));
                break;

            case EnemyTags.Fireball:
                phaseList = new List<EnemyPhase>() {
                    new EnemyPhase(enemyPhaseTags.NORMAL,turnDamageCap: 50),
                    new EnemyPhase(enemyPhaseTags.PANIC),
                    new EnemyPhase(enemyPhaseTags.DISRUPTED,
                        phaseStatusName:"DEFENSE DOWN",
                        phaseStatusEffects: new List<Effect> {
                            new Effect(Effect.EffectId.DEFENSEDOWN, 1)})
                };

                //PHASE NORMAL

                //SEQUENCE 0, MOVE 0
                phaseList[0].GetEnemySequenceList().Add(new EnemySequence(enemySequenceTags.ATTACK, false, 0));
                phaseList[0].GetEnemySequenceList()[0].GetEnemyMoveList().Add(new EnemyMove(new List<EnemyAction>() {
                    new EnemyAction("PINCER STRIKE", EnemyAction.EnemyActionType.Attack),
                    new EnemyAction("PINCER STRIKE", EnemyAction.EnemyActionType.Attack)
                    }));

                //SEQUENCE 0, MOVE 1
                phaseList[0].GetEnemySequenceList()[0].GetEnemyMoveList().Add(new EnemyMove(new List<EnemyAction>() {
                    new EnemyAction("PINCER STRIKE", EnemyAction.EnemyActionType.Attack),
                    new EnemyAction("SHREDDING ATTACK", EnemyAction.EnemyActionType.Attack, effectsList:new List<Effect>(){
                        new Effect(Effect.EffectId.TAKEDAMAGE, GetStrength())})
                    }));

                //SEQUENCE 0, MOVE 2
                phaseList[0].GetEnemySequenceList()[0].GetEnemyMoveList().Add(new EnemyMove(new List<EnemyAction>() {
                    new EnemyAction("ACID SPIT", EnemyAction.EnemyActionType.SuperAttack, effectsList:new List<Effect>(){
                        new Effect(Effect.EffectId.TAKEDAMAGE, GetStrength()),
                        new Effect(Effect.EffectId.DEFENSEDOWN, 20)})
                    }));

                phaseList[0].GetEnemySequenceList().Add(new EnemySequence(enemySequenceTags.DEFEND, false, 0));
                phaseList[0].GetEnemySequenceList()[1].GetEnemyMoveList().Add(new EnemyMove(new List<EnemyAction>() {
                    new EnemyAction("SQUEEZE", EnemyAction.EnemyActionType.EndFocus, effectsList:new List<Effect>(){
                        new Effect(Effect.EffectId.DEFENSEUP, 25)}),
                    }));

                phaseList[0].GetEnemySequenceList().Add(new EnemySequence(enemySequenceTags.HEAL, false, 0));
                phaseList[0].GetEnemySequenceList()[2].GetEnemyMoveList().Add(new EnemyMove(new List<EnemyAction>() {
                    new EnemyAction("CONSISTENCY", EnemyAction.EnemyActionType.EndFocus),
                    new EnemyAction("REGROWTH", EnemyAction.EnemyActionType.Heal),
                    }));

                //PHASE PANIC
                phaseList[1].GetEnemySequenceList().Add(new EnemySequence(enemySequenceTags.ATTACK, true, 2));
                phaseList[1].GetEnemySequenceList()[0].GetEnemyMoveList().Add(new EnemyMove(new List<EnemyAction>() {
                    new EnemyAction("ATTACK", EnemyAction.EnemyActionType.Attack),
                    new EnemyAction("ATTACK", EnemyAction.EnemyActionType.Attack),
                    new EnemyAction("ACID SPIT", EnemyAction.EnemyActionType.SuperAttack, effectsList:new List<Effect>(){
                        new Effect(Effect.EffectId.TAKEDAMAGE, GetStrength()),
                        new Effect(Effect.EffectId.DEFENSEDOWN, 20)})
                    }));

                //PHASE DISRUPTED
                phaseList[2].GetEnemySequenceList().Add(new EnemySequence(enemySequenceTags.WAIT, true, 0));
                phaseList[2].GetEnemySequenceList()[0].GetEnemyMoveList().Add(new EnemyMove(new List<EnemyAction>() {
                    new EnemyAction("EXHAUSTED", EnemyAction.EnemyActionType.Wait),
                    }));
                break;

        }
    }

}
