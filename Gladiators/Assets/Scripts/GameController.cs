﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



/*
 * Classe qui gère le flow du jeu
 */



//
// Note : Playing creature et waiting creature a regarder
//

public class GameController : MonoBehaviour
{

    public enum GameState { PlayerTurn, EnemyTurn, SwitchingTurn, QTE, PlayerAnimation, EnemyAnimation, PlayerCounterAttack, BetweenCombat };

    public static GameController main;
    private EnemyAnimation enemyAnimation;
    public PlayerAnimation playerAnimation;
    //private Creature player, enemy, playingCreature, waitingCreature;
    private Creature playingCreature, waitingCreature;
    private Player player;
    private Enemy enemy;
    private AiPlayer aiPlayer;

    public GameObject playerSprite;
    private GameObject enemySprite;
    public GameObject[] background;

    public Image switchTurnBG;

    private GameState currentGameState;
    private bool isSwitchingTurn;
    private int switchTurnStep;
    private int damage;
    //private int moveNumber;

    private int playerPointsPerTurn = 5;
    private int enemyPointsPerTurn = 5;

    private System.Action currentPendingAction;
    private string currentAnimationName;
    private int currentQteResult;
    private int focusType = 0;

    private string combatText;
    private Vector3 combatTextPosition;
    private Color combatTextColor;
    private int combatTextFontSize;
    private bool failedTimeTurn;
    public float turnTimeBonus;
    private bool combatActive = false;
    private string comboText;

    void Awake()
    {
        main = this;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.T))
        {
            Debug.Log("Printing Status List for " + EntityManager.main.GetActiveEnemy().GetName() + " " + EntityManager.main.GetActiveEnemy().GetCurrentHp());

            foreach (Status s in EntityManager.main.GetActiveEnemy().currentStatusList)
            {
                Debug.Log(s.GetName() + " " +  s.GetRemainingDuration());
            }
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            Status newStatus = new Status("BLINDED", new List<Effect> { new Effect(Effect.EffectId.SKILLDOWN, 15) }, 2);
            EntityManager.main.GetActiveEnemy().AddStatus(newStatus);
        }


    }
    public void LoadFirstCombat()
    {
        player = EntityManager.main.GetPlayer();
        enemy = EntityManager.main.GetActiveEnemy();
        aiPlayer = new AiPlayer();
        aiPlayer.Initialize();
        enemySprite = EntityManager.main.CreateEnemyObject(new Vector3(0,0,0));
        enemyAnimation = enemySprite.GetComponent<EnemyAnimation>();

        //DISPLAY DES INFOS SUR ENNEMI
        UIManager.main.SetupEnemyInfo(new Vector3(enemySprite.transform.position.x, enemySprite.transform.position.y + enemySprite.GetComponent<SpriteRenderer>().bounds.size.y / 3, enemySprite.transform.position.z));

        enemy.ClearStatusList();
        player.ClearStatusList();

        UIManager.main.DisplayEnemySequence();

        //détermine qui commence le combat. 50% de chances + (différence entre playerSPD et enemySPD)/2
        float randFirstStrike = Random.Range(1, 101);
        if (randFirstStrike <= 50 + Mathf.Clamp(player.GetSpeed() - enemy.GetSpeed(), -50, 50))
        {
            //Debug.Log("Player Starts with a " + randFirstStrike + " roll on a " + (50 + Mathf.Clamp(player.GetSpeed() - enemy.GetSpeed(), -50, 50)).ToString() + "% chance");
            SetGameState(GameState.PlayerTurn);
            UIManager.main.creatureTurnText.ChangeText("ENEMY'S\nTURN");
            playingCreature = player;
            waitingCreature = enemy;
            Invoke("TimeBonusPointsFail", turnTimeBonus);
            combatActive = true;
            UIManager.main.StartTurnUIUpdate();
        }
        else
        {
           // Debug.Log("Enemy Starts with a " + randFirstStrike + " roll on a " + (50 + Mathf.Clamp(enemy.GetSpeed() - player.GetSpeed(), -50, 50)).ToString() + "% chance"); currentGameState = GameState.EnemyTurn;
            UIManager.main.creatureTurnText.ChangeText("PLAYER'S\nTURN");
            SetGameState(GameState.EnemyTurn);
            playingCreature = enemy;
            waitingCreature = player;
            combatActive = true;
            //aiPlayer.SetMoveNumber();
            aiPlayer.PlayAiAction();
        }
    }
    public void LoadNextCombat()
    {
        //Cache ancien background
        background[EntityManager.main.GetCurrentEnemyNumber()].SetActive(false);

        //Changement de l'enemy
        EntityManager.main.SetNewEnemy();
        Destroy(enemySprite);
        enemySprite = EntityManager.main.CreateEnemyObject(new Vector3(0, 0, 0));
        enemyAnimation = enemySprite.GetComponent<EnemyAnimation>();
        enemy = EntityManager.main.GetActiveEnemy();
        UIManager.main.SetupPlayerAndEnemy();
        aiPlayer.Initialize();
        enemy.ClearStatusList();

        //DISPLAY DES INFOS SUR ENNEMI
        UIManager.main.SetupEnemyInfo(new Vector3(enemySprite.transform.position.x, enemySprite.transform.position.y + enemySprite.GetComponent<SpriteRenderer>().bounds.size.y / 3, enemySprite.transform.position.z));

        //Reset du player
        player.ClearCombatExp();
        player.ResetAp();
        player.DeactivateFocus();
        player.ClearCurrentComboList();
        player.ClearStatusList();

        //Load nouveau background
        background[EntityManager.main.GetCurrentEnemyNumber()].SetActive(true);

        //Setup du UI
        UIManager.main.ClearActionText();
        UIManager.main.UpdateText();
        UIManager.main.creatureTurnText.ChangeText("ENEMY'S\nTURN");

        //Setup du Game Controller
        SetGameState(GameState.PlayerTurn);
        playingCreature = player;
        waitingCreature = enemy;
        combatActive = true;      

        //Activation des commandes du UI
        UIManager.main.StartTurnUIUpdate();
        Invoke("TimeBonusPointsFail", turnTimeBonus);
    }
    public void NormalAttack()
    {
        if (currentGameState == GameState.PlayerTurn)
        {
            //récupère infos pour affichage du damage
            combatTextPosition = enemySprite.transform.position;
            combatTextColor = new Color(1, 0, 0, 1);
            combatTextFontSize = 60;
            damage = playingCreature.GetDamage();
            damage = Mathf.Max(1, damage - enemy.GetDefense());

            comboText = "ATTACK";

            if (currentQteResult == 3)
            {
                //si les chaining attacks sont déjà enclenchées, un perfect augmente le multiplicateur pour la prochaine attaque
                if (player.GetChainedAttacksMultiplier() > 1)
                {
                    player.SetChainedAttacks(2);
                }
                player.AddToCurrentComboList(comboText, false);
                player.CheckForCombo();       
            }

            if (currentQteResult == 2)
            {
                if (player.GetChainedAttacksMultiplier() > 1)
                {
                    player.SetChainedAttacks(2);
                }
                player.AddToCurrentComboList(comboText, true);
                player.CheckForCombo();
                damage = damage / 2;
            }

            combatText = damage.ToString();

            if (currentQteResult < 2)
            {
                player.ClearCurrentComboList();  //combo breaker
                damage = 0;
                UIManager.main.actionText[UIManager.main.GetActionsCount() - 1].ChangeColor(new Color(1f, 0f, 0f, 1f), 0.4f);
                combatText = "MISS";
                combatTextFontSize = 48;
            }
            //crée l'objet qui affiche le damage
            CombatTextManager.Main.CreateText(combatTextPosition, combatText, combatTextColor, combatTextFontSize);

            //envoie la Audiobank de type ENEMY_HIT au SoundManager avec un délai pour jouer le son
            SoundManagerN.instance.PlaySfx(0.4f, SoundManagerN.AudioBank.ENEMY_HIT);
            player.DamageTarget(enemy, damage);
            enemy.AddToTurnDamageCounter(damage);
            if (EndCombatCheck()) {
                if (player.GetCurrentAp() > 0)
                {
                    UIManager.main.TogglePlayerActionButtons();
                }
                UIManager.main.TogglePlayerEndTurn();
            }
        }

        else if (currentGameState == GameState.EnemyTurn)
        {
            combatTextPosition = playerSprite.transform.position;
            combatTextColor = new Color(1, 0, 0, 1);
            combatTextFontSize = 60;
            damage = playingCreature.GetDamage();
            damage = Mathf.Max(1, damage - player.GetDefense());//test de soustraire avant de multiplier

            if (currentQteResult == 2)
            {
                damage = damage / 2;
            }

            combatText = damage.ToString();

            //CODE POUR BLOCK
            int blockCounter = UIManager.main.GetBlockCounter();
            if (player.GetSpecialActive())
            {
                blockCounter += player.GetSpecialEndCharge() / 20;
                blockCounter = Mathf.Min(5, blockCounter);
                //Debug.Log("BLOCK COUNT:" + blockCounter);
            }
            else
            {
                if(blockCounter >= 1)
                {
                    comboText = "BLOCK";
                    for(int i = 0; i < blockCounter; i++)
                    {
                        player.AddToCurrentComboList(comboText, false);
                    }
                    player.CheckForCombo();
                }
            }
            switch (blockCounter)
            {
                case 0:
                    //damage = damage; // 0%
                    combatText = damage.ToString();
                    break;
                case 1:
                    damage = damage * 4 / 5 ; // 20%
                    combatText = damage.ToString();
                    break;
                case 2:
                    damage = damage * 3 / 5; // 40%
                    combatText = damage.ToString();
                    break;
                case 3:
                    damage = damage * 2 / 5; // 60%
                    combatText = damage.ToString();
                    break;
                case 4:
                    damage = damage / 5; // 80%
                    combatText = damage.ToString();
                    break;
                case 5:
                    damage = 0; // 100%
                    combatText = "BLOCKED!";
                    combatTextColor = new Color(1, 1, 1, 1);
                    break;
            }
           
            if (currentQteResult < 2)
            {
                damage = 0;
                combatText = "MISS";
                combatTextFontSize = 48;
                combatTextColor = new Color(1, 1, 1, 1);
                CombatTextManager.Main.CreateText(combatTextPosition, combatText, combatTextColor, combatTextFontSize);

                //CODE POUR COUNTER
                if (player.GetCurrentAp() >= 1)
                {
                    //annule le invoke de la prochaine action du AI pour donner le temps au counter de se faire
                    //la prochaine action du AI sera invoked plus tard
                    //Debug.Log("Counter attack will make next action call");
                    CancelInvoke("NextAiAction");
                    enemyAnimation.CancelNextActionCall();

                    combatTextPosition = new Vector3(0,0,0);
                    combatTextColor = new Color(0, 1, 1, 1);
                    combatTextFontSize = 120;
                    combatText = "COUNTER!";
                    CombatTextManager.Main.CreateInstructionText(combatTextPosition, combatText, combatTextColor, combatTextFontSize);
                    UIManager.main.SetPlayerCounterInput(true);
                    Invoke(nameof(EndCounterInput), 1f * Time.timeScale);// à moduler selon speed
                }
            }
            else
            {
                playingCreature.DamageTarget(waitingCreature, damage);

                EnemyAction currentEnemyAction = enemy.GetCurrentPhase().GetCurrentSequence().GetCurrentMove().GetActionList()[aiPlayer.GetCurrentEnemyAction()];
                if (currentEnemyAction.GetEnemyActionStatusList().Count > 0)
                {
                    foreach(Status status in currentEnemyAction.GetEnemyActionStatusList())
                    {
                        player.AddStatus(status);
                    }
                }

                CombatTextManager.Main.CreateText(combatTextPosition, combatText, combatTextColor, combatTextFontSize);

                //envoie la Audiobank de type PLAYER_HIT au SoundManager avec un délai pour jouer le son
                SoundManagerN.instance.PlaySfx(0.4f, SoundManagerN.AudioBank.PLAYER_HIT);
                //if (EndCombatCheck())
                //    Invoke("NextAiAction", 1f);
            }
        }
        UIManager.main.UpdateText();
        UIManager.main.ResetBlockCounter();
    }

    public void LightAttack()
    {
        if (currentGameState == GameState.PlayerTurn)
        {
            //récupère infos pour affichage du damage
            combatTextPosition = enemySprite.transform.position;
            combatTextColor = new Color(1, 0, 0, 1);
            combatTextFontSize = 60;
            damage = playingCreature.GetDamage();
            damage = Mathf.Max(1, damage - enemy.GetDefense()); //test de soustraire avant de multiplier

            comboText = "LIGHTATTACK";

            if (currentQteResult == 2)
            {
                //si les chaining attacks sont déjà enclenchées, un perfect augmente le multiplicateur pour la prochaine attaque
                if (player.GetChainedAttacksMultiplier() > 1)
                {
                    player.SetChainedAttacks(2);
                }
                //SI LES SKILLS D'ATTACK SONT ACTIVÉS
                if (player.GetLightAttackSkills())
                {
                    Vector3 skillTextPosition = playerSprite.transform.position;
                    skillTextPosition.x = skillTextPosition.x - 1;
                    Color skillTextColor = new Color(50, 0, 255, 1);
                    int skillTextFontSize = 24;
                    string skillText = "BLINDING ATTACK";
                    CombatTextManager.Main.CreateComboText(skillTextPosition, skillText, skillTextColor, skillTextFontSize);
                    //enemy.AddStatus(StatusSet.GetStatus("BLINDED"), player.GetBlindingEffectPower() ,player.GetBlindingEffectDuration());
                    Status newStatus = new Status("BLINDED", new List<Effect> { new Effect(Effect.EffectId.SKILLDOWN, player.GetBlindingEffectPower()) }, player.GetBlindingEffectDuration());
                    enemy.AddStatus(newStatus);
                }
                player.AddToCurrentComboList(comboText, false);
                player.CheckForCombo();
            }

            if (currentQteResult == 1)
            {
                //si les chaining attacks sont déjà enclenchées, un perfect augmente le multiplicateur pour la prochaine attaque
                if (player.GetChainedAttacksMultiplier() > 1)
                {
                    player.SetChainedAttacks(2);
                }
                if (player.GetLightAttackSkills())
                {
                    player.SetLightAttackSkills(false);
                }
                player.AddToCurrentComboList(comboText, true);
                player.CheckForCombo();
                damage = damage / 2;
            }

            //damage = Mathf.Max(0, damage - enemy.GetDefense());
            combatText = damage.ToString();

            if (currentQteResult < 1)
            {
                player.ClearCurrentComboList();  //combo breaker
                damage = 0;
                UIManager.main.actionText[UIManager.main.GetActionsCount() - 1].ChangeColor(new Color(1f, 0f, 0f, 1f), 0.4f);
                combatText = "MISS";
                combatTextFontSize = 48;
            }
            //crée l'objet qui affiche le damage
            CombatTextManager.Main.CreateText(combatTextPosition, combatText, combatTextColor, combatTextFontSize);

            //envoie la Audiobank de type ENEMY_HIT au SoundManager avec un délai pour jouer le son
            SoundManagerN.instance.PlaySfx(0.4f, SoundManagerN.AudioBank.ENEMY_HIT);
            player.DamageTarget(enemy, damage);
            enemy.AddToTurnDamageCounter(damage);
            if (EndCombatCheck())
            {
                if (player.GetCurrentAp() > 0)
                {
                    UIManager.main.TogglePlayerActionButtons();
                }
                UIManager.main.TogglePlayerEndTurn();
            }
        }

        else if (currentGameState == GameState.EnemyTurn)
        {
            //envoie la Audiobank de type ENEMY_ATTACK au SoundManager
            //SoundManagerN.instance.PlaySfx(0.0f, SoundManagerN.AudioBank.ENEMY_ATTACK);

            //enemy.RemoveAp(1);

            combatTextPosition = playerSprite.transform.position;
            combatTextColor = new Color(1, 0, 0, 1);
            combatTextFontSize = 60;
            damage = playingCreature.GetDamage();
            damage = Mathf.Max(1, damage - player.GetDefense());//test de soustraire avant de multiplier

            if (currentQteResult == 2)
            {
                damage = damage / 2;
            }

            combatText = damage.ToString();

            //CODE POUR BLOCK
            int blockCounter = UIManager.main.GetBlockCounter();
            if (player.GetSpecialActive())
            {
                blockCounter += player.GetSpecialEndCharge() / 20;
                blockCounter = Mathf.Min(5, blockCounter);
                //Debug.Log("BLOCK COUNT:" + blockCounter);
            }
            else
            {
                if (blockCounter >= 1)
                {
                    comboText = "BLOCK";
                    for (int i = 0; i < blockCounter; i++)
                    {
                        player.AddToCurrentComboList(comboText, false);
                    }
                    player.CheckForCombo();
                }
            }
            switch (blockCounter)
            {
                case 0:
                    //damage = damage; // 0%
                    combatText = damage.ToString();
                    break;
                case 1:
                    damage = damage * 4 / 5; // 20%
                    combatText = damage.ToString();
                    break;
                case 2:
                    damage = damage * 3 / 5; // 40%
                    combatText = damage.ToString();
                    break;
                case 3:
                    damage = damage * 2 / 5; // 60%
                    combatText = damage.ToString();
                    break;
                case 4:
                    damage = damage / 5; // 80%
                    combatText = damage.ToString();
                    break;
                case 5:
                    damage = 0; // 100%
                    combatText = "BLOCKED!";
                    combatTextColor = new Color(1, 1, 1, 1);
                    break;
            }

            if (currentQteResult < 2)
            {
                damage = 0;
                combatText = "MISS";
                combatTextFontSize = 48;
                combatTextColor = new Color(1, 1, 1, 1);
                CombatTextManager.Main.CreateText(combatTextPosition, combatText, combatTextColor, combatTextFontSize);

                //CODE POUR COUNTER
                if (player.GetCurrentAp() >= 1)
                {
                    //annule le invoke de la prochaine action du AI pour donner le temps au counter de se faire
                    //la prochaine action du AI sera invoked plus tard
                   //Debug.Log("Counter attack will make next action call");
                    CancelInvoke("NextAiAction");
                    //currentGameState = GameState.PlayerCounterAttack;
                    enemyAnimation.CancelNextActionCall();

                    combatTextPosition = new Vector3(0, 0, 0);
                    combatTextColor = new Color(0, 1, 1, 1);
                    combatTextFontSize = 120;
                    combatText = "COUNTER!";
                    CombatTextManager.Main.CreateInstructionText(combatTextPosition, combatText, combatTextColor, combatTextFontSize);
                    UIManager.main.SetPlayerCounterInput(true);
                    Invoke(nameof(EndCounterInput), 1f * Time.timeScale);// à moduler selon speed
                }
                //else
                // {
                //     EndCounterInput();
                // }

            }
            else
            {
                playingCreature.DamageTarget(waitingCreature, damage);

                CombatTextManager.Main.CreateText(combatTextPosition, combatText, combatTextColor, combatTextFontSize);

                //envoie la Audiobank de type PLAYER_HIT au SoundManager avec un délai pour jouer le son
                SoundManagerN.instance.PlaySfx(0.4f, SoundManagerN.AudioBank.PLAYER_HIT);
                //if (EndCombatCheck())
                //    Invoke("NextAiAction", 1f);
            }
        }
        UIManager.main.UpdateText();
        UIManager.main.ResetBlockCounter();
    }

    public void HeavyAttack()
    {
        if (currentGameState == GameState.PlayerTurn)
        {
            //récupère infos pour affichage du damage
            combatTextPosition = enemySprite.transform.position;
            combatTextColor = new Color(1, 0, 0, 1);
            combatTextFontSize = 60;
            damage = playingCreature.GetDamage();
            damage = Mathf.Max(1, damage - enemy.GetDefense()); //test de soustraire avant de multiplier
            damage = damage * 3 / 2;  //heavy attack 1.5x damage
            comboText = "HEAVYATTACK";

            if (currentQteResult == 4)
            {
                //si les chaining attacks sont déjà enclenchées, un perfect augmente le multiplicateur pour la prochaine attaque
                if (player.GetChainedAttacksMultiplier() > 1)
                {
                    player.SetChainedAttacks(2);
                }
                //SI LES SKILLS D'ATTACK SONT ACTIVÉS
                if (player.GetHeavyAttackSkills())
                {
                    Vector3 skillTextPosition = playerSprite.transform.position;
                    skillTextPosition.x = skillTextPosition.x - 1;
                    Color skillTextColor = new Color(50, 0, 255, 1);
                    int skillTextFontSize = 24;
                    string skillText = "BLEEDING ATTACK";
                    CombatTextManager.Main.CreateComboText(skillTextPosition, skillText, skillTextColor, skillTextFontSize);
                    //enemy.AddStatus(StatusSet.GetStatus("BLEEDING"), player.GetBleedingEffectPower(), player.GetBleedingEffectDuration());
                    Status newStatus = new Status("BLEEDING", new List<Effect> { new Effect(Effect.EffectId.TAKEDAMAGE, player.GetBleedingEffectPower()) }, player.GetBleedingEffectDuration());
                    enemy.AddStatus(newStatus);

                }
                player.AddToCurrentComboList(comboText, false);
                player.CheckForCombo();
            }

            if (currentQteResult == 3)
            {
                //si les chaining attacks sont déjà enclenchées, un perfect augmente le multiplicateur pour la prochaine attaque
                if (player.GetChainedAttacksMultiplier() > 1)
                {
                    player.SetChainedAttacks(2);
                }
                if (player.GetHeavyAttackSkills())
                {
                    player.SetHeavyAttackSkills(false);
                }
                player.AddToCurrentComboList(comboText, true);
                player.CheckForCombo();
                damage = damage / 4 * 3;
            }
            
            if (currentQteResult == 2)
            {
                //si les chaining attacks sont déjà enclenchées, un perfect augmente le multiplicateur pour la prochaine attaque
                if (player.GetChainedAttacksMultiplier() > 1)
                {
                    player.SetChainedAttacks(2);
                }
                if (player.GetHeavyAttackSkills())
                {
                    player.SetHeavyAttackSkills(false);
                }
                player.AddToCurrentComboList(comboText, true);
                player.CheckForCombo();
                damage = damage / 2;
            }

            

            //damage = Mathf.Max(0, damage - enemy.GetDefense());
            combatText = damage.ToString();

            if (currentQteResult < 2)
            {
                player.ClearCurrentComboList();  //combo breaker
                damage = 0;
                UIManager.main.actionText[UIManager.main.GetActionsCount() - 1].ChangeColor(new Color(1f, 0f, 0f, 1f), 0.4f);
                combatText = "MISS";
                combatTextFontSize = 48;
            }
            //crée l'objet qui affiche le damage
            CombatTextManager.Main.CreateText(combatTextPosition, combatText, combatTextColor, combatTextFontSize);

            //envoie la Audiobank de type ENEMY_HIT au SoundManager avec un délai pour jouer le son
            SoundManagerN.instance.PlaySfx(0.4f, SoundManagerN.AudioBank.ENEMY_HIT);
            player.DamageTarget(enemy, damage);
            enemy.AddToTurnDamageCounter(damage);
            if (EndCombatCheck())
            {
                if (player.GetCurrentAp() > 0)
                {
                    UIManager.main.TogglePlayerActionButtons();
                }
                UIManager.main.TogglePlayerEndTurn();
            }
        }

        else if (currentGameState == GameState.EnemyTurn)
        {
            //envoie la Audiobank de type ENEMY_ATTACK au SoundManager
            //SoundManagerN.instance.PlaySfx(0.0f, SoundManagerN.AudioBank.ENEMY_ATTACK);

            //enemy.RemoveAp(1);

            combatTextPosition = playerSprite.transform.position;
            combatTextColor = new Color(1, 0, 0, 1);
            combatTextFontSize = 60;
            damage = playingCreature.GetDamage();
            damage = Mathf.Max(1, damage - player.GetDefense());//test de soustraire avant de multiplier


            if (currentQteResult == 2)
            {
                damage = damage / 2;
            }
            
            combatText = damage.ToString();

            //CODE POUR BLOCK
            int blockCounter = UIManager.main.GetBlockCounter();
            if (player.GetSpecialActive())
            {
                blockCounter += player.GetSpecialEndCharge() / 20;
                blockCounter = Mathf.Min(5, blockCounter);
                //Debug.Log("BLOCK COUNT:" + blockCounter);
            }
            else
            {
                if (blockCounter >= 1)
                {
                    comboText = "BLOCK";
                    for (int i = 0; i < blockCounter; i++)
                    {
                        player.AddToCurrentComboList(comboText, false);
                    }
                    player.CheckForCombo();
                }
            }
            switch (blockCounter)
            {
                case 0:
                    //damage = damage; // 0%
                    combatText = damage.ToString();
                    break;
                case 1:
                    damage = damage * 4 / 5; // 20%
                    combatText = damage.ToString();
                    break;
                case 2:
                    damage = damage * 3 / 5; // 40%
                    combatText = damage.ToString();
                    break;
                case 3:
                    damage = damage * 2 / 5; // 60%
                    combatText = damage.ToString();
                    break;
                case 4:
                    damage = damage / 5; // 80%
                    combatText = damage.ToString();
                    break;
                case 5:
                    damage = 0; // 100%
                    combatText = "BLOCKED!";
                    combatTextColor = new Color(1, 1, 1, 1);
                    break;
            }

            if (currentQteResult < 2)
            {
                damage = 0;
                combatText = "MISS";
                combatTextFontSize = 48;
                combatTextColor = new Color(1, 1, 1, 1);
                CombatTextManager.Main.CreateText(combatTextPosition, combatText, combatTextColor, combatTextFontSize);

                //CODE POUR COUNTER
                if (player.GetCurrentAp() >= 1)
                {
                    //annule le invoke de la prochaine action du AI pour donner le temps au counter de se faire
                    //la prochaine action du AI sera invoked plus tard
                   // Debug.Log("Counter attack will make next action call");
                    CancelInvoke("NextAiAction");
                    //currentGameState = GameState.PlayerCounterAttack;
                    enemyAnimation.CancelNextActionCall();

                    combatTextPosition = new Vector3(0, 0, 0);
                    combatTextColor = new Color(0, 1, 1, 1);
                    combatTextFontSize = 120;
                    combatText = "COUNTER!";
                    CombatTextManager.Main.CreateInstructionText(combatTextPosition, combatText, combatTextColor, combatTextFontSize);
                    UIManager.main.SetPlayerCounterInput(true);
                    Invoke(nameof(EndCounterInput), 1f * Time.timeScale);// à moduler selon speed
                }
                //else
                // {
                //     EndCounterInput();
                // }

            }
            else
            {
                playingCreature.DamageTarget(waitingCreature, damage);

                CombatTextManager.Main.CreateText(combatTextPosition, combatText, combatTextColor, combatTextFontSize);

                //envoie la Audiobank de type PLAYER_HIT au SoundManager avec un délai pour jouer le son
                SoundManagerN.instance.PlaySfx(0.4f, SoundManagerN.AudioBank.PLAYER_HIT);
                //if (EndCombatCheck())
                //    Invoke("NextAiAction", 1f);
            }
        }
        UIManager.main.UpdateText();
        UIManager.main.ResetBlockCounter();
    }

    public void SuperAttack()
    {
        if (currentGameState == GameState.PlayerTurn)
        {
            //récupère infos pour affichage du damage
            combatTextPosition = enemySprite.transform.position;
            damage = playingCreature.GetDamage();
            combatTextColor = new Color(1, 0, 0, 1);
            combatTextFontSize = 60;
            damage = Mathf.Max(1, damage - enemy.GetDefense());

            if (currentQteResult == 5)
            {
                damage = damage * Mathf.Max(1, player.GetSpecialStrCharge() / 20);
            }
            if (currentQteResult == 4)
            {
                damage = damage * Mathf.Max(1, player.GetSpecialStrCharge() / 40);
            }

            combatText = damage.ToString();

            if (currentQteResult <= 2)
            {
                damage = 0;
                combatText = "MISS";
                combatTextFontSize = 48;
                UIManager.main.actionText[UIManager.main.GetActionsCount() - 1].ChangeColor(new Color(1f, 0f, 0f, 1f), 0.4f);
            }
            //crée l'objet qui affiche le damage
            CombatTextManager.Main.CreateText(combatTextPosition, combatText, combatTextColor, combatTextFontSize);

            //envoie la Audiobank de type ENEMY_HIT au SoundManager avec un délai pour jouer le son
            SoundManagerN.instance.PlaySfx(0.4f, SoundManagerN.AudioBank.ENEMY_HIT);

            player.DamageTarget(enemy, damage);
            enemy.AddToTurnDamageCounter(damage);

            if (EndCombatCheck())
            {
                if (player.GetCurrentAp() > 0)
                {
                    UIManager.main.TogglePlayerActionButtons();
                }
                UIManager.main.TogglePlayerEndTurn();
            }
        }

        else if (currentGameState == GameState.EnemyTurn)
        {
            //envoie la Audiobank de type ENEMY_ATTACK au SoundManager
            //SoundManagerN.instance.PlaySfx(0.0f, SoundManagerN.AudioBank.ENEMY_ATTACK);


            //enemy.RemoveAp(3);
            combatTextPosition = playerSprite.transform.position;
            combatTextColor = new Color(1, 0, 0, 1);
            combatTextFontSize = 60;
            damage = enemy.GetDamage();
            damage = Mathf.Max(1, damage - player.GetDefense());  //test de soustraire avant de multiplier

            if (currentQteResult == 5)
            {
                damage = damage * playingCreature.GetSuperAttackMultiplier();
            }
            if (currentQteResult == 4)
            {
                damage = damage * 2;
            }

            

            combatText = damage.ToString();

            //CODE POUR BLOCK
            int blockCounter = UIManager.main.GetBlockCounter();
            if (player.GetSpecialActive())
            {
                blockCounter += player.GetSpecialEndCharge() / 20;
                blockCounter = Mathf.Min(5, blockCounter);
                //Debug.Log("BLOCK COUNT:" + blockCounter);
            }
            else
            {
                if (blockCounter >= 1)
                {
                    comboText = "BLOCK";
                    for (int i = 0; i < blockCounter; i++)
                    {
                        player.AddToCurrentComboList(comboText, false);
                    }
                    player.CheckForCombo();
                }
            }
            switch (blockCounter)
            {
                case 0:
                    //damage = damage; // 0%
                    combatText = damage.ToString();
                    break;
                case 1:
                    damage = damage * 4 / 5; // 20%
                    combatText = damage.ToString();
                    break;
                case 2:
                    damage = damage * 3 / 5; // 40%
                    combatText = damage.ToString();
                    break;
                case 3:
                    damage = damage * 2 / 5; // 60%
                    combatText = damage.ToString();
                    break;
                case 4:
                    damage = damage / 5; // 80%
                    combatText = damage.ToString();
                    break;
                case 5:
                    damage = 0; // 100%
                    combatText = "BLOCKED!";
                    combatTextColor = new Color(1, 1, 1, 1);
                    break;
            }

            if (currentQteResult <= 2)
            {
                damage = 0;
                combatText = "MISS";
                combatTextFontSize = 48;
                combatTextColor = new Color(1, 1, 1, 1);

                //CODE POUR COUNTER
                if (player.GetCurrentAp() >= 1)
                {
                    //annule le invoke de la prochaine action du AI pour donner le temps au counter de se faire
                    //la prochaine action du AI sera invoked plus tard
                    //Debug.Log("Counter attack will make next action call");
                    CancelInvoke("NextAiAction");
                    //currentGameState = GameState.PlayerCounterAttack;
                    enemyAnimation.CancelNextActionCall();

                    combatTextPosition = new Vector3(0, 0, 0);
                    combatTextColor = new Color(0, 1, 1, 1);
                    combatTextFontSize = 120;
                    combatText = "COUNTER!";
                    CombatTextManager.Main.CreateInstructionText(combatTextPosition, combatText, combatTextColor, combatTextFontSize);
                    UIManager.main.SetPlayerCounterInput(true);
                    Invoke(nameof(EndCounterInput), 1f * Time.timeScale);// à moduler selon speed
                }
            }

            playingCreature.DamageTarget(waitingCreature, damage);

            CombatTextManager.Main.CreateText(combatTextPosition, combatText, combatTextColor, combatTextFontSize);

            //envoie la Audiobank de type PLAYER_HIT au SoundManager avec un délai pour jouer le son
            SoundManagerN.instance.PlaySfx(0.4f, SoundManagerN.AudioBank.PLAYER_HIT);

            //if (EndCombatCheck())
            //    Invoke("DelayAiAction", 1f);
        }

        UIManager.main.UpdateText();
        UIManager.main.ResetBlockCounter();
    }
    public void CounterAttack()
    {

        //récupère infos pour affichage du damage
        combatTextPosition = enemySprite.transform.position;
        combatTextColor = new Color(1, 0, 0, 1);
        combatTextFontSize = 60;
        damage = player.GetDamage();
        damage = Mathf.Max(1, damage - EntityManager.main.GetActiveEnemy().GetDefense()); //test de soustraire avant de multiplier


        if (currentQteResult == 2)
        {
            comboText = "COUNTER";
            player.AddToCurrentComboList(comboText, false);
            player.CheckForCombo();
        }

        if (currentQteResult == 1)
        {
            comboText = "COUNTER";
            player.AddToCurrentComboList(comboText, true);
            player.CheckForCombo();
            damage = damage / 2;

        }

        

        //damage = Mathf.Max(0, damage - enemy.GetDefense());
        combatText = damage.ToString();

        if (currentQteResult < 1)
        {
            player.ClearCurrentComboList();  //combo breaker
            damage = 0;
            combatText = "MISS";
            combatTextFontSize = 48;
        }
        //crée l'objet qui affiche le damage
        CombatTextManager.Main.CreateText(combatTextPosition, combatText, combatTextColor, combatTextFontSize);

        //envoie la Audiobank de type ENEMY_HIT au SoundManager avec un délai pour jouer le son
        SoundManagerN.instance.PlaySfx(0.4f, SoundManagerN.AudioBank.ENEMY_HIT);

        player.DamageTarget(enemy, damage);
        enemy.AddToTurnDamageCounter(damage);

        UIManager.main.UpdateText();

        currentGameState = GameState.EnemyTurn;
        //Debug.Log("Counter attack is calling next action NOW");
        DelayNextAiAction();
        //Debug.Log(currentQteResult);
        //Invoke("DelayAiAction", 1f);
    }

    public void Focus()
    {
        switch (focusType)
        {
            case 1:
                combatText = "STR X 2";
                comboText = "STRFOCUS";
                break;

            case 2:
                combatText = "END X 2";
                comboText = "ENDFOCUS";
                break;

            case 3:
                combatText = "SPD X 2";
                comboText = "SPDFOCUS";
                break;
        }

        if (currentGameState == GameState.PlayerTurn)
        {
            combatTextPosition = playerSprite.transform.position;
            combatTextColor = new Color(0, 1, 1, 1);
            combatTextFontSize = 36;

            if (currentQteResult >= 2)
            {
                player.ActivateFocus(focusType);
                player.AddToCurrentComboList(comboText, false);
                player.CheckForCombo();
                if (player.GetFocus(1) && player.GetFocus(2) && player.GetFocus(3))
                {
                    player.FocusCharge();
                }
            }
            else
            {
                player.ClearCurrentComboList();  //combo breaker
                combatText = "FAILED";
                combatTextColor = new Color(1, 0, 0, 1);
                UIManager.main.actionText[UIManager.main.GetActionsCount() - 1].ChangeColor(new Color(1f, 0f, 0f, 1f), 0.4f);
            }
            CombatTextManager.Main.CreateText(combatTextPosition, combatText, combatTextColor, combatTextFontSize);
            if (player.GetCurrentAp() > 0)
            {
                UIManager.main.TogglePlayerActionButtons();
            }
            UIManager.main.TogglePlayerEndTurn();
        }

        else if (currentGameState == GameState.EnemyTurn)
        {
            //enemy.RemoveAp(1);
            combatTextPosition = enemySprite.transform.position;
            combatTextColor = new Color(0f, 1f, 0f, 1f);
            combatTextFontSize = 48;
            combatText = "FOCUS";

            if (currentQteResult == 2)
            {
                enemy.ActivateFocus(focusType);
            }
            else
            {
                combatText = "FOCUS FAILED";
                combatTextColor = new Color(1, 0, 0, 1);
            }

            CombatTextManager.Main.CreateText(combatTextPosition, combatText, combatTextColor, combatTextFontSize);

            //envoie la Audiobank de type ENEMY_ATTACK au SoundManager
            //SoundManagerN.instance.PlaySfx(0.0f, SoundManagerN.AudioBank.ENEMY_ATTACK);

            //Invoke("DelayAiAction", 0.2f);
            
        }
        UIManager.main.UpdateText();
    }

    public void UseItem()
    {
        combatTextPosition = playerSprite.transform.position;
        combatTextColor = new Color(0, 1, 0, 1);
        combatTextFontSize = 60;

        player.UseItem(player);

        combatText = (player.GetMaxHp() / 2).ToString();

        CombatTextManager.Main.CreateText(combatTextPosition, combatText, combatTextColor, combatTextFontSize);
        UIManager.main.UpdateText();
        if (player.GetCurrentAp() > 0)
        {
            UIManager.main.TogglePlayerActionButtons();
        }
        UIManager.main.TogglePlayerEndTurn();

    }

    public void Heal()
    {
        if (currentGameState == GameState.PlayerTurn)
        {
            combatTextPosition = playerSprite.transform.position;
            combatTextColor = new Color(0, 1, 0, 1);
            combatTextFontSize = 60;

            player.UseItem(player);

            combatText = (player.GetMaxHp() / 2).ToString();

            CombatTextManager.Main.CreateText(combatTextPosition, combatText, combatTextColor, combatTextFontSize);
            UIManager.main.UpdateText();
            if (player.GetCurrentAp() > 0)
            {
                UIManager.main.TogglePlayerActionButtons();
            }
            UIManager.main.TogglePlayerEndTurn();
        }
        else if(currentGameState == GameState.EnemyTurn){

            combatTextPosition = enemySprite.transform.position;
            combatTextColor = new Color(0f, 1f, 0f, 1f);
            combatTextFontSize = 48;
            combatText = "";

            if (currentQteResult == 2)
            {
                int healedDamage = (int)(enemy.GetHealingPower() * enemy.GetMaxHp());
                enemy.HealDamage(healedDamage);
                combatText = healedDamage.ToString();
            }
            else
            {
                combatText = "HEALING FAILED";
                combatTextColor = new Color(1, 0, 0, 1);
            }

            CombatTextManager.Main.CreateText(combatTextPosition, combatText, combatTextColor, combatTextFontSize);
        }
        UIManager.main.UpdateText();
    }

    public void SwitchTurn()
    {

        if (currentGameState == GameState.PlayerTurn)
        {
            //player.unusedApCounter += player.GetCurrentAp(); //ajoute AP inutilisés à EXP
            //Debug.Log("Unused AP:" + player.GetCurrentAp().ToString());
            UIManager.main.DisplayEnemyInfo(1);

            player.turnCounter++;
            playingCreature = enemy;
            waitingCreature = player;
            enemy.ResetAp();
            enemy.DeactivateFocus();
            player.ClearCurrentComboList();  //clear combo list
            player.SetChainedAttacks(0); //enlève les chained attacks
            player.SetHeavyAttackSkills(false); //enlève les attack skills
            player.SetLightAttackSkills(false); //enlève les attack skills


            //test (était juste à la fin du tour de l'ennemi avant)
            enemy.UpdateStatus(true);

            enemy.CheckDamageCap();

            currentGameState = GameState.EnemyTurn;
            //aiPlayer.SetMoveNumber();
            aiPlayer.PlayAiAction();

        }
        else if(currentGameState == GameState.EnemyTurn)
        {
            failedTimeTurn = false;
            Invoke(nameof(TimeBonusPointsFail), turnTimeBonus);
            //UIManager.main.EndTurnEvent();
            //enemyAnimation.enemyAnimator.Play("Enemy_Idle", 0, 0);
            UIManager.main.ClearActionText();
            UIManager.main.StartTurnUIUpdate();
            UIManager.main.DisplayEnemyInfo(1);

            playingCreature = player;
            waitingCreature = enemy;
            player.ResetAp();
            player.DeactivateFocus();
            player.ClearCurrentComboList();  //clear combo list
            player.SetChainedAttacks(0); //enlève les chained attacks
            player.SetHeavyAttackSkills(false); //enlève les attack skills
            player.SetLightAttackSkills(false); //enlève les attack skills

            //Commenté pour test
            player.UpdateStatus(true);

            if (player.GetSpecialActive())
            {
                player.ToggleSpecialActive();
                player.EmptySpecialCharge();
                Debug.Log("SPECIAL DEACTIVATED");
            }

            enemy.CheckDamageCap();
            UIManager.main.DisplayEnemyAction("");

            currentGameState = GameState.PlayerTurn;
            
            //Debug.Log("Player's Turn");
        }
        UIManager.main.UpdateText();

    }

    public void CheckTurnTime()
    {   if (currentGameState == GameState.PlayerTurn)
        {
            CancelInvoke("TimeBonusPointsFail");
            if (failedTimeTurn)
                Debug.Log("Took too long");
            else
            {
                Debug.Log("Turn time good");
                player.quickTurnsCounter++; //Ajoute EXP si turnTimer respecté
            }
                
        }
    }
    public GameState GetGameState()
    {
        return currentGameState;
    }

    public Creature getPlayingCreature()
    {
        return playingCreature;

    }
    public void SetGameState(GameState newGameState)
    {
        currentGameState = newGameState;
    }

    public void StartQTE(int qteNumber,int qteType)
    {
        UIManager.main.DisplayEnemyInfo(0);

        currentGameState = GameState.QTE;

        switch (qteType)
        {
            case 1:
                if (player.GetSpecialActive())
                {
                    Debug.Log("Special Speed bonus:" + player.GetSpeed() * player.GetSpecialSpdCharge() / 100);
                    //Si le Special est actif, la charge en speed est ajoutée comme % du speed du player, ça top à 100
                    QTEManager.main.StartQTE(qteNumber, Mathf.Min(100, player.GetSpeed() + player.GetSpeed() * player.GetSpecialSpdCharge() / 100), enemy.GetSpeed());
                }
                else {
                QTEManager.main.StartQTE(qteNumber, player.GetSpeed(), enemy.GetSpeed());
                }
                break;
            case 2:
                if (player.GetSpecialActive())
                {
                    Debug.Log("Special Speed bonus:" + player.GetSpeed() * player.GetSpecialSpdCharge() / 100);
                    //Si le Special est actif, la charge en speed est ajoutée comme % du speed du player, ça top à 100
                    QTEManager.main.StartQTEType2(qteNumber, Mathf.Min(100, player.GetSpeed() + player.GetSpeed() * player.GetSpecialSpdCharge() / 100), enemy.GetSpeed());
                }
                else
                {
                    QTEManager.main.StartQTEType2(qteNumber, player.GetSpeed(), enemy.GetSpeed());
                }
                break;
        }
    }


    public void QTEDone()
    {
        UIManager.main.DisplayEnemyInfo(1);
        currentQteResult = QTEManager.main.getResult();
        StartAnimation();
    }

    public void StartAnimation()
    {
        if (currentGameState == GameState.QTE || currentGameState == GameState.PlayerTurn)
        {
            currentGameState = GameState.PlayerAnimation;
            playerAnimation.playerAnimator.SetBool(currentAnimationName, true);  //active l'animation du player
            //playerAnimation.playerAnimator.Play(currentAnimationName, 0, 0);
            
        }
        else
        {
            currentGameState = GameState.EnemyAnimation;
            enemyAnimation.enemyAnimator.SetBool(currentAnimationName, true);
            //enemyAnimation.enemyAnimator.Play(currentAnimationName, 0, 0);
            
        }
    }

    public void ExecuteAction()
    {
        if (currentGameState == GameState.PlayerAnimation)
        {
            currentGameState = GameState.PlayerTurn;
        }
        else
        {
            currentGameState = GameState.EnemyTurn;
        }
        currentPendingAction();    //NormalAttack(), SuperAttack(), Focus() or CounterAttack()
    }

    public void SetPendingAction(System.Action newPendingAction, string newAnimationName)
    {
        currentPendingAction = newPendingAction;
        currentAnimationName = newAnimationName;
    }

    public void AddActionToComboList(string newActionName)
    {

    }

    public void RemoveActionPoints(int actionPointsCost)
    {
        
        for (int i = 1; i <= actionPointsCost; i++)
        {
            UIManager.main.ApFlash(player.GetMaxAp() - player.GetCurrentAp()); //Fait flasher les boîtes de la ApBar
            //playerActionPoints -= 1;
        }
        player.RemoveAp(actionPointsCost);
    }
 
    public void SetFocusType(int newfocusType)
    {
        focusType = newfocusType;
    }

    public void SetEnemySkillCheck(int newEnemySkillCheck)
    {
        currentQteResult = newEnemySkillCheck;
    }
    public void TimeBonusPointsFail()
    {
        failedTimeTurn = true;
    }

    public bool EndCombatCheck()
    {
        if (player.GetCurrentHp() <= 0)//Player dies
        {
            combatActive = false;
            UIManager.main.DisplayScoreBoard(false);
        }
        if (enemy.GetCurrentHp() <= 0) //Enemy dies
        {
            SetGameState(GameState.BetweenCombat);
            combatActive = false;
            player.AddUpCombatExp();
            UIManager.main.DisplayScoreBoard(true);
        }
        return combatActive;
    }

    public void NextAiAction()
    {
        //Enlève l'animation du block pour la prochaine action du monstre
        playerAnimation.playerAnimator.SetBool("Block", false);
        currentGameState = GameState.EnemyTurn;
        if (EndCombatCheck())
        {
            aiPlayer.IncrementCurrentEnemyAction();
            aiPlayer.PlayAiAction();
        }
         
    }

    public void DelayNextAiAction()
    { 
        Invoke(nameof(NextAiAction), 1f * Time.timeScale);     
    }

    // Function that is called when the monster misses. Checks if the player wants to counter or not
    private void EndCounterInput()
    {
        //Debug.Log("Counter time has ran out");
        if (UIManager.main.hasCountered)
        {
            UIManager.main.hasCountered = false;
            player.counterAttackCounter++;
            SetPendingAction(CounterAttack, "Attack");
            StartQTE(2,1);
        }
        else
        {
            UIManager.main.SetPlayerCounterInput(false);
            if(enemyAnimation.enemyAnimator.GetBool("Attack") || enemyAnimation.enemyAnimator.GetBool("SuperAttack"))
            {
                //Debug.Log("Animation will make next action call");
                enemyAnimation.MakeNextActionCall();
            }
            else
            {
                //Debug.Log("Counter attack is calling next action NOW");
                DelayNextAiAction();  //invoke dans 1 seconde
            }
        }
    }

    public GameObject GetEnemySprite()
    {
        return enemySprite;
    }

}


