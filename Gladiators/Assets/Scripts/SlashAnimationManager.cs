﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlashAnimationManager : MonoBehaviour
{
    public static SlashAnimationManager main;

    public GameObject slashAnimationPrefab;

    //public float fadeTime;

    public RectTransform canvasTransform;

    //public float speed;
    //public Vector2 direction;

    //public string animationName;

    public static SlashAnimationManager Main
    {
        get
        {
            if (main == null)
            {
                main = FindObjectOfType<SlashAnimationManager>();
            }
            return main;
        }
    }

    //private void Awake()
    //{
    //    main = this;
    //}

    public void CreateSlashAnimation(Vector3 position, string animationName)
    {
        GameObject sct = (GameObject)Instantiate(slashAnimationPrefab, position, Quaternion.identity);
        sct.transform.SetParent(canvasTransform);
        sct.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        sct.GetComponent<SlashAnimation>().Initialize(animationName);
        //sct.GetComponent<Text>().text = text;
        //sct.GetComponent<Text>().color = color;
        //sct.GetComponent<Text>().fontSize = fontSize;
    }

}