﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


//Un EnemyMove contient toutes les actions faites par l'ennemi en un tour.
public class EnemyMove
{
    //public string name;
    private List<EnemyAction> actionList;
    //private int currentAction;

    public EnemyMove(List<EnemyAction> newActionList)
    {
        actionList = newActionList;
    }

    public List<EnemyAction> GetActionList()
    {
        return actionList;
    }
   
}