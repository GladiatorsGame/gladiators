﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class Status
{
    //Ne pas inclure durée dans les status?
    //Identifier avec un tag enum?  Utiliser le enum effect pour identification?
    private string statusName;
    private List<Effect> effectsList = new List<Effect> { };
    public enum StatusStartTurn { PLAYER, ENEMY };
    private StatusStartTurn statusStartTurn;

    //avoir un effectPower par effet?
    //avoir une classe qui associe un power à un effet, tout ça dans une liste?
    private int remainingDuration;


    //public Status(string newStatusName, List<Effect> newEffectsList, StatusStartTurn newStatusStartTurn, int newDuration)
    public Status(string newStatusName, List<Effect> newEffectsList, int newDuration)
    {
        statusName = newStatusName;
        remainingDuration = newDuration;
        //statusStartTurn = newStatusStartTurn;
        effectsList = newEffectsList;
    }

    public string GetName()
    {
        return statusName;
    }

    public List<Effect> GetStatusEffects()
    {
        return effectsList;
    }

    public int GetRemainingDuration()
    {
        return remainingDuration;
    }

    public StatusStartTurn GetStatusStartTurn()
    {
        return statusStartTurn;
    }

    public void DecrementDuration()
    {
        remainingDuration--;
    }
    public void IncreaseDuration(int increase)
    {
        remainingDuration+= increase;
    }
}
