﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;


//Une EnemySequence regroupe plusieurs moves d'une certaine catégorie
//Un move = 1 tour
//Les séquences peuvent contenir plusieurs moves et peuvent donc s'étaler sur plusieurs tours.
public class EnemySequence
{
    public Enemy.enemySequenceTags sequenceTag;  //Tag de la Sequence (identifiant)
    private List<EnemyMove> enemyMoveList;  //Liste des Moves dans la Sequence
    private int currentMoveNumber;  //À quel Move on est rendu dans la Sequence
    private string sequenceName;  //Nom de la Sequence
    private bool endPhase;  //bool qui détermine si la fin de la Sequence termine la présente Phase
    private int nextPhaseNumber;  //Prochaine Phase à adopter à la fin de la Sequence
  

    public EnemySequence(Enemy.enemySequenceTags newSequenceTag, bool newEndPhase, int newNextPhaseNumber)
    {
        currentMoveNumber = 0;
        sequenceTag = newSequenceTag;
        enemyMoveList = new List<EnemyMove>() { };
        endPhase = newEndPhase;
        nextPhaseNumber = newNextPhaseNumber;

        switch (sequenceTag)
        {
            case Enemy.enemySequenceTags.ATTACK:
                sequenceName = "ATTACK";
                break;

            case Enemy.enemySequenceTags.CHARGE:
                sequenceName = "CHARGE";
                break;

            case Enemy.enemySequenceTags.DEFEND:
                sequenceName = "DEFEND";
                break;

            case Enemy.enemySequenceTags.HEAL:
                sequenceName = "HEAL";
                break;

            case Enemy.enemySequenceTags.WAIT:
                sequenceName = "STATIC";
                break;
        }
    }

    public List<EnemyMove> GetEnemyMoveList()
    {
        return enemyMoveList;
    }

    public EnemyMove GetCurrentMove()
    {
        return enemyMoveList[currentMoveNumber];
    }

    public int GetCurrentMoveNumber()
    {
        return currentMoveNumber;
    }

    //public void NextMove()
    //{
    //    currentMoveNumber++;
    //    if(currentMoveNumber >= enemyMoveList.Count)
    //    {
    //        EntityManager.main.GetActiveEnemy().GetCurrentPhase().NextSequence();
    //        currentMoveNumber = 0;
    //    }

    //    Debug.Log("Current Move:" + currentMoveNumber);

    //}

    public void ResetMoveCounter()
    {
        currentMoveNumber = 0;
    }

    //Méthode qui fait passer au prochain Move de la Sequence
    public void IncreaseMoveCounter()
    {
        currentMoveNumber++;
    }

    public string GetSequenceName()
    {
        return sequenceName;
    }

    public bool GetEndPhase()
    {
        return endPhase;
    }

    public int GetNextPhaseNumber()
    {
        return nextPhaseNumber;
    }
}
