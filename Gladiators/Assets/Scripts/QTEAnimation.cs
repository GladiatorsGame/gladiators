﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QTEAnimation : MonoBehaviour
{

    public Animator qteAnimator;
    // Start is called before the first frame update

    private void OnFadeIn()
    {
        //QteAnimator.SetBool("FadeIn", false);
        qteAnimator.Play("QteTunnel", 0, 0);
        //Debug.Log("FADEIN QTE");
    }

    private void OnFadeOut()
    {
        //Debug.Log("FADEOUT QTE");
        //QteAnimator.SetBool("FadeOut", false);
    }
}
