﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class QTEManager : MonoBehaviour
{
    public static QTEManager main;
    public GameController gameController;
    public GameObject qteAnimationParent;
    public GameObject qteUIParent;
    public QTEAnimation qteAnimation;
    private bool qteInProgress;
    private int randomNumber, qteLeft;
    private bool qtePassed, timePassed;
    private int successCounter, failCounter;
    private float xJoystick, yJoystick, angle, joystickVectorSum;
    private int sectionPointed;
    private List<int> listOpenSections = new List<int>();
    private int resultat;
    public float countdownDuration;
    private float qteResetTime = 0.5f;

    private int totalSuccess = 0;
    private int totalQte = 0;


    //TEST Type2
    private bool qteInProgressType2, qtePassedType2;
    private int qteLeftType2, sectionPointedType2, frame, animationIndex, printLeft, printIndex;
    private int listIndex;
    private int type2Success = 0;
    public float speedQTEType2;

    //TEST SlashAnimation
    private Vector3 slashPosition;


    // Start is called before the first frame update
    void Start()
    {
        qteInProgress = false;
        qtePassed = false;

        qteInProgressType2 = false;
        qtePassedType2 = false;
    }
    void Awake()
    {
        main = this;

    }
    // Update is called once per frame
    void Update()
    {


        if (qteInProgressType2 || qteInProgress)
        {
            xJoystick = Input.GetAxisRaw("LeftJoystickX");
            yJoystick = Input.GetAxisRaw("LeftJoystickY") * -1;

            joystickVectorSum = Mathf.Abs(xJoystick) + Mathf.Abs(yJoystick);

            


            //TYPE 2 STARTS HERE
            if (qteLeftType2 > 0)
            {
                if (joystickVectorSum >= 1)
                {
                    int newSectionPointedType2 = getSectionPointed();

                    //Si c'est la première section pointée, change automatiquement la sectionPointedType2 sans vérifier
                    //si elle diffère de la valeur précédente
                    if (listIndex == 0)
                    {
                        sectionPointedType2 = newSectionPointedType2;
                        if (sectionPointedType2 == listOpenSections[listIndex])
                        {
                            type2Success++;
                            UIManager.main.UpdateQTEImage(sectionPointedType2, new Color(255, 0, 255, 1));
                            listIndex++;
                        }
                        else
                        {
                            UIManager.main.UpdateQTEImage(sectionPointedType2, new Color(255, 0, 0, 0.5f));
                            listIndex++;
                        }
                    }

                    //si le joueur a changé de section pointée et si toutes les opensections n'ont pas été remplies
                    if (newSectionPointedType2 != sectionPointedType2 && listIndex < listOpenSections.Count) 
                    {
                        sectionPointedType2 = newSectionPointedType2;
                        if (sectionPointedType2 == listOpenSections[listIndex])
                        {
                            type2Success++;
                            UIManager.main.UpdateQTEImage(sectionPointedType2, new Color(255,0,255,1));
                            listIndex++;
                        }
                        else
                        {
                            UIManager.main.UpdateQTEImage(sectionPointedType2, new Color(255, 0, 0, 0.5f));
                            listIndex++;
                        }
                    }

                }
                if (timePassed || listIndex == 8)//CODE DE NICOLAS
                {
                    if (type2Success == listOpenSections.Count)
                    {
                        //SoundManagerN.instance.StopQteProgressSfx();
                        SoundManagerN.instance.PlaySfx(0f, SoundManagerN.AudioBank.QTESUCCESS);
                        qtePassedType2 = true;
                    }

                    if (qtePassedType2)
                    {
                        successCounter = totalQte;
                        //Debug.Log("SUCCESS! Type2");
                    }
                    else
                    {
                        //SoundManagerN.instance.StopQteProgressSfx();
                        SoundManagerN.instance.PlaySfx(0f, SoundManagerN.AudioBank.QTEFAIL);
                        failCounter++;
                        //Debug.Log("FAIL Type2");
                    }
                    qteInProgressType2 = false;
                    CancelInvoke("Countdown");  //TEST DE NICOLAS
                    SoundManagerN.instance.StopSfxLoop(SoundManagerN.AudioBank.QTEPROGRESS, SoundManagerN.AudioBank.QTEEND);
                    SoundManagerN.instance.DeprioritizeAudioSources();

                    //QteAnimation.QteAnimator.SetBool("FadeOut", true);
                    qteAnimation.qteAnimator.Play("QteFadeOut", 0, 0);

                    Invoke("LastClear", 1f);
                    //Debug.Log("QTE Ended Type 2");
                }
            }
            //TYPE 2 ENDS HERE

            //TYPE 1 QTE
            //Condition that regulates the start of the qte
            if (qteLeft > 0)
            {

                //Condition to make the deadzone inactive
                //if (timePassed || (xJoystick != 0 && yJoystick != 0))//CODE DE LAURENT
                if (timePassed || (joystickVectorSum >= 1))//CODE DE NICOLAS
                {
                    if (joystickVectorSum >= 1)//AJOUT DE NICOLAS
                    {
                        // angle = getAngle();

                        sectionPointed = getSectionPointed();

                        //Debug.Log("Section pointed : " + sectionPointed);
                        foreach (int section in listOpenSections)
                        {
                            //Debug.Log("Open Section : " + section);

                            if (section == sectionPointed)
                            {
                                qtePassed = true;
                            }
                        }
                    }

                    if (qtePassed)
                    {
                        UIManager.main.UpdateQTEImage(sectionPointed, new Color(255, 0, 255, 1));
                        successCounter++;
                        //SoundManagerN.instance.StopQteProgressSfx();
                        SoundManagerN.instance.PlaySfx(0f, SoundManagerN.AudioBank.QTESUCCESS);
                    }
                    else
                    {
                        //SoundManagerN.instance.StopQteProgressSfx();
                        SoundManagerN.instance.PlaySfx(0f, SoundManagerN.AudioBank.QTEFAIL);
                        UIManager.main.UpdateQTEImage(sectionPointed, new Color(255, 0, 0, 0.5f));
                        failCounter++;
                    }

                    if (qteLeft > 1)
                    {
                        //Debug.Log("QTE STOPPED");
                        qteLeft--;
                        qteInProgress = false;
                        CancelInvoke("Countdown");  //TEST DE NICOLAS
                        Invoke("ResetQTE", qteResetTime); // This amount is the time between the rounds of the qte

                        //Debug.Log("QTE left" + qteLeft);
                        //Debug.Log("Success : " + successCounter);
                        //Debug.Log("Fails : " + failCounter);
                    }
                    else
                    {
                        qteLeft--;
                        CancelInvoke("Countdown");  //TEST DE NICOLAS
                        SoundManagerN.instance.StopSfxLoop(SoundManagerN.AudioBank.QTEPROGRESS, SoundManagerN.AudioBank.QTEEND);
                        SoundManagerN.instance.DeprioritizeAudioSources();

                        qteAnimation.qteAnimator.Play("QteFadeOut", 0, 0);
                        //QteAnimation.QteAnimator.SetBool("FadeOut", true);
                        //QteAnimationParent.SetActive(false);

                        Invoke("LastClear", 1f);


                        //Debug.Log("QTE left" + qteLeft);
                        //Debug.Log("Success : " + successCounter);
                        //Debug.Log("Fails : " + failCounter);

                        //Debug.Log("QTE Ended");
                    }
                }
            }
        }
    }

    /// <summary>
    /// Will probably take the creature in arguments to position the QTE
    /// </summary>
    public void StartQTE(int numberOfQTE, int playerSpeed, int enemySpeed)
    {
        qteAnimationParent.SetActive(true);
        qteUIParent.SetActive(true);
        //QteAnimation.QteAnimator.SetBool("FadeIn", true);
        qteAnimation.qteAnimator.Play("QteFadeIn", 0, 0);

        UIManager.main.ClearQTEImage();
        qteLeft = numberOfQTE;
        totalQte = numberOfQTE;

        playerSpeed = Mathf.Min(100, playerSpeed);
        enemySpeed = Mathf.Min(100, enemySpeed);
        
        if (playerSpeed - enemySpeed >= 0)
            countdownDuration = 1600 - Mathf.Sqrt(-6400 * (playerSpeed - enemySpeed - 100));
        else
            countdownDuration = 4 * (playerSpeed - enemySpeed) + 800;

        countdownDuration = countdownDuration / 1000;

        //Debug.Log("Countdown duration:" + countdownDuration);

        //SoundManagerN.instance.PlayQteProgressSfx(0);
        SoundManagerN.instance.PrioritizeAudioSources(new SoundManagerN.AudioBank[] { SoundManagerN.AudioBank.QTESTART, SoundManagerN.AudioBank.QTEPROGRESS, SoundManagerN.AudioBank.UI });
        SoundManagerN.instance.PlaySfxLoop(SoundManagerN.AudioBank.QTESTART, SoundManagerN.AudioBank.QTEPROGRESS);
        ResetQTE();

    }

    public void StartQTEType2(int numberOfQTE, int playerSpeed, int enemySpeed)
    {
        //Fait apparaître le UI du QTE
        qteAnimationParent.SetActive(true);
        qteUIParent.SetActive(true);
        
        //Joue l'animation du QTE
        qteAnimation.qteAnimator.Play("QteFadeIn", 0, 0);

        //Reset des images du QTE
        UIManager.main.ClearQTEImage();

        //Setup du nombre de QTE à faire
        qteLeftType2 = numberOfQTE;
        totalQte = numberOfQTE;

        //Setup du temps donné pour QTE basé sur speeds
        playerSpeed = Mathf.Min(100, playerSpeed);
        enemySpeed = Mathf.Min(100, enemySpeed);

        if (playerSpeed - enemySpeed >= 0)
            countdownDuration = 1600 - Mathf.Sqrt(-6400 * (playerSpeed - enemySpeed - 100));
        else
            countdownDuration = 4 * (playerSpeed - enemySpeed) + 800;

        //Remise en millisecondes + ajout d'une seconde pour la durée de l'animation
        countdownDuration = countdownDuration / 1000 + 1;

        //Gestion du son
        SoundManagerN.instance.PrioritizeAudioSources(new SoundManagerN.AudioBank[] { SoundManagerN.AudioBank.QTESTART, SoundManagerN.AudioBank.QTEPROGRESS, SoundManagerN.AudioBank.UI });
        SoundManagerN.instance.PlaySfxLoop(SoundManagerN.AudioBank.QTESTART, SoundManagerN.AudioBank.QTEPROGRESS);

        //Reset du QTE
        ResetQTEType2();
    }



    private void ResetQTE()
    {
        CancelInvoke("Countdown");

        UIManager.main.ClearQTEImage();
        UIManager.main.QTEImageInactive();

        listOpenSections.Clear();

        randomNumber = Random.Range(0, 8);
        listOpenSections.Add(randomNumber);
        UIManager.main.UpdateQTEImage(randomNumber, new Color(0, 255, 255, 120));

        //2e section REMOVED
        //randomNumber = Random.Range(0, 8);
        //listOpenSections.Add(randomNumber);
        //UIManager.main.UpdateQTEImage(randomNumber, Color.yellow);

        qtePassed = false;
        timePassed = false;
        qteInProgress = true;
        Invoke("Countdown", countdownDuration); // This number is the time to complete before automatic fail
    }


    private void ResetQTEType2()
    {
        //Annule le timer
        CancelInvoke("Countdown");

        //Reset des images
        UIManager.main.ClearQTEImage();

        //Vide la liste des sections à pointer
        listOpenSections.Clear();

        //Remet compteur de sections pointées à zéro
        type2Success = 0;

        // Ajout des section à pointer dans le bon ordre
        randomNumber = Random.Range(0, 4);
        randomNumber = randomNumber * 2; //pour que le QTE ne commence pas en diagonal
        for(int i = randomNumber; i < 8; i++)
        {
            listOpenSections.Add(i);//création de la liste à partir d'un nombre random
        }
        
        for (int i = 0; i < randomNumber; i++)
        {
            listOpenSections.Add(i);//création de la liste à partir d'un nombre random
        }

        Debug.Log("Liste Open Sections:" + listOpenSections[0] + " " + listOpenSections[1] + " " + listOpenSections[2] + " " + listOpenSections[3] + " " + listOpenSections[4] + " " + listOpenSections[5] + " " + listOpenSections[6] + " " + listOpenSections[7]);

        //Printleft est le nombre d'images restantes à afficher, printIndex est où l'affichage des images est rendu
        printLeft = 8;
        printIndex = 0;
        Invoke("PrintQTE2", speedQTEType2);

        qtePassedType2 = false;
        timePassed = false;
        qteInProgressType2 = true;
        animationIndex = 0;
        listIndex = 0;
        Invoke("Countdown", countdownDuration); // This number is the time to complete before automatic fail
    }

    private void PrintQTE2()
    {
        UIManager.main.UpdateQTEImage(listOpenSections[printIndex], new Color(0, 255, 255, 1));
        printIndex++;
        printLeft--;
        if (printLeft >= 1){
            Invoke("PrintQTE2", speedQTEType2);
        }
    }
    private void LastClear()
    {
        // This is where the action will be applied depending on the result of the qte
        qteInProgress = false;
        qteInProgressType2 = false;
        // Debug.Log("QTE STOPPED Last clear");

        qteAnimationParent.SetActive(false);

        if (successCounter == totalQte)
        {
            EntityManager.main.GetPlayer().perfectCounter++;
        }

        listOpenSections.Clear();
        type2Success = 0;

        qteLeft = 0;
        qteLeftType2 = 0;

        UIManager.main.ClearQTEImage();
        qteUIParent.SetActive(false);
        gameController.QTEDone();


    }
    private void Countdown()
    {
        timePassed = true;
    }



    public int getResult()
    {
        int qteResult = successCounter;
        totalSuccess += successCounter;
        successCounter = 0;
        failCounter = 0;
        return qteResult;
    }

    //Section to find the angle of the joystick
    private float getAngle()
    {
        if (yJoystick > 0)
        {
            if (xJoystick > 0)
            {
                return Mathf.Atan2(yJoystick, xJoystick) * Mathf.Rad2Deg;
                //Debug.Log(1);
            }
            else
            {
                return Mathf.Atan2(yJoystick, xJoystick) * Mathf.Rad2Deg;
                //Debug.Log(2);
            }
        }
        else
        {
            if (xJoystick > 0)
            {
                return 360 + (Mathf.Atan2(yJoystick, xJoystick) * Mathf.Rad2Deg);
                //Debug.Log(4);
            }
            else
            {
                return 360 + (Mathf.Atan2(yJoystick, xJoystick) * Mathf.Rad2Deg);
                //Debug.Log(3);
            }
        }
    }

    private int getSectionPointed()
    {
        angle = getAngle();
        if (angle < 22.5 || angle >= 337.5)//315
        {
            return 0;
            //Debug.Log(8);
        }
        else if (angle < 67.5)//270
        {
            return 1;

            //Debug.Log(7);
        }
        else if (angle < 112.5)//225
        {
            return 2;

            //Debug.Log(6);
        }
        else if (angle < 157.5)//180
        {
            return 3;

            //Debug.Log(5);
        }
        else if (angle < 202.5)//135
        {
            return 4;

            //Debug.Log(4);
        }
        else if (angle < 247.5)//90
        {
            return 5;

            //Debug.Log(3);
        }
        else if (angle < 292.5)//45
        {
            return 6;

            //Debug.Log(2);
        }
        else
        {
            return 7;

            //Debug.Log(1);
        }
    }
 
}
