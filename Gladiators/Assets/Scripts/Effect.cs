﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

    public class Effect
    {
        public Effect.EffectId statusEffectId;
        public int statusEffectPower;
        public enum EffectId { TAKEDAMAGE, ATTACKUP, ATTACKDOWN, DEFENSEUP, DEFENSEDOWN, SKILLUP, SKILLDOWN, FOCUSLOCKED, NONE };

    public Effect(Effect.EffectId newStatusEffectId, int newStatusEffectPower)
        {
            statusEffectId = newStatusEffectId;
            statusEffectPower = newStatusEffectPower;
        }
    }