﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;



public class Player : Creature
{
    public int perfectCounter, quickTurnsCounter, counterAttackCounter, blockCounter, noItemCounter, turnCounter, combatExp, levelExp, totalExp;
    public int levelBaseStrength, levelBaseEndurance, levelBaseSpeed, levelBaseWeaponDamage, levelBaseArmor;
    public int maxSpecialCharge, currentSpecialCharge, specialStrCharge, specialSpdCharge, specialEndCharge;
    public List<int> halfComboList;
    

    public float chargeModifier;
    public bool chainingAttacks;
    private bool specialActive;
    public List<string> currentComboList;
    private bool heavyAttackSkillsAvailable, lightAttackSkillsAvailable;
    private Enemy enemy;

    private Vector3 comboTextPosition;
    private Color comboTextColor;
    private int comboTextFontSize;
    private string comboText;
    private Player player;

    public enum scoreBoardStats { STR, END, SPD, WPNDMG, ARMOR };

    public Player(int baseHp, int strenght, int endurance, int speed, int weaponDamage, int armor, int superAttackMultiplier, int healingPower) : base(baseHp, strenght, endurance, speed, weaponDamage, armor, superAttackMultiplier, healingPower)
    {
        player = this;
        perfectCounter = 0;
        quickTurnsCounter = 0;
        counterAttackCounter = 0;
        blockCounter = 0;
        noItemCounter = 0;
        turnCounter = 0;
        combatExp = 0;
        levelExp = 0;
        totalExp = 0;
        maxSpecialCharge = 24;
        currentSpecialCharge = 0;
        specialStrCharge = 0;
        specialEndCharge = 0;
        specialSpdCharge = 0;
        specialActive = false;
        chainingAttacks = false;
        currentComboList = new List<string>();
        halfComboList = new List<int>();
        chargeModifier = 1f;

        heavyAttackSkillsAvailable = false;
        lightAttackSkillsAvailable = false;

        

        //enemy = EntityManager.main.GetActiveEnemy();

    }

    public void AddUpCombatExp()
    {
        int playerStats = EntityManager.main.GetActiveEnemy().GetExpStats();
        int enemyStats = EntityManager.main.GetActiveEnemy().GetExpStats();
        int enemyExpBonus = Math.Max(0, enemyStats * 10 / playerStats);

        int turnPenalty = Math.Min(0, 5 - turnCounter);

        combatExp = perfectCounter + quickTurnsCounter + counterAttackCounter + blockCounter + turnPenalty;
        combatExp += combatExp * enemyExpBonus / 100;

        levelExp += combatExp;

        totalExp += combatExp;

        levelBaseStrength = player.GetStrengthStat();
        levelBaseEndurance = player.GetEnduranceStat();
        levelBaseSpeed = player.GetSpeedStat();
        levelBaseWeaponDamage = player.GetWeaponDamageStat();
        levelBaseArmor = player.GetArmorStat();


    }

    public void ClearCombatExp()
    {
        perfectCounter = 0;
        quickTurnsCounter = 0;
        counterAttackCounter = 0;
        blockCounter = 0;
        noItemCounter = 0;
        turnCounter = 0;
        combatExp = 0;
    }

    public void EmptySpecialCharge()
    {
        currentSpecialCharge = 0;
        specialStrCharge = 0;
        specialSpdCharge = 0;
        specialEndCharge = 0;
    }

    public int GetCurrentSpecialCharge()
    {
        return currentSpecialCharge;
    }

    public int GetMaxSpecialCharge()
    {
        return maxSpecialCharge;
    }

    public bool GetSpecialActive()
    {
        return specialActive;
    }

    public void ToggleSpecialActive()
    {
        specialActive =! specialActive;
    }

    public int GetSpecialStrCharge()
    {
        return specialStrCharge;
    }

    public int GetSpecialSpdCharge()
    {
        return specialSpdCharge;
    }

    public int GetSpecialEndCharge()
    {
        return specialEndCharge;
    }

    public void ClearCurrentComboList()
    {
        currentComboList.Clear();
        halfComboList.Clear();
        chargeModifier = 1f;
        Debug.Log("COMBO LIST CLEARED");
    }

    public void AddToCurrentComboList(string newAction, bool isHalf)
    {
        currentComboList.Add(newAction);
        halfComboList.Add(0);
        if (isHalf)
        {
            halfComboList[halfComboList.Count -1] = 1;
        }
    }

    public void CheckForCombo()
    {
        //Si 3 actions ont été enchaînées et que la gauge du special n'est pas pleine
        if (currentComboList.Count >= 3 && specialActive == false)
        {
            while (currentComboList.Count > 3)//ramène le nombre d'actions à 3 pour calcul du combo
            {
                currentComboList.RemoveAt(0);
                halfComboList.RemoveAt(0);
            }
            foreach (Combo combo in PlayerComboSet.GetComboList())
            {
                if (combo.GetActionList().SequenceEqual(currentComboList)) //si le combo est trouvé parmi la liste de tous les combos
                {
                    if (currentSpecialCharge < maxSpecialCharge)  //si la gauge de special n'est pas totalement chargée
                    {
                        //halfComboCounter compte le nombre d'actions "Half success" dans le combo
                        //la charge du Special est modifiée en conséquence
                        switch (halfComboList.Sum())
                        {
                            case 0:
                                break;

                            case 1:
                                chargeModifier = 0.75f;
                                break;

                            case 2:
                                chargeModifier = 0.5f;
                                break;

                            case 3:
                                chargeModifier = 0.25f;
                                break;
                        }

                        //AJOUT DE LA CHARGE AUX STATS
                        specialStrCharge = (int)Mathf.Min(100, specialStrCharge + combo.GetStrCharge() * chargeModifier);
                        specialEndCharge = (int)Mathf.Min(100, specialEndCharge + combo.GetEndCharge() * chargeModifier);
                        specialSpdCharge = (int)Mathf.Min(100, specialSpdCharge + combo.GetSpdCharge() * chargeModifier);

                        //AJOUT DE LA CHARGE A LA GAUGE DU SPECIAL
                        currentSpecialCharge += combo.GetSpecialChargeValue();

                        //PREPARATION DU TEXTE DE COMBO
                        comboTextPosition = GameController.main.playerSprite.transform.position;
                        comboTextPosition.x = comboTextPosition.x - 1;
                        comboTextColor = new Color(1, 0, 1, 1);
                        comboTextFontSize = 24;
                        comboText = combo.GetName();
                        
                        //VERIFICATION DES FOCUS ACTIVÉS POUR BONUS
                        switch (combo.GetSpecialChargeType())   //détermine le type de charge pour le special move
                        {
                            case Combo.ChargeType.STR:
                                if (player.GetFocus(1))
                                {
                                    specialStrCharge = Mathf.Min(100, specialStrCharge + combo.GetFocusBonus());
                                    comboText = comboText + "+";
                                }
                                break;

                            case Combo.ChargeType.END:
                                if (player.GetFocus(2))
                                {
                                    specialEndCharge = Mathf.Min(100, specialEndCharge + combo.GetFocusBonus());
                                    comboText = comboText + "+";
                                }
                                break;
                            case Combo.ChargeType.SPD:
                                if (player.GetFocus(3))
                                {
                                    specialSpdCharge = Mathf.Min(100, specialSpdCharge + combo.GetFocusBonus());
                                    comboText = comboText + "+";
                                }
                                break;
                            //case Combo.ChargeType.ALL:
                            //    specialStrCharge = Mathf.Min(5, specialStrCharge+1);
                            //    specialEndCharge = Mathf.Min(5, specialEndCharge+1);
                            //    specialSpdCharge = Mathf.Min(5, specialSpdCharge+1);
                            //    currentSpecialCharge += combo.GetSpecialChargeValue();
                            //    break;
                        }
                        if(currentSpecialCharge > maxSpecialCharge)  //si la charge du special dépasse la charge max, change pour la charge max
                        {
                            currentSpecialCharge = maxSpecialCharge;
                        }
                    }
                    
                    //AFFICHAGE DU COMBO TEXT
                    CombatTextManager.Main.CreateComboText(comboTextPosition, comboText, comboTextColor, comboTextFontSize);

                    //SI LE COMBO ACTIVE ATTACK SKILLS, REND ATTACK SKILLS DISPONIBLES
                    if (combo.GetHeavyAttackSkills())
                    {
                        player.SetHeavyAttackSkills(true);
                    }
                    if (combo.GetLightAttackSkills())
                    {
                        player.SetLightAttackSkills(true);
                    }
                    //SI LE COMBO ACTIVE CHAINED ATTACKS, SET A LA PREMIÈRE CHAINED ATTACK
                    if (combo.GetChainingAttacksValue())
                    {
                        player.SetChainedAttacks(1);
                    }
                    ClearCurrentComboList();
                    break;   
                }
            }
        }
    }

    //Quand les 3 focus sont activés, l'attaque spéciale est chargée, 20 points dans chaque stat.
    public void FocusCharge()
    {
        if (currentSpecialCharge < maxSpecialCharge)
        {
            specialStrCharge = Mathf.Min(100, specialStrCharge + 20);
            specialEndCharge = Mathf.Min(100, specialEndCharge + 20);
            specialSpdCharge = Mathf.Min(100, specialSpdCharge + 20);

            //Charge le special de 1/3
            currentSpecialCharge = currentSpecialCharge + 8;

            //PREPARATION DU TEXTE DE COMBO
            comboTextPosition = GameController.main.playerSprite.transform.position;
            comboTextPosition.x = comboTextPosition.x - 1;
            comboTextColor = new Color(1, 0, 1, 1);
            comboTextFontSize = 24;
            comboText = "FOCUS COMBO";
            CombatTextManager.Main.CreateComboText(comboTextPosition, comboText, comboTextColor, comboTextFontSize);

        }
        if (currentSpecialCharge > maxSpecialCharge)  //si la charge du special dépasse la charge max, change pour la charge max
        {
            currentSpecialCharge = maxSpecialCharge;
        }

    }

    public void SetHeavyAttackSkills(bool newValue)
    {
        heavyAttackSkillsAvailable = newValue;
    }

    public bool GetHeavyAttackSkills()
    {
        return heavyAttackSkillsAvailable;
    }

    public void SetLightAttackSkills(bool newValue)
    {
        lightAttackSkillsAvailable = newValue;
    }

    public bool GetLightAttackSkills()
    {
        return lightAttackSkillsAvailable;
    }


    public void IncreaseStat(scoreBoardStats stat)
    {
        switch (stat)
        {
            case scoreBoardStats.STR:
                if (levelExp >= 1)
                {
                    IncreaseStrengthStat(1);
                    levelExp -= 1;
                }
                break;
            case scoreBoardStats.END:
                if (levelExp >= 1)
                {
                    int lostHp = GetMaxHp() - GetCurrentHp();
                    IncreaseEnduranceStat(1);
                    SetMaxHp(GetBaseHp() + GetEnduranceStat() * 20);
                    SetCurrentHp(GetMaxHp() - lostHp);
                    levelExp -= 1;
                }
                break;
            case scoreBoardStats.SPD:
                if (levelExp >= 1)
                {
                    IncreaseSpeedStat(1);
                    levelExp -= 1;
                }
                break;
            case scoreBoardStats.WPNDMG:
                if (levelExp >= 1)
                {
                    IncreaseWeaponDamage(1);
                    levelExp -= 1;
                }
                break;
            case scoreBoardStats.ARMOR:
                if (levelExp >= 2)
                {
                    IncreaseArmor(1);
                    levelExp -= 2;
                }
                break;
        }
    }

    public void DecreaseStat(scoreBoardStats stat)
    {
        switch (stat)
        {
            case scoreBoardStats.STR:
                if (levelExp < combatExp && GetStrengthStat() > levelBaseStrength)
                {
                    IncreaseStrengthStat(-1);
                    levelExp += 1;
                }
                break;
            case scoreBoardStats.END:
                if (levelExp < combatExp && GetEnduranceStat() > levelBaseEndurance)
                {
                    int lostHp = GetMaxHp() - GetCurrentHp();
                    IncreaseEnduranceStat(-1);
                    SetMaxHp(GetBaseHp() + GetEnduranceStat() * 20);
                    SetCurrentHp(GetMaxHp() - lostHp);
                    levelExp += 1;
                }
                break;
            case scoreBoardStats.SPD:
                if (levelExp < combatExp && GetSpeedStat() > levelBaseSpeed)
                {
                    IncreaseSpeedStat(-1);
                    levelExp += 1;
                }
                break;
            case scoreBoardStats.WPNDMG:
                if (levelExp < combatExp && GetWeaponDamage() > levelBaseWeaponDamage)
                {
                    IncreaseWeaponDamage(-1);
                    levelExp += 1;
                }
                break;
            case scoreBoardStats.ARMOR:
                if (levelExp <= combatExp - 2 && GetArmorStat() > levelBaseArmor)
                {
                    IncreaseArmor(-1);
                    levelExp += 2;
                }
                break;
        }
    }
}
