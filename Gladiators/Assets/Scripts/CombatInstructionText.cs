﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CombatInstructionText : MonoBehaviour
{
    private float speed;
    private Vector2 direction;
    private float fadeTime;
    private float flashProgress;
    private float flashRate;
    private Color nextColor;
    private Color originalColor;
    private bool toNextColor;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (flashProgress >= 1.0)
        {
            if (toNextColor)
            {
                nextColor.a = GetComponent<Text>().color.a;
                GetComponent<Text>().color = nextColor;
                flashProgress = 0;
                toNextColor = false;
            }
            else
            {
                originalColor.a = GetComponent<Text>().color.a;
                GetComponent<Text>().color = originalColor;
                flashProgress = 0;
                toNextColor = true;
            }
        }
        flashProgress += flashRate * Time.deltaTime / Time.timeScale;
        
    }
    public void Initialize(float speed, Vector2 direction, float fadeTime, Color originalColor)
    {
        this.speed = speed;
        this.fadeTime = fadeTime;
        this.direction = direction;
        this.originalColor = originalColor;

        flashProgress = 1.0f;
        flashRate = 8.0f;
        nextColor = new Color(1, 1, 1, 1);
        toNextColor = true;

        //Si les infos de l'ennemi ne devraient pas être affichées, cache les à 10% d'alpha 
        
        UIManager.main.DisplayEnemyInfo(0.1f);  

        StartCoroutine(FadeOut());
    }

    private IEnumerator FadeOut()
    {
        float startAlpha = GetComponent<Text>().color.a;

        float rate = 1.0f / fadeTime;
        float progress = 0.0f;

        while (progress < 1.0)
        {
            Color tmpColor = GetComponent<Text>().color;
            GetComponent<Text>().color = new Color(tmpColor.r, tmpColor.g, tmpColor.b, Mathf.Lerp(startAlpha, 0, progress));
            progress += rate * Time.deltaTime / Time.timeScale;
            yield return null;
        }
        UIManager.main.DisplayEnemyInfo(1);
        Destroy(gameObject);

    }

}
