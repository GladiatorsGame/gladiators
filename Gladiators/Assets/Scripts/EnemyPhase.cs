﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

//Une EnemyPhase contient plusieurs séquences de moves et sera répétée en boucle
//Il faut remplir des conditions pour changer d'EnemyPhase.

public class EnemyPhase
{
    public Enemy.enemyPhaseTags phaseTag;
    private string phaseName;
    private List<EnemySequence> enemySequenceList;
    private List<Effect> phaseStatusEffects;
    private int currentSequence;
    private int turnDamageCap;
    private int turnSkillCap;
    private string phaseStatusName;
    private int phaseStatusPower;
    private int phaseStatusDuration;

    public EnemyPhase(Enemy.enemyPhaseTags newEnemyPhaseTag, int turnDamageCap = 0, string phaseStatusName = "", List<Effect> phaseStatusEffects = null, int phaseStatusDuration = 1)
    {
        //enemy = EntityManager.main.GetActiveEnemy();
        currentSequence = 0;
        phaseTag = newEnemyPhaseTag;
        enemySequenceList = new List<EnemySequence>() { };
        this.phaseStatusEffects = phaseStatusEffects ?? new List<Effect> { };
        this.turnDamageCap = turnDamageCap;
        this.phaseStatusName = phaseStatusName;
        this.phaseStatusDuration = phaseStatusDuration;

        switch (phaseTag)
        {
            case Enemy.enemyPhaseTags.NORMAL:
                phaseName = "NORMAL";
                break;

            case Enemy.enemyPhaseTags.DISRUPTED:
                phaseName = "DISRUPTED";
                break;

            case Enemy.enemyPhaseTags.PANIC:
                phaseName = "PANIC";
                break;
        }
    }

    public List<EnemySequence> GetEnemySequenceList()
    {
        return enemySequenceList;
    }

    public int GetCurrentSequenceNumber()
    {
        return currentSequence;
    }

    public EnemySequence GetCurrentSequence()
    {
        return enemySequenceList[currentSequence];
    }

    public void IncreaseSequenceCounter()
    {
        currentSequence++;
    }

    public void ResetSequenceCounter()
    {
        currentSequence = 0;
    }

    public string GetPhaseName()
    {
        return phaseName;
    }

    public int GetTurnDamageCap()
    {
        return turnDamageCap;
    }

    public string GetPhaseStatusName()
    {
        return phaseStatusName;
    }

    public List<Effect> GetPhaseStatusEffects()
    {
        return phaseStatusEffects;
    }

    public int GetPhaseStatusDuration()
    {
        return phaseStatusDuration;
    }
}
