﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


//Un EnemyAction est une action simple réalisée par l'ennemi.
public class EnemyAction
{
    private string enemyActionName;
    public enum EnemyActionType { Attack, SuperAttack, Wait, Heal, StrFocus, EndFocus, SpdFocus}
    private EnemyActionType enemyActionType;
    private int enemyActionPower, enemyActionEffectDuration;
    private List<Effect> enemyActionEffectsList;
    private List<Status> enemyActionStatusList;

    public EnemyAction(string name, EnemyActionType type, int power = 0, int duration = 0, List<Effect> effectsList = null, List<Status> statusList = null)
    {
        enemyActionName = name;
        enemyActionType = type;
        enemyActionPower = power;
        enemyActionEffectDuration = duration;
        enemyActionEffectsList = effectsList ?? new List<Effect> { };
        enemyActionStatusList = statusList ?? new List<Status> { };
    }

    public string GetEnemyActionName()
    {
        return enemyActionName;
    }

    public int GetEnemyActionPower()
    {
        return enemyActionPower;
    }

    public int GetEnemyActionEffectDuration()
    {
        return enemyActionEffectDuration;
    }

    public EnemyActionType GetEnemyActionType()
    {
        return enemyActionType;
    }

    public List<Effect> GetEnemyActionEffectsList()
    {
        return enemyActionEffectsList;
    }

    public List<Status> GetEnemyActionStatusList()
    {
        return enemyActionStatusList;
    }


}