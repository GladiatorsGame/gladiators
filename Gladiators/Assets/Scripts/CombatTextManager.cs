﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CombatTextManager : MonoBehaviour
{
    private static CombatTextManager main;

    [Header("Prefabs")]
    public GameObject textPrefab;
    public GameObject instructionTextPrefab;
    public GameObject comboTextPrefab;
    public GameObject enemyActionTextPrefab;

    [Header("Rect Transform")]
    public RectTransform canvasTransform;

    [Header("Combat Text")]
    public float combatTextFadeTime;
    public float textSpeed;
    public Vector2 textDirection;

    [Header("Instruction Text")]
    public float instructionTextFadeTime;
    public float instructionTextSpeed;
    public Vector2 instructionTextDirection;

    [Header("Combo Text")]
    public float comboTextFadeTime;
    public float comboTextSpeed;
    public Vector2 comboTextDirection;

    [Header("Enemy Action Text")]
    public float enemyActionTextFadeTime;
    public float enemyActionTextSpeed;
    public Vector2 enemyActionTextDirection;

    public static CombatTextManager Main
    {
        get
        {
            if (main == null)
            {
                main = FindObjectOfType<CombatTextManager>();
            }
            return main;
        }
    }

    //private void Awake()
    //{
    //    main = this;
    //}

    public void CreateText(Vector3 position, string text, Color color, int fontSize)
    {
        GameObject sct = (GameObject)Instantiate(textPrefab, position, Quaternion.identity);
        sct.transform.SetParent(canvasTransform);
        sct.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        sct.GetComponent<CombatText>().Initialize(textSpeed, textDirection, combatTextFadeTime);
        sct.GetComponent<Text>().text = text;
        sct.GetComponent<Text>().color = color;
        sct.GetComponent<Text>().fontSize = fontSize;
    }

    public void CreateInstructionText(Vector3 position, string text, Color color, int fontSize)
    {
        GameObject sct = (GameObject)Instantiate(instructionTextPrefab, position, Quaternion.identity);
        sct.transform.SetParent(canvasTransform);
        sct.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        sct.GetComponent<CombatInstructionText>().Initialize(instructionTextSpeed, instructionTextDirection, instructionTextFadeTime, color);
        sct.GetComponent<Text>().text = text;
        sct.GetComponent<Text>().color = color;
        sct.GetComponent<Text>().fontSize = fontSize;
    }

    public void CreateComboText(Vector3 position, string text, Color color, int fontSize)
    {
        GameObject sct = (GameObject)Instantiate(comboTextPrefab, position, Quaternion.identity);
        sct.transform.SetParent(canvasTransform);
        sct.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        sct.GetComponent<ComboText>().Initialize(comboTextSpeed, comboTextDirection, comboTextFadeTime);
        sct.GetComponent<Text>().text = text;
        sct.GetComponent<Text>().color = color;
        sct.GetComponent<Text>().fontSize = fontSize;
    }

    public void CreateEnemyActionText(Vector3 position, string text, Color color, int fontSize)
    {
        GameObject sct = (GameObject)Instantiate(enemyActionTextPrefab, position, Quaternion.identity);
        sct.transform.SetParent(canvasTransform);
        sct.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        sct.GetComponent<EnemyActionText>().Initialize(enemyActionTextSpeed, enemyActionTextDirection, enemyActionTextFadeTime);
        sct.GetComponent<Text>().text = text;
        sct.GetComponent<Text>().color = color;
        sct.GetComponent<Text>().fontSize = fontSize;
    }

}