﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu(fileName = "New Enemy Data", menuName = "Enemy Dataset", order = 1)]

public class EnemyData : ScriptableObject
{
    public string enemyName;
    public Enemy.EnemyTags enemyTag;
    public GameObject prefab;

    public int baseHp;
    public int strenght;
    public int endurance;
    public int speed;

    public int weaponDamage;
    public int armor;

    public int superAttackMultiplier;
    //public int nbHealing;
    public float healingPower;

    public int perfectCheck;
    public int doubleCheck;
    public int halfCheck;

    public int turnDamageCap;

    //[Header("Enemy Actions")]

    //[FormerlySerializedAs("phaseList")]
    //public List<EnemyPhase> phaseList;

}