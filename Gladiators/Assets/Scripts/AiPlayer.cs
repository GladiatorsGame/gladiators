﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiPlayer
{
    private Enemy enemy;
    private Player player;
    private int skillCheck, bleedingDamage;
   // private int moveNumber;
    private readonly List<EnemyPhase> phaseList;
    private readonly int currentPhase, currentSequence, currentMove;
    private int currentEnemyAction;
    private List<EnemyAction> enemyActionList;

    private string combatText;
    private Vector3 combatTextPosition;
    private Color combatTextColor;
    private int combatTextFontSize;

    private string enemyActionText;

    public void Initialize()
    {
        enemy = EntityManager.main.GetActiveEnemy();
        player = EntityManager.main.GetPlayer();
        currentEnemyAction = 0;
    }
    public void PlayAiAction()
    {
        enemyActionList = enemy.GetCurrentPhase().GetCurrentSequence().GetCurrentMove().GetActionList();

        if (currentEnemyAction < enemyActionList.Count)
        {
            skillCheck = enemy.GetSkillCheck();

            enemyActionText = enemyActionList[currentEnemyAction].GetEnemyActionName();
            UIManager.main.DisplayEnemyAction(enemyActionText);

            Debug.Log("Enemy action " + (currentEnemyAction + 1).ToString() + " / " + enemyActionList.Count + " : " + enemyActionList[currentEnemyAction].GetEnemyActionName());
            //Debug.Log(enemy.moveList[moveNumber].actionList[enemy.moveList[moveNumber].currentAction]);
            switch (enemyActionList[currentEnemyAction].GetEnemyActionType())
            {
                case EnemyAction.EnemyActionType.Attack:

                    if (skillCheck >= enemy.GetPerfectCheck())
                    {
                        GameController.main.SetEnemySkillCheck(3);
                    }
                    else if (skillCheck >= enemy.GetHalfCheck())
                    {
                        GameController.main.SetEnemySkillCheck(2);
                    }
                    else
                    {
                        GameController.main.SetEnemySkillCheck(0);
                    }
                    
                    GameController.main.SetPendingAction(GameController.main.NormalAttack, "Attack");//Enemy_Attack
                    GameController.main.StartAnimation();
                    break;

                case EnemyAction.EnemyActionType.SuperAttack:

                    if (skillCheck >= enemy.GetPerfectCheck())
                    {
                        GameController.main.SetEnemySkillCheck(5);
                    }
                    else if (skillCheck >= enemy.GetDoubleCheck())
                    {
                        GameController.main.SetEnemySkillCheck(4);
                    }
                    else if (skillCheck >= enemy.GetHalfCheck())
                    {
                        GameController.main.SetEnemySkillCheck(3);
                    }
                    else
                    {
                        GameController.main.SetEnemySkillCheck(0);
                    }
                    GameController.main.SetPendingAction(GameController.main.SuperAttack, "SuperAttack");//Enemy_Attack
                    GameController.main.StartAnimation();
                    break;

                case EnemyAction.EnemyActionType.StrFocus:    //focusType 1

                    if (skillCheck >= enemy.GetHalfCheck())
                    {
                        GameController.main.SetEnemySkillCheck(2);
                    }
                    else
                    {
                        GameController.main.SetEnemySkillCheck(0);
                    }
                    GameController.main.SetPendingAction(GameController.main.Focus, "Focus");//Enemy_Focus
                    GameController.main.SetFocusType(1);
                    GameController.main.StartAnimation();
                    break;

                case EnemyAction.EnemyActionType.EndFocus:    //focusType 2

                    if (skillCheck >= enemy.GetHalfCheck())
                    {
                        GameController.main.SetEnemySkillCheck(2);
                    }
                    else
                    {
                        GameController.main.SetEnemySkillCheck(0);
                    }
                    GameController.main.SetPendingAction(GameController.main.Focus, "Focus");//Enemy_Focus
                    GameController.main.SetFocusType(2);
                    GameController.main.StartAnimation();
                    break;

                case EnemyAction.EnemyActionType.SpdFocus:    //focusType 3

                    if (skillCheck >= enemy.GetHalfCheck())
                    {
                        GameController.main.SetEnemySkillCheck(2);
                    }
                    else
                    {
                        GameController.main.SetEnemySkillCheck(0);
                    }
                    GameController.main.SetPendingAction(GameController.main.Focus, "Focus");//Enemy_Focus
                    GameController.main.SetFocusType(3);
                    GameController.main.StartAnimation();
                    break;

                case EnemyAction.EnemyActionType.Heal:
                    
                    GameController.main.SetEnemySkillCheck(2); //automatic success
                    GameController.main.SetPendingAction(GameController.main.Heal, "Focus");
                    GameController.main.StartAnimation();
                    break;

                default:
                    Debug.Log("ACTION NOT FOUND!");
                    GameController.main.DelayNextAiAction();
                    break;

            }

            //currentEnemyAction++;

        }
        else
        {
            //APPLICATION DU BLEEDING DAMAGE
            //Mulitiplié par 10 pour test, idéalement pondérer selon un power quelconque.
            bleedingDamage = enemy.CheckForStatusEffect(Effect.EffectId.TAKEDAMAGE) * 10;

            if (bleedingDamage > 0)
            {
                combatTextPosition = GameController.main.GetEnemySprite().transform.position;
                combatTextColor = new Color(1, 0, 0, 1);
                combatTextFontSize = 60;
                combatText = bleedingDamage.ToString() + " Bleeding";
                CombatTextManager.Main.CreateText(combatTextPosition, combatText, combatTextColor, combatTextFontSize);

                enemy.TakeDamage(bleedingDamage);
            }
            
            currentEnemyAction = 0;
            enemy.NextMove();

            UIManager.main.PreEndTurnCooldown();
            Debug.Log("Enemy's turn ended");
            //GameController.main.SwitchTurn();
        }
    }

    public void IncrementCurrentEnemyAction()
    {
        currentEnemyAction++;
    }

    public int GetCurrentEnemyAction()
    {
        return currentEnemyAction;
    }
}
