﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class APFlash : MonoBehaviour
{
    [SerializeField]
    private float fadeTime;
    private Color startingColor;
    private Image instance;

    void Start()
    {
        instance = GetComponent<Image>();
        startingColor = instance.color;
    }

    void Update()
    {
        
    }

    public void Flash()
    {
        StopAllCoroutines();
        instance.color = startingColor;
        instance.enabled = true;
        StartCoroutine(FadeOut());
    }

    private IEnumerator FadeOut()
    {
        float startAlpha = instance.color.a;

        float rate = 1.0f / fadeTime;
        float progress = 0.0f;

        while (progress < 1.0)
        {
            Color tmpColor = instance.color;
            instance.color = new Color(tmpColor.r, tmpColor.g, tmpColor.b, Mathf.Lerp(startAlpha, 0, progress));
            progress += rate * Time.deltaTime / Time.timeScale;
            yield return null;
        }
        instance.enabled = false;
        //instance.color = startingColor;
    }
}
