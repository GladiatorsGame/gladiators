﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    public static PlayerAnimation main;
    public Animator playerAnimator;
    public GameController gameController;
    public Light playerLight;

    private enum LightingStage { Normal, Building, MaxLight, NoLight };
    private bool maxLight;
    private float startingIntensity;
    private float startingRange;
    private LightingStage lightingStage;

    // Start is called before the first frame update
    void Start()
    {
        startingIntensity = playerLight.intensity;
        startingRange = playerLight.range;
        lightingStage = LightingStage.Normal;
    }

    // Update is called once per frame
    void Update()
    {
        switch (lightingStage)
        {
            case LightingStage.Building:
                float intensitySpeed = 1.0f;
                float currentIntensity = playerLight.intensity;
                float targetIntensity = startingIntensity + 5;
                playerLight.intensity = Mathf.Lerp(currentIntensity, targetIntensity, Time.deltaTime * intensitySpeed);

                float rangeSpeed = 1.0f;
                float currentRange = playerLight.range;
                float targetRange = startingRange + 5;
                playerLight.range = Mathf.Lerp(currentRange, targetRange, Time.deltaTime * rangeSpeed);
                break;

            case LightingStage.MaxLight:
                intensitySpeed = 20.0f;
                currentIntensity = playerLight.intensity;
                targetIntensity = startingIntensity + 20;
                playerLight.intensity = Mathf.Lerp(currentIntensity, targetIntensity, Time.deltaTime * intensitySpeed);

                rangeSpeed = 20.0f;
                currentRange = playerLight.range;
                targetRange = startingRange + 50;
                playerLight.range = Mathf.Lerp(currentRange, targetRange, Time.deltaTime * rangeSpeed);
                break;

            case LightingStage.NoLight:
                intensitySpeed = 20.0f;
                currentIntensity = playerLight.intensity;
                targetIntensity = 0;
                playerLight.intensity = Mathf.Lerp(currentIntensity, targetIntensity, Time.deltaTime * intensitySpeed);

                rangeSpeed = 20.0f;
                currentRange = playerLight.range;
                targetRange = startingRange;
                playerLight.range = Mathf.Lerp(currentRange, targetRange, Time.deltaTime * rangeSpeed);
                break;

        }
    }

    void OnAttack()
    {
        //envoie la Audiobank de type PLAYER_ATTACK au SoundManager
        SoundManagerN.instance.PlaySfx(0.0f, SoundManagerN.AudioBank.PLAYER_ATTACK);

        lightingStage = LightingStage.Normal;
        playerLight.intensity = startingIntensity;
        playerLight.range = startingRange;

        //remet l'animation à idle
        playerAnimator.SetBool("Attack", false);  //arrête l'animation d'attaque
        GameController.main.ExecuteAction();  //true = isplayerturn
    }

    void OnFocusStart()
    {
        //Joue le son du focus
        SoundManagerN.instance.PlaySfx(0.0f, SoundManagerN.AudioBank.PLAYER_FOCUS);
    }

    void OnFocusEnd()
    {
        //remet l'animation à idle
        playerAnimator.SetBool("Focus", false);  //arrête l'animation d'attaque
        GameController.main.ExecuteAction();
    }

    void OnItemStart()
    {
        //Joue le son du focus
        SoundManagerN.instance.PlaySfx(0.0f, SoundManagerN.AudioBank.PLAYER_FOCUS);
    }

    void OnItemEnd()
    {
        //remet l'animation à idle
        playerAnimator.SetBool("Item", false);  //arrête l'animation d'attaque
        GameController.main.ExecuteAction();
    }

    void OnBuildLight()
    {
        lightingStage = LightingStage.Building;
    }

    void OnMaxLight()
    {
        lightingStage = LightingStage.MaxLight;
    }

    void OnNoLight()
    {
        lightingStage = LightingStage.NoLight;
    }
}

