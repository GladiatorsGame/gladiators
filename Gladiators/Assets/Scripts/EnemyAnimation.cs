﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimation : MonoBehaviour
{
    public Animator enemyAnimator;
    private bool blockCountdown = false;
    private bool isBlocking = false;
    private bool actionCalled = false;
    private bool callNextAction = false; //détermine si une méthode doit appeler la prochaine action de l'ennemi
    private Vector3 combatTextPosition;
    private Color combatTextColor;
    private int combatTextFontSize;
    private string combatText;

    private void OnEndEntry()
    {
        enemyAnimator.SetBool("Entry", false);
    }

    private void OnAction()
    {
        if (!isBlocking && !actionCalled)
        {
            //Debug.Log("Enemy Action");
            GameController.main.ExecuteAction();
            actionCalled = true;
            
        }

    }

    private void OnPlayAttackSound()
    {
        SoundManagerN.instance.PlaySfx(0.0f, SoundManagerN.AudioBank.ENEMY_ATTACK_END);
    }

    private void OnPlaySuperAttackSound()
    {
        SoundManagerN.instance.PlaySfx(0.0f, SoundManagerN.AudioBank.ENEMY_SUPERATTACK_END);
    }


    private void OnStartAttack()
    {
        actionCalled = false;
        callNextAction = true;
        //envoie la Audiobank de type ENEMY_ATTACK au SoundManager
        if (enemyAnimator.GetBool("LoopAttackAudio"))
        {
            //Debug.Log("START ATTACK AUDIO LOOP");
            SoundManagerN.instance.PlaySfxLoop(SoundManagerN.AudioBank.ENEMY_ATTACK_START, SoundManagerN.AudioBank.ENEMY_ATTACK_LOOP);
        }
        else
        {
            //Debug.Log("ATTACK SINGLE SFX");
            SoundManagerN.instance.PlaySfx(0.0f, SoundManagerN.AudioBank.ENEMY_ATTACK_START);
        }
    }

    private void OnEndAttack()
    {
        //Debug.Log("Animation ended, next action call by animation is set to:" + callNextAction);
        enemyAnimator.SetBool("Attack", false);
        if (callNextAction)
        {
           // Debug.Log("Attack animation is calling next action NOW");
            GameController.main.DelayNextAiAction();
        }
        
        
    }

    private void OnStartSuperAttack()
    {
        actionCalled = false;
        callNextAction = true;
        //envoie la Audiobank de type ENEMY_ATTACK au SoundManager
        if (enemyAnimator.GetBool("LoopSuperAttackAudio"))
        {
            SoundManagerN.instance.PlaySfxLoop(SoundManagerN.AudioBank.ENEMY_SUPERATTACK_START, SoundManagerN.AudioBank.ENEMY_SUPERATTACK_LOOP);
        }
        else
        {
            SoundManagerN.instance.PlaySfx(0.0f, SoundManagerN.AudioBank.ENEMY_SUPERATTACK_START);
            
        }
    }

    private void OnEndSuperAttack()
    {
        //Debug.Log("Animation ended, next action call by animation is set to:" + callNextAction);
        enemyAnimator.SetBool("SuperAttack", false);
        if (callNextAction)
        {
            //Debug.Log("Super attack animation is calling next action NOW");
            GameController.main.DelayNextAiAction();
        }

    }
    private void OnStartFocus()
    {
        actionCalled = false;
        callNextAction = true;
        //envoie la Audiobank de type ENEMY_FOCUS au SoundManager
        SoundManagerN.instance.PlaySfx(0.0f, SoundManagerN.AudioBank.ENEMY_FOCUS);
    }

    private void OnEndFocus()
    {
        //Debug.Log("Animation ended, next action call by animation is set to:" + callNextAction);
        enemyAnimator.SetBool("Focus", false);
        if (callNextAction)
        {
            //Debug.Log("Focus animation is calling next action NOW");
            GameController.main.DelayNextAiAction();
        }

    }

    private void OpenBlockWindow()
    {
        if(EntityManager.main.GetPlayer().GetCurrentAp()> 0)
        {
            //Debug.Log("Block Window Open");
            isBlocking = true;
            UIManager.main.TogglePlayerBlockInput();
            //enemyAnimator.speed = 0.05f;
            Time.timeScale = 0.1f;
            //CombatTextManager.Main.CreateText(combatTextPosition, combatText, combatTextColor, combatTextFontSize);

            //blockCountdown = true;
            combatTextPosition = new Vector3(0, 0, 0);
            combatTextColor = new Color(0, 1, 1, 1);
            combatTextFontSize = 120;
            combatText = "BLOCK!";
            CombatTextManager.Main.CreateInstructionText(combatTextPosition, combatText, combatTextColor, combatTextFontSize);
            Invoke(nameof(CloseBlockWindow), 1f * Time.timeScale);//ajuster selon speeds
        }

    }

    private void CloseBlockWindow()
    {
        UIManager.main.TogglePlayerBlockInput();
        Time.timeScale = 1;
        //Debug.Log("Block Window closed");
        isBlocking = false;
        if (!actionCalled)
        {
            //Debug.Log("Blocking action follows");
            OnAction();
        }
            
        //enemyAnimator.speed = 1;
    }

    public void CancelNextActionCall()
    {
        callNextAction = false;
    }

    public void MakeNextActionCall()
    {
        callNextAction = true;
    }

    public void OnStopAttackAudioLoop()
    {
        SoundManagerN.instance.StopSfxLoop(SoundManagerN.AudioBank.ENEMY_ATTACK_LOOP, SoundManagerN.AudioBank.ENEMY_ATTACK_END);
       // SoundManagerN.instance.PlaySfx(0f, SoundManagerN.AudioBank.ENEMY_ATTACK_END);
        
        
    }

    public void OnStopSuperAttackAudioLoop()
    {
        SoundManagerN.instance.StopSfxLoop(SoundManagerN.AudioBank.ENEMY_SUPERATTACK_LOOP, SoundManagerN.AudioBank.ENEMY_SUPERATTACK_END);
        //SoundManagerN.instance.PlaySfx(0f,SoundManagerN.AudioBank.ENEMY_SUPERATTACK_END);
        
    }
}

