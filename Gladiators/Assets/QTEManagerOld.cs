﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QTEManagerOLD : MonoBehaviour
{
    public static QTEManagerOLD main;
    private bool qteStarted;
    int randomNumber;
    private bool qtePassed, timePassed;

    
    // Start is called before the first frame update
    void Start()
    {
        qteStarted = false;
        qtePassed = false;
    }
    void Awake()
    {
        main = this;
    }
    // Update is called once per frame
    void Update()
    {
        if (qteStarted)
        {
            if (timePassed || Input.anyKeyDown)
            {
                if (Input.GetKeyDown(KeyCode.P))
                {
                    Debug.Log("P key");
                }
                else
                {
                    if (Input.GetKeyDown(KeyCode.UpArrow))
                    {
                        if (randomNumber == 0)
                        {
                            qtePassed = true;
                            Debug.Log("QTE 0");
                        }
                    }
                    else if (Input.GetKeyDown(KeyCode.DownArrow))
                    {
                        if (randomNumber == 1)
                        {
                            qtePassed = true;
                            Debug.Log("QTE 1");
                        }
                    }
                    else if (Input.GetKeyDown(KeyCode.LeftArrow))
                    {
                        if (randomNumber == 2)
                        {
                            qtePassed = true;
                            Debug.Log("QTE 2");
                        }
                    }
                    else if (Input.GetKeyDown(KeyCode.RightArrow))
                    {
                        if (randomNumber == 3)
                        {
                            qtePassed = true;
                            Debug.Log("QTE 3");
                        }
                    }

                    // UIManager.main.UpdateQTEImage(1, true);

                    //else
                    //{
                    //    Debug.Log("Bleg");
                    //    qteStarted = false;
                    //    UIManager.main.UpdateQTEImage(randomNumber, false);
                    //}

                    //Debug.Log(qtePassed);
                    Debug.Log("QTEPassed:" + qtePassed); //NICOLAS
                                                         //Debug.Log("QTE Ended");

                    if (qtePassed)
                    {
                        UIManager.main.UpdateQTEImage(randomNumber, Color.cyan);
                    }
                    else
                    {
                        UIManager.main.UpdateQTEImage(randomNumber, Color.black);
                    }

                    //if (IsInvoking("Countdown"))

                    qteStarted = false;
                    Debug.Log("QTE Ended"); //NICOLAS
                }
                
            }
        }
    }
    /// <summary>
    /// Will probably take the creature in arguments to position the QTE
    /// </summary>
    public void StartQTE()
    {
        CancelInvoke("Countdown");
        UIManager.main.ClearQTEImage();

        randomNumber = Random.Range(0, 4);
        //StartCoroutine("StartCountdown");

        UIManager.main.UpdateQTEImage(randomNumber, Color.yellow);

        qteStarted = true;
        qtePassed = false;

        timePassed = false;
        //Debug.Log("Test coroutine");
        Debug.Log("StartQTE"); //NICOLAS
        Invoke("Countdown", 1.5f);

    }


    private void Countdown()
    {
        //Debug.Log("Test coroutine2");
        Debug.Log("Time's Up!");
        timePassed = true;

        //Debug.Log("Test coroutine");

        //yield return new WaitForSeconds(1.5f);
        //Debug.Log("Test coroutine2");

        //if (qteStarted)
        //{
        //    
        //}
    }
}
